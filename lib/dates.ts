import _isDate from "date-fns/isDate";
import parse from "date-fns/parse";

function isDate(d: string | Date): d is Date {
  return _isDate(d);
}

export function parseDateString(value: string | Date): Date | undefined {
  const date = isDate(value) ? value : parse(value, "dd.MM.yyyy", new Date());

  // Javascript will throw when date is invalid but only when doing .toISOString()
  // https://stackoverflow.com/questions/1353684/detecting-an-invalid-date-date-instance-in-javascript
  if (Number.isNaN(date.getTime())) {
    return undefined;
  }

  if (date.getFullYear() < 50) {
    // assume dates like 01.01.02 to mean 2002
    date.setFullYear(date.getFullYear() + 2000);
  } else if (date.getFullYear() < 100) {
    // assume dates like 01.01.99 to mean 1999
    date.setFullYear(date.getFullYear() + 1900);
  }
  return date;
}
