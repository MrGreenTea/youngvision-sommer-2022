import { GetStaticProps } from "next";
import { I18nProps } from "next-rosetta";
import { Locale } from "../i18n";

export type LocalizedGetStaticProps<
  T extends Record<string, unknown> = Record<string, unknown>
> = GetStaticProps<T & I18nProps<Locale>>;

export type LocalizedProps<
  T extends Record<string, unknown> = Record<string, unknown>
> = LocalizedGetStaticProps<T>;

export type Ticket = {
  name: string;
  minimum: number;
  maximum: number;
  recommended: number;
  info: string;
};
