export function resortToPlaceholderPortait(
  url: string | undefined | null
): string {
  return url ?? "/images/Portrait_Placeholder.png";
}
