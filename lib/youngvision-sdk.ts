/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* AUTOMATICALLY GENERATED! DONT EDIT! */

import { GraphQLClient } from "graphql-request";
import * as Dom from "graphql-request/dist/types.dom";
import gql from "graphql-tag";
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> &
  { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> &
  { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A date string, such as 2007-12-03 (YYYY-MM-DD), compliant with ISO 8601 standard for representation of dates using the Gregorian calendar. */
  Date: string;
  /** A date-time string at UTC, such as 2007-12-03T10:15:30Z, compliant with the date-timeformat outlined in section 5.6 of the RFC 3339 profile of the ISO 8601 standard for representationof dates and times using the Gregorian calendar. */
  DateTime: string;
  Hex: string;
  /** Raw JSON value */
  Json: string;
  /** The Long scalar type represents non-fractional signed whole numeric values. Long can represent values between -(2^63) and 2^63 - 1. */
  Long: number;
  RGBAHue: number;
  RGBATransparency: number;
  /** Slate-compatible RichText AST */
  RichTextAST: string;
};

export type Aggregate = {
  __typename?: "Aggregate";
  count: Scalars["Int"];
};

/** Asset system model */
export type Asset = Node & {
  __typename?: "Asset";
  /** System stage field */
  stage: Stage;
  /** System Locale field */
  locale: Locale;
  /** Get the other localizations for this document */
  localizations: Array<Asset>;
  /** Get the document in other stages */
  documentInStages: Array<Asset>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** The file handle */
  handle: Scalars["String"];
  /** The file name */
  fileName: Scalars["String"];
  /** The height of the file */
  height?: Maybe<Scalars["Float"]>;
  /** The file width */
  width?: Maybe<Scalars["Float"]>;
  /** The file size */
  size?: Maybe<Scalars["Float"]>;
  /** The mime type of the file */
  mimeType?: Maybe<Scalars["String"]>;
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  logoSponsor: Array<Sponsor>;
  imageTeamMember: Array<TeamMember>;
  imageTestimonial: Array<Testimonial>;
  imageShortInfoText: Array<ShortInfoText>;
  imageSpeaker: Array<Speaker>;
  imageExtraProgram: Array<ExtraProgram>;
  scheduledIn: Array<ScheduledOperation>;
  /** List of Asset versions */
  history: Array<Version>;
  /** Get the url for the asset with provided transformations applied. */
  url: Scalars["String"];
};

/** Asset system model */
export type AssetLocalizationsArgs = {
  locales?: Array<Locale>;
  includeCurrent?: Scalars["Boolean"];
};

/** Asset system model */
export type AssetDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

/** Asset system model */
export type AssetCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

/** Asset system model */
export type AssetUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

/** Asset system model */
export type AssetPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

/** Asset system model */
export type AssetCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** Asset system model */
export type AssetUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** Asset system model */
export type AssetPublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** Asset system model */
export type AssetLogoSponsorArgs = {
  where?: Maybe<SponsorWhereInput>;
  orderBy?: Maybe<SponsorOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

/** Asset system model */
export type AssetImageTeamMemberArgs = {
  where?: Maybe<TeamMemberWhereInput>;
  orderBy?: Maybe<TeamMemberOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

/** Asset system model */
export type AssetImageTestimonialArgs = {
  where?: Maybe<TestimonialWhereInput>;
  orderBy?: Maybe<TestimonialOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

/** Asset system model */
export type AssetImageShortInfoTextArgs = {
  where?: Maybe<ShortInfoTextWhereInput>;
  orderBy?: Maybe<ShortInfoTextOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

/** Asset system model */
export type AssetImageSpeakerArgs = {
  where?: Maybe<SpeakerWhereInput>;
  orderBy?: Maybe<SpeakerOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

/** Asset system model */
export type AssetImageExtraProgramArgs = {
  where?: Maybe<ExtraProgramWhereInput>;
  orderBy?: Maybe<ExtraProgramOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

/** Asset system model */
export type AssetScheduledInArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

/** Asset system model */
export type AssetHistoryArgs = {
  limit?: Scalars["Int"];
  skip?: Scalars["Int"];
  stageOverride?: Maybe<Stage>;
};

/** Asset system model */
export type AssetUrlArgs = {
  transformation?: Maybe<AssetTransformationInput>;
};

export type AssetConnectInput = {
  /** Document to connect */
  where: AssetWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type AssetConnection = {
  __typename?: "AssetConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<AssetEdge>;
  aggregate: Aggregate;
};

export type AssetCreateInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  handle: Scalars["String"];
  fileName: Scalars["String"];
  height?: Maybe<Scalars["Float"]>;
  width?: Maybe<Scalars["Float"]>;
  size?: Maybe<Scalars["Float"]>;
  mimeType?: Maybe<Scalars["String"]>;
  logoSponsor?: Maybe<SponsorCreateManyInlineInput>;
  imageTeamMember?: Maybe<TeamMemberCreateManyInlineInput>;
  imageTestimonial?: Maybe<TestimonialCreateManyInlineInput>;
  imageShortInfoText?: Maybe<ShortInfoTextCreateManyInlineInput>;
  imageSpeaker?: Maybe<SpeakerCreateManyInlineInput>;
  imageExtraProgram?: Maybe<ExtraProgramCreateManyInlineInput>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: Maybe<AssetCreateLocalizationsInput>;
};

export type AssetCreateLocalizationDataInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  handle: Scalars["String"];
  fileName: Scalars["String"];
  height?: Maybe<Scalars["Float"]>;
  width?: Maybe<Scalars["Float"]>;
  size?: Maybe<Scalars["Float"]>;
  mimeType?: Maybe<Scalars["String"]>;
};

export type AssetCreateLocalizationInput = {
  /** Localization input */
  data: AssetCreateLocalizationDataInput;
  locale: Locale;
};

export type AssetCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: Maybe<Array<AssetCreateLocalizationInput>>;
};

export type AssetCreateManyInlineInput = {
  /** Create and connect multiple existing Asset documents */
  create?: Maybe<Array<AssetCreateInput>>;
  /** Connect multiple existing Asset documents */
  connect?: Maybe<Array<AssetWhereUniqueInput>>;
};

export type AssetCreateOneInlineInput = {
  /** Create and connect one Asset document */
  create?: Maybe<AssetCreateInput>;
  /** Connect one existing Asset document */
  connect?: Maybe<AssetWhereUniqueInput>;
};

/** An edge in a connection. */
export type AssetEdge = {
  __typename?: "AssetEdge";
  /** The item at the end of the edge. */
  node: Asset;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** Identifies documents */
export type AssetManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<AssetWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<AssetWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<AssetWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  logoSponsor_every?: Maybe<SponsorWhereInput>;
  logoSponsor_some?: Maybe<SponsorWhereInput>;
  logoSponsor_none?: Maybe<SponsorWhereInput>;
  imageTeamMember_every?: Maybe<TeamMemberWhereInput>;
  imageTeamMember_some?: Maybe<TeamMemberWhereInput>;
  imageTeamMember_none?: Maybe<TeamMemberWhereInput>;
  imageTestimonial_every?: Maybe<TestimonialWhereInput>;
  imageTestimonial_some?: Maybe<TestimonialWhereInput>;
  imageTestimonial_none?: Maybe<TestimonialWhereInput>;
  imageShortInfoText_every?: Maybe<ShortInfoTextWhereInput>;
  imageShortInfoText_some?: Maybe<ShortInfoTextWhereInput>;
  imageShortInfoText_none?: Maybe<ShortInfoTextWhereInput>;
  imageSpeaker_every?: Maybe<SpeakerWhereInput>;
  imageSpeaker_some?: Maybe<SpeakerWhereInput>;
  imageSpeaker_none?: Maybe<SpeakerWhereInput>;
  imageExtraProgram_every?: Maybe<ExtraProgramWhereInput>;
  imageExtraProgram_some?: Maybe<ExtraProgramWhereInput>;
  imageExtraProgram_none?: Maybe<ExtraProgramWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

export enum AssetOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  HandleAsc = "handle_ASC",
  HandleDesc = "handle_DESC",
  FileNameAsc = "fileName_ASC",
  FileNameDesc = "fileName_DESC",
  HeightAsc = "height_ASC",
  HeightDesc = "height_DESC",
  WidthAsc = "width_ASC",
  WidthDesc = "width_DESC",
  SizeAsc = "size_ASC",
  SizeDesc = "size_DESC",
  MimeTypeAsc = "mimeType_ASC",
  MimeTypeDesc = "mimeType_DESC",
}

/** Transformations for Assets */
export type AssetTransformationInput = {
  image?: Maybe<ImageTransformationInput>;
  document?: Maybe<DocumentTransformationInput>;
  /** Pass true if you want to validate the passed transformation parameters */
  validateOptions?: Maybe<Scalars["Boolean"]>;
};

export type AssetUpdateInput = {
  handle?: Maybe<Scalars["String"]>;
  fileName?: Maybe<Scalars["String"]>;
  height?: Maybe<Scalars["Float"]>;
  width?: Maybe<Scalars["Float"]>;
  size?: Maybe<Scalars["Float"]>;
  mimeType?: Maybe<Scalars["String"]>;
  logoSponsor?: Maybe<SponsorUpdateManyInlineInput>;
  imageTeamMember?: Maybe<TeamMemberUpdateManyInlineInput>;
  imageTestimonial?: Maybe<TestimonialUpdateManyInlineInput>;
  imageShortInfoText?: Maybe<ShortInfoTextUpdateManyInlineInput>;
  imageSpeaker?: Maybe<SpeakerUpdateManyInlineInput>;
  imageExtraProgram?: Maybe<ExtraProgramUpdateManyInlineInput>;
  /** Manage document localizations */
  localizations?: Maybe<AssetUpdateLocalizationsInput>;
};

export type AssetUpdateLocalizationDataInput = {
  handle?: Maybe<Scalars["String"]>;
  fileName?: Maybe<Scalars["String"]>;
  height?: Maybe<Scalars["Float"]>;
  width?: Maybe<Scalars["Float"]>;
  size?: Maybe<Scalars["Float"]>;
  mimeType?: Maybe<Scalars["String"]>;
};

export type AssetUpdateLocalizationInput = {
  data: AssetUpdateLocalizationDataInput;
  locale: Locale;
};

export type AssetUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: Maybe<Array<AssetCreateLocalizationInput>>;
  /** Localizations to update */
  update?: Maybe<Array<AssetUpdateLocalizationInput>>;
  upsert?: Maybe<Array<AssetUpsertLocalizationInput>>;
  /** Localizations to delete */
  delete?: Maybe<Array<Locale>>;
};

export type AssetUpdateManyInlineInput = {
  /** Create and connect multiple Asset documents */
  create?: Maybe<Array<AssetCreateInput>>;
  /** Connect multiple existing Asset documents */
  connect?: Maybe<Array<AssetConnectInput>>;
  /** Override currently-connected documents with multiple existing Asset documents */
  set?: Maybe<Array<AssetWhereUniqueInput>>;
  /** Update multiple Asset documents */
  update?: Maybe<Array<AssetUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple Asset documents */
  upsert?: Maybe<Array<AssetUpsertWithNestedWhereUniqueInput>>;
  /** Disconnect multiple Asset documents */
  disconnect?: Maybe<Array<AssetWhereUniqueInput>>;
  /** Delete multiple Asset documents */
  delete?: Maybe<Array<AssetWhereUniqueInput>>;
};

export type AssetUpdateManyInput = {
  fileName?: Maybe<Scalars["String"]>;
  height?: Maybe<Scalars["Float"]>;
  width?: Maybe<Scalars["Float"]>;
  size?: Maybe<Scalars["Float"]>;
  mimeType?: Maybe<Scalars["String"]>;
  /** Optional updates to localizations */
  localizations?: Maybe<AssetUpdateManyLocalizationsInput>;
};

export type AssetUpdateManyLocalizationDataInput = {
  fileName?: Maybe<Scalars["String"]>;
  height?: Maybe<Scalars["Float"]>;
  width?: Maybe<Scalars["Float"]>;
  size?: Maybe<Scalars["Float"]>;
  mimeType?: Maybe<Scalars["String"]>;
};

export type AssetUpdateManyLocalizationInput = {
  data: AssetUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type AssetUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: Maybe<Array<AssetUpdateManyLocalizationInput>>;
};

export type AssetUpdateManyWithNestedWhereInput = {
  /** Document search */
  where: AssetWhereInput;
  /** Update many input */
  data: AssetUpdateManyInput;
};

export type AssetUpdateOneInlineInput = {
  /** Create and connect one Asset document */
  create?: Maybe<AssetCreateInput>;
  /** Update single Asset document */
  update?: Maybe<AssetUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Asset document */
  upsert?: Maybe<AssetUpsertWithNestedWhereUniqueInput>;
  /** Connect existing Asset document */
  connect?: Maybe<AssetWhereUniqueInput>;
  /** Disconnect currently connected Asset document */
  disconnect?: Maybe<Scalars["Boolean"]>;
  /** Delete currently connected Asset document */
  delete?: Maybe<Scalars["Boolean"]>;
};

export type AssetUpdateWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: AssetWhereUniqueInput;
  /** Document to update */
  data: AssetUpdateInput;
};

export type AssetUpsertInput = {
  /** Create document if it didn't exist */
  create: AssetCreateInput;
  /** Update document if it exists */
  update: AssetUpdateInput;
};

export type AssetUpsertLocalizationInput = {
  update: AssetUpdateLocalizationDataInput;
  create: AssetCreateLocalizationDataInput;
  locale: Locale;
};

export type AssetUpsertWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: AssetWhereUniqueInput;
  /** Upsert data */
  data: AssetUpsertInput;
};

/** Identifies documents */
export type AssetWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<AssetWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<AssetWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<AssetWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  handle?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  handle_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  handle_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  handle_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  handle_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  handle_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  handle_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  handle_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  handle_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  handle_not_ends_with?: Maybe<Scalars["String"]>;
  fileName?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  fileName_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  fileName_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  fileName_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  fileName_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  fileName_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  fileName_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  fileName_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  fileName_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  fileName_not_ends_with?: Maybe<Scalars["String"]>;
  height?: Maybe<Scalars["Float"]>;
  /** All values that are not equal to given value. */
  height_not?: Maybe<Scalars["Float"]>;
  /** All values that are contained in given list. */
  height_in?: Maybe<Array<Scalars["Float"]>>;
  /** All values that are not contained in given list. */
  height_not_in?: Maybe<Array<Scalars["Float"]>>;
  /** All values less than the given value. */
  height_lt?: Maybe<Scalars["Float"]>;
  /** All values less than or equal the given value. */
  height_lte?: Maybe<Scalars["Float"]>;
  /** All values greater than the given value. */
  height_gt?: Maybe<Scalars["Float"]>;
  /** All values greater than or equal the given value. */
  height_gte?: Maybe<Scalars["Float"]>;
  width?: Maybe<Scalars["Float"]>;
  /** All values that are not equal to given value. */
  width_not?: Maybe<Scalars["Float"]>;
  /** All values that are contained in given list. */
  width_in?: Maybe<Array<Scalars["Float"]>>;
  /** All values that are not contained in given list. */
  width_not_in?: Maybe<Array<Scalars["Float"]>>;
  /** All values less than the given value. */
  width_lt?: Maybe<Scalars["Float"]>;
  /** All values less than or equal the given value. */
  width_lte?: Maybe<Scalars["Float"]>;
  /** All values greater than the given value. */
  width_gt?: Maybe<Scalars["Float"]>;
  /** All values greater than or equal the given value. */
  width_gte?: Maybe<Scalars["Float"]>;
  size?: Maybe<Scalars["Float"]>;
  /** All values that are not equal to given value. */
  size_not?: Maybe<Scalars["Float"]>;
  /** All values that are contained in given list. */
  size_in?: Maybe<Array<Scalars["Float"]>>;
  /** All values that are not contained in given list. */
  size_not_in?: Maybe<Array<Scalars["Float"]>>;
  /** All values less than the given value. */
  size_lt?: Maybe<Scalars["Float"]>;
  /** All values less than or equal the given value. */
  size_lte?: Maybe<Scalars["Float"]>;
  /** All values greater than the given value. */
  size_gt?: Maybe<Scalars["Float"]>;
  /** All values greater than or equal the given value. */
  size_gte?: Maybe<Scalars["Float"]>;
  mimeType?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  mimeType_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  mimeType_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  mimeType_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  mimeType_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  mimeType_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  mimeType_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  mimeType_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  mimeType_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  mimeType_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  logoSponsor_every?: Maybe<SponsorWhereInput>;
  logoSponsor_some?: Maybe<SponsorWhereInput>;
  logoSponsor_none?: Maybe<SponsorWhereInput>;
  imageTeamMember_every?: Maybe<TeamMemberWhereInput>;
  imageTeamMember_some?: Maybe<TeamMemberWhereInput>;
  imageTeamMember_none?: Maybe<TeamMemberWhereInput>;
  imageTestimonial_every?: Maybe<TestimonialWhereInput>;
  imageTestimonial_some?: Maybe<TestimonialWhereInput>;
  imageTestimonial_none?: Maybe<TestimonialWhereInput>;
  imageShortInfoText_every?: Maybe<ShortInfoTextWhereInput>;
  imageShortInfoText_some?: Maybe<ShortInfoTextWhereInput>;
  imageShortInfoText_none?: Maybe<ShortInfoTextWhereInput>;
  imageSpeaker_every?: Maybe<SpeakerWhereInput>;
  imageSpeaker_some?: Maybe<SpeakerWhereInput>;
  imageSpeaker_none?: Maybe<SpeakerWhereInput>;
  imageExtraProgram_every?: Maybe<ExtraProgramWhereInput>;
  imageExtraProgram_some?: Maybe<ExtraProgramWhereInput>;
  imageExtraProgram_none?: Maybe<ExtraProgramWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

/** References Asset record uniquely */
export type AssetWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

export type BatchPayload = {
  __typename?: "BatchPayload";
  /** The number of nodes that have been affected by the Batch operation. */
  count: Scalars["Long"];
};

/** Representing a color value comprising of HEX, RGBA and css color values */
export type Color = {
  __typename?: "Color";
  hex: Scalars["Hex"];
  rgba: Rgba;
  css: Scalars["String"];
};

/** Accepts either HEX or RGBA color value. At least one of hex or rgba value should be passed. If both are passed RGBA is used. */
export type ColorInput = {
  hex?: Maybe<Scalars["Hex"]>;
  rgba?: Maybe<RgbaInput>;
};

export type ConnectPositionInput = {
  /** Connect document after specified document */
  after?: Maybe<Scalars["ID"]>;
  /** Connect document before specified document */
  before?: Maybe<Scalars["ID"]>;
  /** Connect document at first position */
  start?: Maybe<Scalars["Boolean"]>;
  /** Connect document at last position */
  end?: Maybe<Scalars["Boolean"]>;
};

export type Contact = Node & {
  __typename?: "Contact";
  /** System stage field */
  stage: Stage;
  /** Get the document in other stages */
  documentInStages: Array<Contact>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  email: Scalars["String"];
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  scheduledIn: Array<ScheduledOperation>;
  /** List of Contact versions */
  history: Array<Version>;
};

export type ContactDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

export type ContactCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type ContactUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type ContactPublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type ContactScheduledInArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

export type ContactHistoryArgs = {
  limit?: Scalars["Int"];
  skip?: Scalars["Int"];
  stageOverride?: Maybe<Stage>;
};

export type ContactConnectInput = {
  /** Document to connect */
  where: ContactWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type ContactConnection = {
  __typename?: "ContactConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<ContactEdge>;
  aggregate: Aggregate;
};

export type ContactCreateInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  email: Scalars["String"];
};

export type ContactCreateManyInlineInput = {
  /** Create and connect multiple existing Contact documents */
  create?: Maybe<Array<ContactCreateInput>>;
  /** Connect multiple existing Contact documents */
  connect?: Maybe<Array<ContactWhereUniqueInput>>;
};

export type ContactCreateOneInlineInput = {
  /** Create and connect one Contact document */
  create?: Maybe<ContactCreateInput>;
  /** Connect one existing Contact document */
  connect?: Maybe<ContactWhereUniqueInput>;
};

/** An edge in a connection. */
export type ContactEdge = {
  __typename?: "ContactEdge";
  /** The item at the end of the edge. */
  node: Contact;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** Identifies documents */
export type ContactManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<ContactWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<ContactWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<ContactWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  email?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  email_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  email_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  email_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  email_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  email_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  email_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  email_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  email_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  email_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

export enum ContactOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  EmailAsc = "email_ASC",
  EmailDesc = "email_DESC",
}

export type ContactUpdateInput = {
  email?: Maybe<Scalars["String"]>;
};

export type ContactUpdateManyInlineInput = {
  /** Create and connect multiple Contact documents */
  create?: Maybe<Array<ContactCreateInput>>;
  /** Connect multiple existing Contact documents */
  connect?: Maybe<Array<ContactConnectInput>>;
  /** Override currently-connected documents with multiple existing Contact documents */
  set?: Maybe<Array<ContactWhereUniqueInput>>;
  /** Update multiple Contact documents */
  update?: Maybe<Array<ContactUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple Contact documents */
  upsert?: Maybe<Array<ContactUpsertWithNestedWhereUniqueInput>>;
  /** Disconnect multiple Contact documents */
  disconnect?: Maybe<Array<ContactWhereUniqueInput>>;
  /** Delete multiple Contact documents */
  delete?: Maybe<Array<ContactWhereUniqueInput>>;
};

export type ContactUpdateManyInput = {
  /** No fields in updateMany data input */
  _?: Maybe<Scalars["String"]>;
};

export type ContactUpdateManyWithNestedWhereInput = {
  /** Document search */
  where: ContactWhereInput;
  /** Update many input */
  data: ContactUpdateManyInput;
};

export type ContactUpdateOneInlineInput = {
  /** Create and connect one Contact document */
  create?: Maybe<ContactCreateInput>;
  /** Update single Contact document */
  update?: Maybe<ContactUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Contact document */
  upsert?: Maybe<ContactUpsertWithNestedWhereUniqueInput>;
  /** Connect existing Contact document */
  connect?: Maybe<ContactWhereUniqueInput>;
  /** Disconnect currently connected Contact document */
  disconnect?: Maybe<Scalars["Boolean"]>;
  /** Delete currently connected Contact document */
  delete?: Maybe<Scalars["Boolean"]>;
};

export type ContactUpdateWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: ContactWhereUniqueInput;
  /** Document to update */
  data: ContactUpdateInput;
};

export type ContactUpsertInput = {
  /** Create document if it didn't exist */
  create: ContactCreateInput;
  /** Update document if it exists */
  update: ContactUpdateInput;
};

export type ContactUpsertWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: ContactWhereUniqueInput;
  /** Upsert data */
  data: ContactUpsertInput;
};

/** Identifies documents */
export type ContactWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<ContactWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<ContactWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<ContactWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  email?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  email_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  email_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  email_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  email_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  email_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  email_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  email_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  email_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  email_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

/** References Contact record uniquely */
export type ContactWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
  email?: Maybe<Scalars["String"]>;
};

export type CovidConcept = Node & {
  __typename?: "CovidConcept";
  /** System stage field */
  stage: Stage;
  /** System Locale field */
  locale: Locale;
  /** Get the other localizations for this document */
  localizations: Array<CovidConcept>;
  /** Get the document in other stages */
  documentInStages: Array<CovidConcept>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  header: Scalars["String"];
  description: Scalars["String"];
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  scheduledIn: Array<ScheduledOperation>;
  /** List of CovidConcept versions */
  history: Array<Version>;
};

export type CovidConceptLocalizationsArgs = {
  locales?: Array<Locale>;
  includeCurrent?: Scalars["Boolean"];
};

export type CovidConceptDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

export type CovidConceptCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type CovidConceptUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type CovidConceptPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type CovidConceptCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type CovidConceptUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type CovidConceptPublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type CovidConceptScheduledInArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

export type CovidConceptHistoryArgs = {
  limit?: Scalars["Int"];
  skip?: Scalars["Int"];
  stageOverride?: Maybe<Stage>;
};

export type CovidConceptConnectInput = {
  /** Document to connect */
  where: CovidConceptWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type CovidConceptConnection = {
  __typename?: "CovidConceptConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<CovidConceptEdge>;
  aggregate: Aggregate;
};

export type CovidConceptCreateInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** header input for default locale (en) */
  header: Scalars["String"];
  /** description input for default locale (en) */
  description: Scalars["String"];
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: Maybe<CovidConceptCreateLocalizationsInput>;
};

export type CovidConceptCreateLocalizationDataInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  header: Scalars["String"];
  description: Scalars["String"];
};

export type CovidConceptCreateLocalizationInput = {
  /** Localization input */
  data: CovidConceptCreateLocalizationDataInput;
  locale: Locale;
};

export type CovidConceptCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: Maybe<Array<CovidConceptCreateLocalizationInput>>;
};

export type CovidConceptCreateManyInlineInput = {
  /** Create and connect multiple existing CovidConcept documents */
  create?: Maybe<Array<CovidConceptCreateInput>>;
  /** Connect multiple existing CovidConcept documents */
  connect?: Maybe<Array<CovidConceptWhereUniqueInput>>;
};

export type CovidConceptCreateOneInlineInput = {
  /** Create and connect one CovidConcept document */
  create?: Maybe<CovidConceptCreateInput>;
  /** Connect one existing CovidConcept document */
  connect?: Maybe<CovidConceptWhereUniqueInput>;
};

/** An edge in a connection. */
export type CovidConceptEdge = {
  __typename?: "CovidConceptEdge";
  /** The item at the end of the edge. */
  node: CovidConcept;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** Identifies documents */
export type CovidConceptManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<CovidConceptWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<CovidConceptWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<CovidConceptWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

export enum CovidConceptOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  HeaderAsc = "header_ASC",
  HeaderDesc = "header_DESC",
  DescriptionAsc = "description_ASC",
  DescriptionDesc = "description_DESC",
}

export type CovidConceptUpdateInput = {
  /** header input for default locale (en) */
  header?: Maybe<Scalars["String"]>;
  /** description input for default locale (en) */
  description?: Maybe<Scalars["String"]>;
  /** Manage document localizations */
  localizations?: Maybe<CovidConceptUpdateLocalizationsInput>;
};

export type CovidConceptUpdateLocalizationDataInput = {
  header?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
};

export type CovidConceptUpdateLocalizationInput = {
  data: CovidConceptUpdateLocalizationDataInput;
  locale: Locale;
};

export type CovidConceptUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: Maybe<Array<CovidConceptCreateLocalizationInput>>;
  /** Localizations to update */
  update?: Maybe<Array<CovidConceptUpdateLocalizationInput>>;
  upsert?: Maybe<Array<CovidConceptUpsertLocalizationInput>>;
  /** Localizations to delete */
  delete?: Maybe<Array<Locale>>;
};

export type CovidConceptUpdateManyInlineInput = {
  /** Create and connect multiple CovidConcept documents */
  create?: Maybe<Array<CovidConceptCreateInput>>;
  /** Connect multiple existing CovidConcept documents */
  connect?: Maybe<Array<CovidConceptConnectInput>>;
  /** Override currently-connected documents with multiple existing CovidConcept documents */
  set?: Maybe<Array<CovidConceptWhereUniqueInput>>;
  /** Update multiple CovidConcept documents */
  update?: Maybe<Array<CovidConceptUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple CovidConcept documents */
  upsert?: Maybe<Array<CovidConceptUpsertWithNestedWhereUniqueInput>>;
  /** Disconnect multiple CovidConcept documents */
  disconnect?: Maybe<Array<CovidConceptWhereUniqueInput>>;
  /** Delete multiple CovidConcept documents */
  delete?: Maybe<Array<CovidConceptWhereUniqueInput>>;
};

export type CovidConceptUpdateManyInput = {
  /** header input for default locale (en) */
  header?: Maybe<Scalars["String"]>;
  /** description input for default locale (en) */
  description?: Maybe<Scalars["String"]>;
  /** Optional updates to localizations */
  localizations?: Maybe<CovidConceptUpdateManyLocalizationsInput>;
};

export type CovidConceptUpdateManyLocalizationDataInput = {
  header?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
};

export type CovidConceptUpdateManyLocalizationInput = {
  data: CovidConceptUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type CovidConceptUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: Maybe<Array<CovidConceptUpdateManyLocalizationInput>>;
};

export type CovidConceptUpdateManyWithNestedWhereInput = {
  /** Document search */
  where: CovidConceptWhereInput;
  /** Update many input */
  data: CovidConceptUpdateManyInput;
};

export type CovidConceptUpdateOneInlineInput = {
  /** Create and connect one CovidConcept document */
  create?: Maybe<CovidConceptCreateInput>;
  /** Update single CovidConcept document */
  update?: Maybe<CovidConceptUpdateWithNestedWhereUniqueInput>;
  /** Upsert single CovidConcept document */
  upsert?: Maybe<CovidConceptUpsertWithNestedWhereUniqueInput>;
  /** Connect existing CovidConcept document */
  connect?: Maybe<CovidConceptWhereUniqueInput>;
  /** Disconnect currently connected CovidConcept document */
  disconnect?: Maybe<Scalars["Boolean"]>;
  /** Delete currently connected CovidConcept document */
  delete?: Maybe<Scalars["Boolean"]>;
};

export type CovidConceptUpdateWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: CovidConceptWhereUniqueInput;
  /** Document to update */
  data: CovidConceptUpdateInput;
};

export type CovidConceptUpsertInput = {
  /** Create document if it didn't exist */
  create: CovidConceptCreateInput;
  /** Update document if it exists */
  update: CovidConceptUpdateInput;
};

export type CovidConceptUpsertLocalizationInput = {
  update: CovidConceptUpdateLocalizationDataInput;
  create: CovidConceptCreateLocalizationDataInput;
  locale: Locale;
};

export type CovidConceptUpsertWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: CovidConceptWhereUniqueInput;
  /** Upsert data */
  data: CovidConceptUpsertInput;
};

/** Identifies documents */
export type CovidConceptWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<CovidConceptWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<CovidConceptWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<CovidConceptWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  header?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  header_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  header_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  header_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  header_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  header_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  header_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  header_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  header_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  header_not_ends_with?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  description_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  description_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  description_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  description_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  description_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  description_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  description_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  description_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  description_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

/** References CovidConcept record uniquely */
export type CovidConceptWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

export enum DiscountColor {
  Green = "green",
  Cyan = "cyan",
  Yellow = "yellow",
  Pink = "pink",
}

export enum DocumentFileTypes {
  Jpg = "jpg",
  Odp = "odp",
  Ods = "ods",
  Odt = "odt",
  Png = "png",
  Svg = "svg",
  Txt = "txt",
  Webp = "webp",
  Docx = "docx",
  Pdf = "pdf",
  Html = "html",
  Doc = "doc",
  Xlsx = "xlsx",
  Xls = "xls",
  Pptx = "pptx",
  Ppt = "ppt",
}

export type DocumentOutputInput = {
  /**
   * Transforms a document into a desired file type.
   * See this matrix for format support:
   *
   * PDF:	jpg, odp, ods, odt, png, svg, txt, and webp
   * DOC:	docx, html, jpg, odt, pdf, png, svg, txt, and webp
   * DOCX:	doc, html, jpg, odt, pdf, png, svg, txt, and webp
   * ODT:	doc, docx, html, jpg, pdf, png, svg, txt, and webp
   * XLS:	jpg, pdf, ods, png, svg, xlsx, and webp
   * XLSX:	jpg, pdf, ods, png, svg, xls, and webp
   * ODS:	jpg, pdf, png, xls, svg, xlsx, and webp
   * PPT:	jpg, odp, pdf, png, svg, pptx, and webp
   * PPTX:	jpg, odp, pdf, png, svg, ppt, and webp
   * ODP:	jpg, pdf, png, ppt, svg, pptx, and webp
   * BMP:	jpg, odp, ods, odt, pdf, png, svg, and webp
   * GIF:	jpg, odp, ods, odt, pdf, png, svg, and webp
   * JPG:	jpg, odp, ods, odt, pdf, png, svg, and webp
   * PNG:	jpg, odp, ods, odt, pdf, png, svg, and webp
   * WEBP:	jpg, odp, ods, odt, pdf, png, svg, and webp
   * TIFF:	jpg, odp, ods, odt, pdf, png, svg, and webp
   * AI:	    jpg, odp, ods, odt, pdf, png, svg, and webp
   * PSD:	jpg, odp, ods, odt, pdf, png, svg, and webp
   * SVG:	jpg, odp, ods, odt, pdf, png, and webp
   * HTML:	jpg, odt, pdf, svg, txt, and webp
   * TXT:	jpg, html, odt, pdf, svg, and webp
   */
  format?: Maybe<DocumentFileTypes>;
};

/** Transformations for Documents */
export type DocumentTransformationInput = {
  /** Changes the output for the file. */
  output?: Maybe<DocumentOutputInput>;
};

export type DocumentVersion = {
  __typename?: "DocumentVersion";
  id: Scalars["ID"];
  stage: Stage;
  revision: Scalars["Int"];
  createdAt: Scalars["DateTime"];
  data?: Maybe<Scalars["Json"]>;
};

export type ExtraProgram = Node & {
  __typename?: "ExtraProgram";
  /** System stage field */
  stage: Stage;
  /** System Locale field */
  locale: Locale;
  /** Get the other localizations for this document */
  localizations: Array<ExtraProgram>;
  /** Get the document in other stages */
  documentInStages: Array<ExtraProgram>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  subtitle?: Maybe<Scalars["String"]>;
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  image?: Maybe<Asset>;
  scheduledIn: Array<ScheduledOperation>;
  /** List of ExtraProgram versions */
  history: Array<Version>;
};

export type ExtraProgramLocalizationsArgs = {
  locales?: Array<Locale>;
  includeCurrent?: Scalars["Boolean"];
};

export type ExtraProgramDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

export type ExtraProgramCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ExtraProgramUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ExtraProgramPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ExtraProgramCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type ExtraProgramUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type ExtraProgramPublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type ExtraProgramImageArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type ExtraProgramScheduledInArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

export type ExtraProgramHistoryArgs = {
  limit?: Scalars["Int"];
  skip?: Scalars["Int"];
  stageOverride?: Maybe<Stage>;
};

export type ExtraProgramConnectInput = {
  /** Document to connect */
  where: ExtraProgramWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type ExtraProgramConnection = {
  __typename?: "ExtraProgramConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<ExtraProgramEdge>;
  aggregate: Aggregate;
};

export type ExtraProgramCreateInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** subtitle input for default locale (en) */
  subtitle?: Maybe<Scalars["String"]>;
  image?: Maybe<AssetCreateOneInlineInput>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: Maybe<ExtraProgramCreateLocalizationsInput>;
};

export type ExtraProgramCreateLocalizationDataInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  subtitle?: Maybe<Scalars["String"]>;
};

export type ExtraProgramCreateLocalizationInput = {
  /** Localization input */
  data: ExtraProgramCreateLocalizationDataInput;
  locale: Locale;
};

export type ExtraProgramCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: Maybe<Array<ExtraProgramCreateLocalizationInput>>;
};

export type ExtraProgramCreateManyInlineInput = {
  /** Create and connect multiple existing ExtraProgram documents */
  create?: Maybe<Array<ExtraProgramCreateInput>>;
  /** Connect multiple existing ExtraProgram documents */
  connect?: Maybe<Array<ExtraProgramWhereUniqueInput>>;
};

export type ExtraProgramCreateOneInlineInput = {
  /** Create and connect one ExtraProgram document */
  create?: Maybe<ExtraProgramCreateInput>;
  /** Connect one existing ExtraProgram document */
  connect?: Maybe<ExtraProgramWhereUniqueInput>;
};

/** An edge in a connection. */
export type ExtraProgramEdge = {
  __typename?: "ExtraProgramEdge";
  /** The item at the end of the edge. */
  node: ExtraProgram;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** Identifies documents */
export type ExtraProgramManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<ExtraProgramWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<ExtraProgramWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<ExtraProgramWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  name_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  name_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  name_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  name_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  name_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  name_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  name_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  name_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  name_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  image?: Maybe<AssetWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

export enum ExtraProgramOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  NameAsc = "name_ASC",
  NameDesc = "name_DESC",
  SubtitleAsc = "subtitle_ASC",
  SubtitleDesc = "subtitle_DESC",
}

export type ExtraProgramUpdateInput = {
  name?: Maybe<Scalars["String"]>;
  /** subtitle input for default locale (en) */
  subtitle?: Maybe<Scalars["String"]>;
  image?: Maybe<AssetUpdateOneInlineInput>;
  /** Manage document localizations */
  localizations?: Maybe<ExtraProgramUpdateLocalizationsInput>;
};

export type ExtraProgramUpdateLocalizationDataInput = {
  subtitle?: Maybe<Scalars["String"]>;
};

export type ExtraProgramUpdateLocalizationInput = {
  data: ExtraProgramUpdateLocalizationDataInput;
  locale: Locale;
};

export type ExtraProgramUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: Maybe<Array<ExtraProgramCreateLocalizationInput>>;
  /** Localizations to update */
  update?: Maybe<Array<ExtraProgramUpdateLocalizationInput>>;
  upsert?: Maybe<Array<ExtraProgramUpsertLocalizationInput>>;
  /** Localizations to delete */
  delete?: Maybe<Array<Locale>>;
};

export type ExtraProgramUpdateManyInlineInput = {
  /** Create and connect multiple ExtraProgram documents */
  create?: Maybe<Array<ExtraProgramCreateInput>>;
  /** Connect multiple existing ExtraProgram documents */
  connect?: Maybe<Array<ExtraProgramConnectInput>>;
  /** Override currently-connected documents with multiple existing ExtraProgram documents */
  set?: Maybe<Array<ExtraProgramWhereUniqueInput>>;
  /** Update multiple ExtraProgram documents */
  update?: Maybe<Array<ExtraProgramUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple ExtraProgram documents */
  upsert?: Maybe<Array<ExtraProgramUpsertWithNestedWhereUniqueInput>>;
  /** Disconnect multiple ExtraProgram documents */
  disconnect?: Maybe<Array<ExtraProgramWhereUniqueInput>>;
  /** Delete multiple ExtraProgram documents */
  delete?: Maybe<Array<ExtraProgramWhereUniqueInput>>;
};

export type ExtraProgramUpdateManyInput = {
  name?: Maybe<Scalars["String"]>;
  /** subtitle input for default locale (en) */
  subtitle?: Maybe<Scalars["String"]>;
  /** Optional updates to localizations */
  localizations?: Maybe<ExtraProgramUpdateManyLocalizationsInput>;
};

export type ExtraProgramUpdateManyLocalizationDataInput = {
  subtitle?: Maybe<Scalars["String"]>;
};

export type ExtraProgramUpdateManyLocalizationInput = {
  data: ExtraProgramUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type ExtraProgramUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: Maybe<Array<ExtraProgramUpdateManyLocalizationInput>>;
};

export type ExtraProgramUpdateManyWithNestedWhereInput = {
  /** Document search */
  where: ExtraProgramWhereInput;
  /** Update many input */
  data: ExtraProgramUpdateManyInput;
};

export type ExtraProgramUpdateOneInlineInput = {
  /** Create and connect one ExtraProgram document */
  create?: Maybe<ExtraProgramCreateInput>;
  /** Update single ExtraProgram document */
  update?: Maybe<ExtraProgramUpdateWithNestedWhereUniqueInput>;
  /** Upsert single ExtraProgram document */
  upsert?: Maybe<ExtraProgramUpsertWithNestedWhereUniqueInput>;
  /** Connect existing ExtraProgram document */
  connect?: Maybe<ExtraProgramWhereUniqueInput>;
  /** Disconnect currently connected ExtraProgram document */
  disconnect?: Maybe<Scalars["Boolean"]>;
  /** Delete currently connected ExtraProgram document */
  delete?: Maybe<Scalars["Boolean"]>;
};

export type ExtraProgramUpdateWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: ExtraProgramWhereUniqueInput;
  /** Document to update */
  data: ExtraProgramUpdateInput;
};

export type ExtraProgramUpsertInput = {
  /** Create document if it didn't exist */
  create: ExtraProgramCreateInput;
  /** Update document if it exists */
  update: ExtraProgramUpdateInput;
};

export type ExtraProgramUpsertLocalizationInput = {
  update: ExtraProgramUpdateLocalizationDataInput;
  create: ExtraProgramCreateLocalizationDataInput;
  locale: Locale;
};

export type ExtraProgramUpsertWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: ExtraProgramWhereUniqueInput;
  /** Upsert data */
  data: ExtraProgramUpsertInput;
};

/** Identifies documents */
export type ExtraProgramWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<ExtraProgramWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<ExtraProgramWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<ExtraProgramWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  name_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  name_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  name_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  name_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  name_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  name_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  name_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  name_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  name_not_ends_with?: Maybe<Scalars["String"]>;
  subtitle?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  subtitle_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  subtitle_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  subtitle_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  subtitle_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  subtitle_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  subtitle_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  subtitle_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  subtitle_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  subtitle_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  image?: Maybe<AssetWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

/** References ExtraProgram record uniquely */
export type ExtraProgramWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

export type GeneralFaq = Node & {
  __typename?: "GeneralFaq";
  /** System stage field */
  stage: Stage;
  /** System Locale field */
  locale: Locale;
  /** Get the other localizations for this document */
  localizations: Array<GeneralFaq>;
  /** Get the document in other stages */
  documentInStages: Array<GeneralFaq>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  question?: Maybe<Scalars["String"]>;
  answer?: Maybe<Scalars["String"]>;
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  scheduledIn: Array<ScheduledOperation>;
  /** List of GeneralFaq versions */
  history: Array<Version>;
};

export type GeneralFaqLocalizationsArgs = {
  locales?: Array<Locale>;
  includeCurrent?: Scalars["Boolean"];
};

export type GeneralFaqDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

export type GeneralFaqCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type GeneralFaqUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type GeneralFaqPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type GeneralFaqCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type GeneralFaqUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type GeneralFaqPublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type GeneralFaqScheduledInArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

export type GeneralFaqHistoryArgs = {
  limit?: Scalars["Int"];
  skip?: Scalars["Int"];
  stageOverride?: Maybe<Stage>;
};

export type GeneralFaqConnectInput = {
  /** Document to connect */
  where: GeneralFaqWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type GeneralFaqConnection = {
  __typename?: "GeneralFaqConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<GeneralFaqEdge>;
  aggregate: Aggregate;
};

export type GeneralFaqCreateInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** question input for default locale (en) */
  question?: Maybe<Scalars["String"]>;
  /** answer input for default locale (en) */
  answer?: Maybe<Scalars["String"]>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: Maybe<GeneralFaqCreateLocalizationsInput>;
};

export type GeneralFaqCreateLocalizationDataInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  question?: Maybe<Scalars["String"]>;
  answer?: Maybe<Scalars["String"]>;
};

export type GeneralFaqCreateLocalizationInput = {
  /** Localization input */
  data: GeneralFaqCreateLocalizationDataInput;
  locale: Locale;
};

export type GeneralFaqCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: Maybe<Array<GeneralFaqCreateLocalizationInput>>;
};

export type GeneralFaqCreateManyInlineInput = {
  /** Create and connect multiple existing GeneralFaq documents */
  create?: Maybe<Array<GeneralFaqCreateInput>>;
  /** Connect multiple existing GeneralFaq documents */
  connect?: Maybe<Array<GeneralFaqWhereUniqueInput>>;
};

export type GeneralFaqCreateOneInlineInput = {
  /** Create and connect one GeneralFaq document */
  create?: Maybe<GeneralFaqCreateInput>;
  /** Connect one existing GeneralFaq document */
  connect?: Maybe<GeneralFaqWhereUniqueInput>;
};

/** An edge in a connection. */
export type GeneralFaqEdge = {
  __typename?: "GeneralFaqEdge";
  /** The item at the end of the edge. */
  node: GeneralFaq;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** Identifies documents */
export type GeneralFaqManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<GeneralFaqWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<GeneralFaqWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<GeneralFaqWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

export enum GeneralFaqOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  QuestionAsc = "question_ASC",
  QuestionDesc = "question_DESC",
  AnswerAsc = "answer_ASC",
  AnswerDesc = "answer_DESC",
}

export type GeneralFaqUpdateInput = {
  /** question input for default locale (en) */
  question?: Maybe<Scalars["String"]>;
  /** answer input for default locale (en) */
  answer?: Maybe<Scalars["String"]>;
  /** Manage document localizations */
  localizations?: Maybe<GeneralFaqUpdateLocalizationsInput>;
};

export type GeneralFaqUpdateLocalizationDataInput = {
  question?: Maybe<Scalars["String"]>;
  answer?: Maybe<Scalars["String"]>;
};

export type GeneralFaqUpdateLocalizationInput = {
  data: GeneralFaqUpdateLocalizationDataInput;
  locale: Locale;
};

export type GeneralFaqUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: Maybe<Array<GeneralFaqCreateLocalizationInput>>;
  /** Localizations to update */
  update?: Maybe<Array<GeneralFaqUpdateLocalizationInput>>;
  upsert?: Maybe<Array<GeneralFaqUpsertLocalizationInput>>;
  /** Localizations to delete */
  delete?: Maybe<Array<Locale>>;
};

export type GeneralFaqUpdateManyInlineInput = {
  /** Create and connect multiple GeneralFaq documents */
  create?: Maybe<Array<GeneralFaqCreateInput>>;
  /** Connect multiple existing GeneralFaq documents */
  connect?: Maybe<Array<GeneralFaqConnectInput>>;
  /** Override currently-connected documents with multiple existing GeneralFaq documents */
  set?: Maybe<Array<GeneralFaqWhereUniqueInput>>;
  /** Update multiple GeneralFaq documents */
  update?: Maybe<Array<GeneralFaqUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple GeneralFaq documents */
  upsert?: Maybe<Array<GeneralFaqUpsertWithNestedWhereUniqueInput>>;
  /** Disconnect multiple GeneralFaq documents */
  disconnect?: Maybe<Array<GeneralFaqWhereUniqueInput>>;
  /** Delete multiple GeneralFaq documents */
  delete?: Maybe<Array<GeneralFaqWhereUniqueInput>>;
};

export type GeneralFaqUpdateManyInput = {
  /** question input for default locale (en) */
  question?: Maybe<Scalars["String"]>;
  /** answer input for default locale (en) */
  answer?: Maybe<Scalars["String"]>;
  /** Optional updates to localizations */
  localizations?: Maybe<GeneralFaqUpdateManyLocalizationsInput>;
};

export type GeneralFaqUpdateManyLocalizationDataInput = {
  question?: Maybe<Scalars["String"]>;
  answer?: Maybe<Scalars["String"]>;
};

export type GeneralFaqUpdateManyLocalizationInput = {
  data: GeneralFaqUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type GeneralFaqUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: Maybe<Array<GeneralFaqUpdateManyLocalizationInput>>;
};

export type GeneralFaqUpdateManyWithNestedWhereInput = {
  /** Document search */
  where: GeneralFaqWhereInput;
  /** Update many input */
  data: GeneralFaqUpdateManyInput;
};

export type GeneralFaqUpdateOneInlineInput = {
  /** Create and connect one GeneralFaq document */
  create?: Maybe<GeneralFaqCreateInput>;
  /** Update single GeneralFaq document */
  update?: Maybe<GeneralFaqUpdateWithNestedWhereUniqueInput>;
  /** Upsert single GeneralFaq document */
  upsert?: Maybe<GeneralFaqUpsertWithNestedWhereUniqueInput>;
  /** Connect existing GeneralFaq document */
  connect?: Maybe<GeneralFaqWhereUniqueInput>;
  /** Disconnect currently connected GeneralFaq document */
  disconnect?: Maybe<Scalars["Boolean"]>;
  /** Delete currently connected GeneralFaq document */
  delete?: Maybe<Scalars["Boolean"]>;
};

export type GeneralFaqUpdateWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: GeneralFaqWhereUniqueInput;
  /** Document to update */
  data: GeneralFaqUpdateInput;
};

export type GeneralFaqUpsertInput = {
  /** Create document if it didn't exist */
  create: GeneralFaqCreateInput;
  /** Update document if it exists */
  update: GeneralFaqUpdateInput;
};

export type GeneralFaqUpsertLocalizationInput = {
  update: GeneralFaqUpdateLocalizationDataInput;
  create: GeneralFaqCreateLocalizationDataInput;
  locale: Locale;
};

export type GeneralFaqUpsertWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: GeneralFaqWhereUniqueInput;
  /** Upsert data */
  data: GeneralFaqUpsertInput;
};

/** Identifies documents */
export type GeneralFaqWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<GeneralFaqWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<GeneralFaqWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<GeneralFaqWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  question?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  question_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  question_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  question_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  question_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  question_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  question_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  question_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  question_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  question_not_ends_with?: Maybe<Scalars["String"]>;
  answer?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  answer_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  answer_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  answer_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  answer_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  answer_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  answer_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  answer_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  answer_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  answer_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

/** References GeneralFaq record uniquely */
export type GeneralFaqWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

export enum ImageFit {
  /** Resizes the image to fit within the specified parameters without distorting, cropping, or changing the aspect ratio. */
  Clip = "clip",
  /** Resizes the image to fit the specified parameters exactly by removing any parts of the image that don't fit within the boundaries. */
  Crop = "crop",
  /** Resizes the image to fit the specified parameters exactly by scaling the image to the desired size. The aspect ratio of the image is not respected and the image can be distorted using this method. */
  Scale = "scale",
  /** Resizes the image to fit within the parameters, but as opposed to 'fit:clip' will not scale the image if the image is smaller than the output size. */
  Max = "max",
}

export type ImageResizeInput = {
  /** The width in pixels to resize the image to. The value must be an integer from 1 to 10000. */
  width?: Maybe<Scalars["Int"]>;
  /** The height in pixels to resize the image to. The value must be an integer from 1 to 10000. */
  height?: Maybe<Scalars["Int"]>;
  /** The default value for the fit parameter is fit:clip. */
  fit?: Maybe<ImageFit>;
};

/** Transformations for Images */
export type ImageTransformationInput = {
  /** Resizes the image */
  resize?: Maybe<ImageResizeInput>;
};

export type Job = Node & {
  __typename?: "Job";
  /** System stage field */
  stage: Stage;
  /** System Locale field */
  locale: Locale;
  /** Get the other localizations for this document */
  localizations: Array<Job>;
  /** Get the document in other stages */
  documentInStages: Array<Job>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** Short name of the job */
  title: Scalars["String"];
  /** Detailed description of the tasks */
  description: Scalars["String"];
  /** Discount, for example 80 EUR or free ticket */
  discount: Scalars["String"];
  /** Where in the job listing this should appear. Position of 1 will appear first, 2 second, ... */
  position?: Maybe<Scalars["Int"]>;
  /** How many people do we need for this job? */
  neededPeople?: Maybe<Scalars["Int"]>;
  /** Who will do this job */
  commitedWorkers: Array<Scalars["String"]>;
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  /** The color used for the discount badge */
  color: DiscountColor;
  scheduledIn: Array<ScheduledOperation>;
  /** List of Job versions */
  history: Array<Version>;
};

export type JobLocalizationsArgs = {
  locales?: Array<Locale>;
  includeCurrent?: Scalars["Boolean"];
};

export type JobDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

export type JobCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type JobUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type JobPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type JobCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type JobUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type JobPublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type JobScheduledInArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

export type JobHistoryArgs = {
  limit?: Scalars["Int"];
  skip?: Scalars["Int"];
  stageOverride?: Maybe<Stage>;
};

export type JobConnectInput = {
  /** Document to connect */
  where: JobWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type JobConnection = {
  __typename?: "JobConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<JobEdge>;
  aggregate: Aggregate;
};

export type JobCreateInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** title input for default locale (en) */
  title: Scalars["String"];
  /** description input for default locale (en) */
  description: Scalars["String"];
  /** discount input for default locale (en) */
  discount: Scalars["String"];
  position?: Maybe<Scalars["Int"]>;
  neededPeople?: Maybe<Scalars["Int"]>;
  commitedWorkers?: Maybe<Array<Scalars["String"]>>;
  color: DiscountColor;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: Maybe<JobCreateLocalizationsInput>;
};

export type JobCreateLocalizationDataInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  title: Scalars["String"];
  description: Scalars["String"];
  discount: Scalars["String"];
};

export type JobCreateLocalizationInput = {
  /** Localization input */
  data: JobCreateLocalizationDataInput;
  locale: Locale;
};

export type JobCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: Maybe<Array<JobCreateLocalizationInput>>;
};

export type JobCreateManyInlineInput = {
  /** Create and connect multiple existing Job documents */
  create?: Maybe<Array<JobCreateInput>>;
  /** Connect multiple existing Job documents */
  connect?: Maybe<Array<JobWhereUniqueInput>>;
};

export type JobCreateOneInlineInput = {
  /** Create and connect one Job document */
  create?: Maybe<JobCreateInput>;
  /** Connect one existing Job document */
  connect?: Maybe<JobWhereUniqueInput>;
};

/** An edge in a connection. */
export type JobEdge = {
  __typename?: "JobEdge";
  /** The item at the end of the edge. */
  node: Job;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

export type JobFaq = Node & {
  __typename?: "JobFaq";
  /** System stage field */
  stage: Stage;
  /** System Locale field */
  locale: Locale;
  /** Get the other localizations for this document */
  localizations: Array<JobFaq>;
  /** Get the document in other stages */
  documentInStages: Array<JobFaq>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  question: Scalars["String"];
  answer: Scalars["String"];
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  scheduledIn: Array<ScheduledOperation>;
  /** List of JobFaq versions */
  history: Array<Version>;
};

export type JobFaqLocalizationsArgs = {
  locales?: Array<Locale>;
  includeCurrent?: Scalars["Boolean"];
};

export type JobFaqDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

export type JobFaqCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type JobFaqUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type JobFaqPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type JobFaqCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type JobFaqUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type JobFaqPublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type JobFaqScheduledInArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

export type JobFaqHistoryArgs = {
  limit?: Scalars["Int"];
  skip?: Scalars["Int"];
  stageOverride?: Maybe<Stage>;
};

export type JobFaqConnectInput = {
  /** Document to connect */
  where: JobFaqWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type JobFaqConnection = {
  __typename?: "JobFaqConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<JobFaqEdge>;
  aggregate: Aggregate;
};

export type JobFaqCreateInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** question input for default locale (en) */
  question: Scalars["String"];
  /** answer input for default locale (en) */
  answer: Scalars["String"];
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: Maybe<JobFaqCreateLocalizationsInput>;
};

export type JobFaqCreateLocalizationDataInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  question: Scalars["String"];
  answer: Scalars["String"];
};

export type JobFaqCreateLocalizationInput = {
  /** Localization input */
  data: JobFaqCreateLocalizationDataInput;
  locale: Locale;
};

export type JobFaqCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: Maybe<Array<JobFaqCreateLocalizationInput>>;
};

export type JobFaqCreateManyInlineInput = {
  /** Create and connect multiple existing JobFaq documents */
  create?: Maybe<Array<JobFaqCreateInput>>;
  /** Connect multiple existing JobFaq documents */
  connect?: Maybe<Array<JobFaqWhereUniqueInput>>;
};

export type JobFaqCreateOneInlineInput = {
  /** Create and connect one JobFaq document */
  create?: Maybe<JobFaqCreateInput>;
  /** Connect one existing JobFaq document */
  connect?: Maybe<JobFaqWhereUniqueInput>;
};

/** An edge in a connection. */
export type JobFaqEdge = {
  __typename?: "JobFaqEdge";
  /** The item at the end of the edge. */
  node: JobFaq;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** Identifies documents */
export type JobFaqManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<JobFaqWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<JobFaqWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<JobFaqWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

export enum JobFaqOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  QuestionAsc = "question_ASC",
  QuestionDesc = "question_DESC",
  AnswerAsc = "answer_ASC",
  AnswerDesc = "answer_DESC",
}

export type JobFaqUpdateInput = {
  /** question input for default locale (en) */
  question?: Maybe<Scalars["String"]>;
  /** answer input for default locale (en) */
  answer?: Maybe<Scalars["String"]>;
  /** Manage document localizations */
  localizations?: Maybe<JobFaqUpdateLocalizationsInput>;
};

export type JobFaqUpdateLocalizationDataInput = {
  question?: Maybe<Scalars["String"]>;
  answer?: Maybe<Scalars["String"]>;
};

export type JobFaqUpdateLocalizationInput = {
  data: JobFaqUpdateLocalizationDataInput;
  locale: Locale;
};

export type JobFaqUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: Maybe<Array<JobFaqCreateLocalizationInput>>;
  /** Localizations to update */
  update?: Maybe<Array<JobFaqUpdateLocalizationInput>>;
  upsert?: Maybe<Array<JobFaqUpsertLocalizationInput>>;
  /** Localizations to delete */
  delete?: Maybe<Array<Locale>>;
};

export type JobFaqUpdateManyInlineInput = {
  /** Create and connect multiple JobFaq documents */
  create?: Maybe<Array<JobFaqCreateInput>>;
  /** Connect multiple existing JobFaq documents */
  connect?: Maybe<Array<JobFaqConnectInput>>;
  /** Override currently-connected documents with multiple existing JobFaq documents */
  set?: Maybe<Array<JobFaqWhereUniqueInput>>;
  /** Update multiple JobFaq documents */
  update?: Maybe<Array<JobFaqUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple JobFaq documents */
  upsert?: Maybe<Array<JobFaqUpsertWithNestedWhereUniqueInput>>;
  /** Disconnect multiple JobFaq documents */
  disconnect?: Maybe<Array<JobFaqWhereUniqueInput>>;
  /** Delete multiple JobFaq documents */
  delete?: Maybe<Array<JobFaqWhereUniqueInput>>;
};

export type JobFaqUpdateManyInput = {
  /** question input for default locale (en) */
  question?: Maybe<Scalars["String"]>;
  /** answer input for default locale (en) */
  answer?: Maybe<Scalars["String"]>;
  /** Optional updates to localizations */
  localizations?: Maybe<JobFaqUpdateManyLocalizationsInput>;
};

export type JobFaqUpdateManyLocalizationDataInput = {
  question?: Maybe<Scalars["String"]>;
  answer?: Maybe<Scalars["String"]>;
};

export type JobFaqUpdateManyLocalizationInput = {
  data: JobFaqUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type JobFaqUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: Maybe<Array<JobFaqUpdateManyLocalizationInput>>;
};

export type JobFaqUpdateManyWithNestedWhereInput = {
  /** Document search */
  where: JobFaqWhereInput;
  /** Update many input */
  data: JobFaqUpdateManyInput;
};

export type JobFaqUpdateOneInlineInput = {
  /** Create and connect one JobFaq document */
  create?: Maybe<JobFaqCreateInput>;
  /** Update single JobFaq document */
  update?: Maybe<JobFaqUpdateWithNestedWhereUniqueInput>;
  /** Upsert single JobFaq document */
  upsert?: Maybe<JobFaqUpsertWithNestedWhereUniqueInput>;
  /** Connect existing JobFaq document */
  connect?: Maybe<JobFaqWhereUniqueInput>;
  /** Disconnect currently connected JobFaq document */
  disconnect?: Maybe<Scalars["Boolean"]>;
  /** Delete currently connected JobFaq document */
  delete?: Maybe<Scalars["Boolean"]>;
};

export type JobFaqUpdateWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: JobFaqWhereUniqueInput;
  /** Document to update */
  data: JobFaqUpdateInput;
};

export type JobFaqUpsertInput = {
  /** Create document if it didn't exist */
  create: JobFaqCreateInput;
  /** Update document if it exists */
  update: JobFaqUpdateInput;
};

export type JobFaqUpsertLocalizationInput = {
  update: JobFaqUpdateLocalizationDataInput;
  create: JobFaqCreateLocalizationDataInput;
  locale: Locale;
};

export type JobFaqUpsertWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: JobFaqWhereUniqueInput;
  /** Upsert data */
  data: JobFaqUpsertInput;
};

/** Identifies documents */
export type JobFaqWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<JobFaqWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<JobFaqWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<JobFaqWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  question?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  question_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  question_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  question_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  question_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  question_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  question_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  question_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  question_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  question_not_ends_with?: Maybe<Scalars["String"]>;
  answer?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  answer_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  answer_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  answer_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  answer_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  answer_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  answer_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  answer_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  answer_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  answer_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

/** References JobFaq record uniquely */
export type JobFaqWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

/** Identifies documents */
export type JobManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<JobWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<JobWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<JobWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  position?: Maybe<Scalars["Int"]>;
  /** All values that are not equal to given value. */
  position_not?: Maybe<Scalars["Int"]>;
  /** All values that are contained in given list. */
  position_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values that are not contained in given list. */
  position_not_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values less than the given value. */
  position_lt?: Maybe<Scalars["Int"]>;
  /** All values less than or equal the given value. */
  position_lte?: Maybe<Scalars["Int"]>;
  /** All values greater than the given value. */
  position_gt?: Maybe<Scalars["Int"]>;
  /** All values greater than or equal the given value. */
  position_gte?: Maybe<Scalars["Int"]>;
  neededPeople?: Maybe<Scalars["Int"]>;
  /** All values that are not equal to given value. */
  neededPeople_not?: Maybe<Scalars["Int"]>;
  /** All values that are contained in given list. */
  neededPeople_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values that are not contained in given list. */
  neededPeople_not_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values less than the given value. */
  neededPeople_lt?: Maybe<Scalars["Int"]>;
  /** All values less than or equal the given value. */
  neededPeople_lte?: Maybe<Scalars["Int"]>;
  /** All values greater than the given value. */
  neededPeople_gt?: Maybe<Scalars["Int"]>;
  /** All values greater than or equal the given value. */
  neededPeople_gte?: Maybe<Scalars["Int"]>;
  /** Matches if the field array contains *all* items provided to the filter and order does match */
  commitedWorkers?: Maybe<Array<Scalars["String"]>>;
  /** Matches if the field array does not contains *all* items provided to the filter or order does not match */
  commitedWorkers_not?: Maybe<Array<Scalars["String"]>>;
  /** Matches if the field array contains *all* items provided to the filter */
  commitedWorkers_contains_all?: Maybe<Array<Scalars["String"]>>;
  /** Matches if the field array contains at least one item provided to the filter */
  commitedWorkers_contains_some?: Maybe<Array<Scalars["String"]>>;
  /** Matches if the field array does not contain any of the items provided to the filter */
  commitedWorkers_contains_none?: Maybe<Array<Scalars["String"]>>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  color?: Maybe<DiscountColor>;
  /** All values that are not equal to given value. */
  color_not?: Maybe<DiscountColor>;
  /** All values that are contained in given list. */
  color_in?: Maybe<Array<DiscountColor>>;
  /** All values that are not contained in given list. */
  color_not_in?: Maybe<Array<DiscountColor>>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

export enum JobOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  TitleAsc = "title_ASC",
  TitleDesc = "title_DESC",
  DescriptionAsc = "description_ASC",
  DescriptionDesc = "description_DESC",
  DiscountAsc = "discount_ASC",
  DiscountDesc = "discount_DESC",
  PositionAsc = "position_ASC",
  PositionDesc = "position_DESC",
  NeededPeopleAsc = "neededPeople_ASC",
  NeededPeopleDesc = "neededPeople_DESC",
  CommitedWorkersAsc = "commitedWorkers_ASC",
  CommitedWorkersDesc = "commitedWorkers_DESC",
  ColorAsc = "color_ASC",
  ColorDesc = "color_DESC",
}

export type JobUpdateInput = {
  /** title input for default locale (en) */
  title?: Maybe<Scalars["String"]>;
  /** description input for default locale (en) */
  description?: Maybe<Scalars["String"]>;
  /** discount input for default locale (en) */
  discount?: Maybe<Scalars["String"]>;
  position?: Maybe<Scalars["Int"]>;
  neededPeople?: Maybe<Scalars["Int"]>;
  commitedWorkers?: Maybe<Array<Scalars["String"]>>;
  color?: Maybe<DiscountColor>;
  /** Manage document localizations */
  localizations?: Maybe<JobUpdateLocalizationsInput>;
};

export type JobUpdateLocalizationDataInput = {
  title?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  discount?: Maybe<Scalars["String"]>;
};

export type JobUpdateLocalizationInput = {
  data: JobUpdateLocalizationDataInput;
  locale: Locale;
};

export type JobUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: Maybe<Array<JobCreateLocalizationInput>>;
  /** Localizations to update */
  update?: Maybe<Array<JobUpdateLocalizationInput>>;
  upsert?: Maybe<Array<JobUpsertLocalizationInput>>;
  /** Localizations to delete */
  delete?: Maybe<Array<Locale>>;
};

export type JobUpdateManyInlineInput = {
  /** Create and connect multiple Job documents */
  create?: Maybe<Array<JobCreateInput>>;
  /** Connect multiple existing Job documents */
  connect?: Maybe<Array<JobConnectInput>>;
  /** Override currently-connected documents with multiple existing Job documents */
  set?: Maybe<Array<JobWhereUniqueInput>>;
  /** Update multiple Job documents */
  update?: Maybe<Array<JobUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple Job documents */
  upsert?: Maybe<Array<JobUpsertWithNestedWhereUniqueInput>>;
  /** Disconnect multiple Job documents */
  disconnect?: Maybe<Array<JobWhereUniqueInput>>;
  /** Delete multiple Job documents */
  delete?: Maybe<Array<JobWhereUniqueInput>>;
};

export type JobUpdateManyInput = {
  /** title input for default locale (en) */
  title?: Maybe<Scalars["String"]>;
  /** description input for default locale (en) */
  description?: Maybe<Scalars["String"]>;
  /** discount input for default locale (en) */
  discount?: Maybe<Scalars["String"]>;
  position?: Maybe<Scalars["Int"]>;
  neededPeople?: Maybe<Scalars["Int"]>;
  commitedWorkers?: Maybe<Array<Scalars["String"]>>;
  color?: Maybe<DiscountColor>;
  /** Optional updates to localizations */
  localizations?: Maybe<JobUpdateManyLocalizationsInput>;
};

export type JobUpdateManyLocalizationDataInput = {
  title?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  discount?: Maybe<Scalars["String"]>;
};

export type JobUpdateManyLocalizationInput = {
  data: JobUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type JobUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: Maybe<Array<JobUpdateManyLocalizationInput>>;
};

export type JobUpdateManyWithNestedWhereInput = {
  /** Document search */
  where: JobWhereInput;
  /** Update many input */
  data: JobUpdateManyInput;
};

export type JobUpdateOneInlineInput = {
  /** Create and connect one Job document */
  create?: Maybe<JobCreateInput>;
  /** Update single Job document */
  update?: Maybe<JobUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Job document */
  upsert?: Maybe<JobUpsertWithNestedWhereUniqueInput>;
  /** Connect existing Job document */
  connect?: Maybe<JobWhereUniqueInput>;
  /** Disconnect currently connected Job document */
  disconnect?: Maybe<Scalars["Boolean"]>;
  /** Delete currently connected Job document */
  delete?: Maybe<Scalars["Boolean"]>;
};

export type JobUpdateWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: JobWhereUniqueInput;
  /** Document to update */
  data: JobUpdateInput;
};

export type JobUpsertInput = {
  /** Create document if it didn't exist */
  create: JobCreateInput;
  /** Update document if it exists */
  update: JobUpdateInput;
};

export type JobUpsertLocalizationInput = {
  update: JobUpdateLocalizationDataInput;
  create: JobCreateLocalizationDataInput;
  locale: Locale;
};

export type JobUpsertWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: JobWhereUniqueInput;
  /** Upsert data */
  data: JobUpsertInput;
};

/** Identifies documents */
export type JobWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<JobWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<JobWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<JobWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  title?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  title_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  title_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  title_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  title_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  title_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  title_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  title_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  title_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  title_not_ends_with?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  description_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  description_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  description_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  description_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  description_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  description_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  description_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  description_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  description_not_ends_with?: Maybe<Scalars["String"]>;
  discount?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  discount_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  discount_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  discount_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  discount_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  discount_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  discount_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  discount_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  discount_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  discount_not_ends_with?: Maybe<Scalars["String"]>;
  position?: Maybe<Scalars["Int"]>;
  /** All values that are not equal to given value. */
  position_not?: Maybe<Scalars["Int"]>;
  /** All values that are contained in given list. */
  position_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values that are not contained in given list. */
  position_not_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values less than the given value. */
  position_lt?: Maybe<Scalars["Int"]>;
  /** All values less than or equal the given value. */
  position_lte?: Maybe<Scalars["Int"]>;
  /** All values greater than the given value. */
  position_gt?: Maybe<Scalars["Int"]>;
  /** All values greater than or equal the given value. */
  position_gte?: Maybe<Scalars["Int"]>;
  neededPeople?: Maybe<Scalars["Int"]>;
  /** All values that are not equal to given value. */
  neededPeople_not?: Maybe<Scalars["Int"]>;
  /** All values that are contained in given list. */
  neededPeople_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values that are not contained in given list. */
  neededPeople_not_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values less than the given value. */
  neededPeople_lt?: Maybe<Scalars["Int"]>;
  /** All values less than or equal the given value. */
  neededPeople_lte?: Maybe<Scalars["Int"]>;
  /** All values greater than the given value. */
  neededPeople_gt?: Maybe<Scalars["Int"]>;
  /** All values greater than or equal the given value. */
  neededPeople_gte?: Maybe<Scalars["Int"]>;
  /** Matches if the field array contains *all* items provided to the filter and order does match */
  commitedWorkers?: Maybe<Array<Scalars["String"]>>;
  /** Matches if the field array does not contains *all* items provided to the filter or order does not match */
  commitedWorkers_not?: Maybe<Array<Scalars["String"]>>;
  /** Matches if the field array contains *all* items provided to the filter */
  commitedWorkers_contains_all?: Maybe<Array<Scalars["String"]>>;
  /** Matches if the field array contains at least one item provided to the filter */
  commitedWorkers_contains_some?: Maybe<Array<Scalars["String"]>>;
  /** Matches if the field array does not contain any of the items provided to the filter */
  commitedWorkers_contains_none?: Maybe<Array<Scalars["String"]>>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  color?: Maybe<DiscountColor>;
  /** All values that are not equal to given value. */
  color_not?: Maybe<DiscountColor>;
  /** All values that are contained in given list. */
  color_in?: Maybe<Array<DiscountColor>>;
  /** All values that are not contained in given list. */
  color_not_in?: Maybe<Array<DiscountColor>>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

/** References Job record uniquely */
export type JobWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

/** Locale system enumeration */
export enum Locale {
  /** System locale */
  En = "en",
  De = "de",
}

/** Representing a geolocation point with latitude and longitude */
export type Location = {
  __typename?: "Location";
  latitude: Scalars["Float"];
  longitude: Scalars["Float"];
  distance: Scalars["Float"];
};

/** Representing a geolocation point with latitude and longitude */
export type LocationDistanceArgs = {
  from: LocationInput;
};

/** Input for a geolocation point with latitude and longitude */
export type LocationInput = {
  latitude: Scalars["Float"];
  longitude: Scalars["Float"];
};

export type Mutation = {
  __typename?: "Mutation";
  /** Create one ticket */
  createTicket?: Maybe<Ticket>;
  /** Update one ticket */
  updateTicket?: Maybe<Ticket>;
  /** Delete one ticket from _all_ existing stages. Returns deleted document. */
  deleteTicket?: Maybe<Ticket>;
  /** Upsert one ticket */
  upsertTicket?: Maybe<Ticket>;
  /** Publish one ticket */
  publishTicket?: Maybe<Ticket>;
  /** Unpublish one ticket from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishTicket?: Maybe<Ticket>;
  /** Update many Ticket documents */
  updateManyTicketsConnection: TicketConnection;
  /** Delete many Ticket documents, return deleted documents */
  deleteManyTicketsConnection: TicketConnection;
  /** Publish many Ticket documents */
  publishManyTicketsConnection: TicketConnection;
  /** Find many Ticket documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyTicketsConnection: TicketConnection;
  /**
   * Update many tickets
   * @deprecated Please use the new paginated many mutation (updateManyTicketsConnection)
   */
  updateManyTickets: BatchPayload;
  /**
   * Delete many Ticket documents
   * @deprecated Please use the new paginated many mutation (deleteManyTicketsConnection)
   */
  deleteManyTickets: BatchPayload;
  /**
   * Publish many Ticket documents
   * @deprecated Please use the new paginated many mutation (publishManyTicketsConnection)
   */
  publishManyTickets: BatchPayload;
  /**
   * Unpublish many Ticket documents
   * @deprecated Please use the new paginated many mutation (unpublishManyTicketsConnection)
   */
  unpublishManyTickets: BatchPayload;
  /** Schedule to publish one ticket */
  schedulePublishTicket?: Maybe<Ticket>;
  /** Unpublish one ticket from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishTicket?: Maybe<Ticket>;
  /**
   * Create one asset
   * @deprecated Asset mutations will be overhauled soon
   */
  createAsset?: Maybe<Asset>;
  /** Update one asset */
  updateAsset?: Maybe<Asset>;
  /** Delete one asset from _all_ existing stages. Returns deleted document. */
  deleteAsset?: Maybe<Asset>;
  /** Upsert one asset */
  upsertAsset?: Maybe<Asset>;
  /** Publish one asset */
  publishAsset?: Maybe<Asset>;
  /** Unpublish one asset from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishAsset?: Maybe<Asset>;
  /** Update many Asset documents */
  updateManyAssetsConnection: AssetConnection;
  /** Delete many Asset documents, return deleted documents */
  deleteManyAssetsConnection: AssetConnection;
  /** Publish many Asset documents */
  publishManyAssetsConnection: AssetConnection;
  /** Find many Asset documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyAssetsConnection: AssetConnection;
  /**
   * Update many assets
   * @deprecated Please use the new paginated many mutation (updateManyAssetsConnection)
   */
  updateManyAssets: BatchPayload;
  /**
   * Delete many Asset documents
   * @deprecated Please use the new paginated many mutation (deleteManyAssetsConnection)
   */
  deleteManyAssets: BatchPayload;
  /**
   * Publish many Asset documents
   * @deprecated Please use the new paginated many mutation (publishManyAssetsConnection)
   */
  publishManyAssets: BatchPayload;
  /**
   * Unpublish many Asset documents
   * @deprecated Please use the new paginated many mutation (unpublishManyAssetsConnection)
   */
  unpublishManyAssets: BatchPayload;
  /** Schedule to publish one asset */
  schedulePublishAsset?: Maybe<Asset>;
  /** Unpublish one asset from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishAsset?: Maybe<Asset>;
  /** Create one sponsor */
  createSponsor?: Maybe<Sponsor>;
  /** Update one sponsor */
  updateSponsor?: Maybe<Sponsor>;
  /** Delete one sponsor from _all_ existing stages. Returns deleted document. */
  deleteSponsor?: Maybe<Sponsor>;
  /** Upsert one sponsor */
  upsertSponsor?: Maybe<Sponsor>;
  /** Publish one sponsor */
  publishSponsor?: Maybe<Sponsor>;
  /** Unpublish one sponsor from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishSponsor?: Maybe<Sponsor>;
  /** Update many Sponsor documents */
  updateManySponsorsConnection: SponsorConnection;
  /** Delete many Sponsor documents, return deleted documents */
  deleteManySponsorsConnection: SponsorConnection;
  /** Publish many Sponsor documents */
  publishManySponsorsConnection: SponsorConnection;
  /** Find many Sponsor documents that match criteria in specified stage and unpublish from target stages */
  unpublishManySponsorsConnection: SponsorConnection;
  /**
   * Update many sponsors
   * @deprecated Please use the new paginated many mutation (updateManySponsorsConnection)
   */
  updateManySponsors: BatchPayload;
  /**
   * Delete many Sponsor documents
   * @deprecated Please use the new paginated many mutation (deleteManySponsorsConnection)
   */
  deleteManySponsors: BatchPayload;
  /**
   * Publish many Sponsor documents
   * @deprecated Please use the new paginated many mutation (publishManySponsorsConnection)
   */
  publishManySponsors: BatchPayload;
  /**
   * Unpublish many Sponsor documents
   * @deprecated Please use the new paginated many mutation (unpublishManySponsorsConnection)
   */
  unpublishManySponsors: BatchPayload;
  /** Schedule to publish one sponsor */
  schedulePublishSponsor?: Maybe<Sponsor>;
  /** Unpublish one sponsor from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishSponsor?: Maybe<Sponsor>;
  /** Create one shortInfoText */
  createShortInfoText?: Maybe<ShortInfoText>;
  /** Update one shortInfoText */
  updateShortInfoText?: Maybe<ShortInfoText>;
  /** Delete one shortInfoText from _all_ existing stages. Returns deleted document. */
  deleteShortInfoText?: Maybe<ShortInfoText>;
  /** Upsert one shortInfoText */
  upsertShortInfoText?: Maybe<ShortInfoText>;
  /** Publish one shortInfoText */
  publishShortInfoText?: Maybe<ShortInfoText>;
  /** Unpublish one shortInfoText from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishShortInfoText?: Maybe<ShortInfoText>;
  /** Update many ShortInfoText documents */
  updateManyShortInfoTextsConnection: ShortInfoTextConnection;
  /** Delete many ShortInfoText documents, return deleted documents */
  deleteManyShortInfoTextsConnection: ShortInfoTextConnection;
  /** Publish many ShortInfoText documents */
  publishManyShortInfoTextsConnection: ShortInfoTextConnection;
  /** Find many ShortInfoText documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyShortInfoTextsConnection: ShortInfoTextConnection;
  /**
   * Update many shortInfoTexts
   * @deprecated Please use the new paginated many mutation (updateManyShortInfoTextsConnection)
   */
  updateManyShortInfoTexts: BatchPayload;
  /**
   * Delete many ShortInfoText documents
   * @deprecated Please use the new paginated many mutation (deleteManyShortInfoTextsConnection)
   */
  deleteManyShortInfoTexts: BatchPayload;
  /**
   * Publish many ShortInfoText documents
   * @deprecated Please use the new paginated many mutation (publishManyShortInfoTextsConnection)
   */
  publishManyShortInfoTexts: BatchPayload;
  /**
   * Unpublish many ShortInfoText documents
   * @deprecated Please use the new paginated many mutation (unpublishManyShortInfoTextsConnection)
   */
  unpublishManyShortInfoTexts: BatchPayload;
  /** Schedule to publish one shortInfoText */
  schedulePublishShortInfoText?: Maybe<ShortInfoText>;
  /** Unpublish one shortInfoText from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishShortInfoText?: Maybe<ShortInfoText>;
  /** Create one contact */
  createContact?: Maybe<Contact>;
  /** Update one contact */
  updateContact?: Maybe<Contact>;
  /** Delete one contact from _all_ existing stages. Returns deleted document. */
  deleteContact?: Maybe<Contact>;
  /** Upsert one contact */
  upsertContact?: Maybe<Contact>;
  /** Publish one contact */
  publishContact?: Maybe<Contact>;
  /** Unpublish one contact from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishContact?: Maybe<Contact>;
  /** Update many Contact documents */
  updateManyContactsConnection: ContactConnection;
  /** Delete many Contact documents, return deleted documents */
  deleteManyContactsConnection: ContactConnection;
  /** Publish many Contact documents */
  publishManyContactsConnection: ContactConnection;
  /** Find many Contact documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyContactsConnection: ContactConnection;
  /**
   * Update many contacts
   * @deprecated Please use the new paginated many mutation (updateManyContactsConnection)
   */
  updateManyContacts: BatchPayload;
  /**
   * Delete many Contact documents
   * @deprecated Please use the new paginated many mutation (deleteManyContactsConnection)
   */
  deleteManyContacts: BatchPayload;
  /**
   * Publish many Contact documents
   * @deprecated Please use the new paginated many mutation (publishManyContactsConnection)
   */
  publishManyContacts: BatchPayload;
  /**
   * Unpublish many Contact documents
   * @deprecated Please use the new paginated many mutation (unpublishManyContactsConnection)
   */
  unpublishManyContacts: BatchPayload;
  /** Schedule to publish one contact */
  schedulePublishContact?: Maybe<Contact>;
  /** Unpublish one contact from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishContact?: Maybe<Contact>;
  /** Create one team */
  createTeam?: Maybe<Team>;
  /** Update one team */
  updateTeam?: Maybe<Team>;
  /** Delete one team from _all_ existing stages. Returns deleted document. */
  deleteTeam?: Maybe<Team>;
  /** Upsert one team */
  upsertTeam?: Maybe<Team>;
  /** Publish one team */
  publishTeam?: Maybe<Team>;
  /** Unpublish one team from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishTeam?: Maybe<Team>;
  /** Update many Team documents */
  updateManyTeamsConnection: TeamConnection;
  /** Delete many Team documents, return deleted documents */
  deleteManyTeamsConnection: TeamConnection;
  /** Publish many Team documents */
  publishManyTeamsConnection: TeamConnection;
  /** Find many Team documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyTeamsConnection: TeamConnection;
  /**
   * Update many teams
   * @deprecated Please use the new paginated many mutation (updateManyTeamsConnection)
   */
  updateManyTeams: BatchPayload;
  /**
   * Delete many Team documents
   * @deprecated Please use the new paginated many mutation (deleteManyTeamsConnection)
   */
  deleteManyTeams: BatchPayload;
  /**
   * Publish many Team documents
   * @deprecated Please use the new paginated many mutation (publishManyTeamsConnection)
   */
  publishManyTeams: BatchPayload;
  /**
   * Unpublish many Team documents
   * @deprecated Please use the new paginated many mutation (unpublishManyTeamsConnection)
   */
  unpublishManyTeams: BatchPayload;
  /** Schedule to publish one team */
  schedulePublishTeam?: Maybe<Team>;
  /** Unpublish one team from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishTeam?: Maybe<Team>;
  /** Create one job */
  createJob?: Maybe<Job>;
  /** Update one job */
  updateJob?: Maybe<Job>;
  /** Delete one job from _all_ existing stages. Returns deleted document. */
  deleteJob?: Maybe<Job>;
  /** Upsert one job */
  upsertJob?: Maybe<Job>;
  /** Publish one job */
  publishJob?: Maybe<Job>;
  /** Unpublish one job from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishJob?: Maybe<Job>;
  /** Update many Job documents */
  updateManyJobsConnection: JobConnection;
  /** Delete many Job documents, return deleted documents */
  deleteManyJobsConnection: JobConnection;
  /** Publish many Job documents */
  publishManyJobsConnection: JobConnection;
  /** Find many Job documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyJobsConnection: JobConnection;
  /**
   * Update many jobs
   * @deprecated Please use the new paginated many mutation (updateManyJobsConnection)
   */
  updateManyJobs: BatchPayload;
  /**
   * Delete many Job documents
   * @deprecated Please use the new paginated many mutation (deleteManyJobsConnection)
   */
  deleteManyJobs: BatchPayload;
  /**
   * Publish many Job documents
   * @deprecated Please use the new paginated many mutation (publishManyJobsConnection)
   */
  publishManyJobs: BatchPayload;
  /**
   * Unpublish many Job documents
   * @deprecated Please use the new paginated many mutation (unpublishManyJobsConnection)
   */
  unpublishManyJobs: BatchPayload;
  /** Schedule to publish one job */
  schedulePublishJob?: Maybe<Job>;
  /** Unpublish one job from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishJob?: Maybe<Job>;
  /** Create one teamMember */
  createTeamMember?: Maybe<TeamMember>;
  /** Update one teamMember */
  updateTeamMember?: Maybe<TeamMember>;
  /** Delete one teamMember from _all_ existing stages. Returns deleted document. */
  deleteTeamMember?: Maybe<TeamMember>;
  /** Upsert one teamMember */
  upsertTeamMember?: Maybe<TeamMember>;
  /** Publish one teamMember */
  publishTeamMember?: Maybe<TeamMember>;
  /** Unpublish one teamMember from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishTeamMember?: Maybe<TeamMember>;
  /** Update many TeamMember documents */
  updateManyTeamMembersConnection: TeamMemberConnection;
  /** Delete many TeamMember documents, return deleted documents */
  deleteManyTeamMembersConnection: TeamMemberConnection;
  /** Publish many TeamMember documents */
  publishManyTeamMembersConnection: TeamMemberConnection;
  /** Find many TeamMember documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyTeamMembersConnection: TeamMemberConnection;
  /**
   * Update many teamMembers
   * @deprecated Please use the new paginated many mutation (updateManyTeamMembersConnection)
   */
  updateManyTeamMembers: BatchPayload;
  /**
   * Delete many TeamMember documents
   * @deprecated Please use the new paginated many mutation (deleteManyTeamMembersConnection)
   */
  deleteManyTeamMembers: BatchPayload;
  /**
   * Publish many TeamMember documents
   * @deprecated Please use the new paginated many mutation (publishManyTeamMembersConnection)
   */
  publishManyTeamMembers: BatchPayload;
  /**
   * Unpublish many TeamMember documents
   * @deprecated Please use the new paginated many mutation (unpublishManyTeamMembersConnection)
   */
  unpublishManyTeamMembers: BatchPayload;
  /** Schedule to publish one teamMember */
  schedulePublishTeamMember?: Maybe<TeamMember>;
  /** Unpublish one teamMember from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishTeamMember?: Maybe<TeamMember>;
  /** Create one testimonial */
  createTestimonial?: Maybe<Testimonial>;
  /** Update one testimonial */
  updateTestimonial?: Maybe<Testimonial>;
  /** Delete one testimonial from _all_ existing stages. Returns deleted document. */
  deleteTestimonial?: Maybe<Testimonial>;
  /** Upsert one testimonial */
  upsertTestimonial?: Maybe<Testimonial>;
  /** Publish one testimonial */
  publishTestimonial?: Maybe<Testimonial>;
  /** Unpublish one testimonial from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishTestimonial?: Maybe<Testimonial>;
  /** Update many Testimonial documents */
  updateManyTestimonialsConnection: TestimonialConnection;
  /** Delete many Testimonial documents, return deleted documents */
  deleteManyTestimonialsConnection: TestimonialConnection;
  /** Publish many Testimonial documents */
  publishManyTestimonialsConnection: TestimonialConnection;
  /** Find many Testimonial documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyTestimonialsConnection: TestimonialConnection;
  /**
   * Update many testimonials
   * @deprecated Please use the new paginated many mutation (updateManyTestimonialsConnection)
   */
  updateManyTestimonials: BatchPayload;
  /**
   * Delete many Testimonial documents
   * @deprecated Please use the new paginated many mutation (deleteManyTestimonialsConnection)
   */
  deleteManyTestimonials: BatchPayload;
  /**
   * Publish many Testimonial documents
   * @deprecated Please use the new paginated many mutation (publishManyTestimonialsConnection)
   */
  publishManyTestimonials: BatchPayload;
  /**
   * Unpublish many Testimonial documents
   * @deprecated Please use the new paginated many mutation (unpublishManyTestimonialsConnection)
   */
  unpublishManyTestimonials: BatchPayload;
  /** Schedule to publish one testimonial */
  schedulePublishTestimonial?: Maybe<Testimonial>;
  /** Unpublish one testimonial from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishTestimonial?: Maybe<Testimonial>;
  /** Create one ticketOrder */
  createTicketOrder?: Maybe<TicketOrder>;
  /** Update one ticketOrder */
  updateTicketOrder?: Maybe<TicketOrder>;
  /** Delete one ticketOrder from _all_ existing stages. Returns deleted document. */
  deleteTicketOrder?: Maybe<TicketOrder>;
  /** Upsert one ticketOrder */
  upsertTicketOrder?: Maybe<TicketOrder>;
  /** Publish one ticketOrder */
  publishTicketOrder?: Maybe<TicketOrder>;
  /** Unpublish one ticketOrder from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishTicketOrder?: Maybe<TicketOrder>;
  /** Update many TicketOrder documents */
  updateManyTicketOrdersConnection: TicketOrderConnection;
  /** Delete many TicketOrder documents, return deleted documents */
  deleteManyTicketOrdersConnection: TicketOrderConnection;
  /** Publish many TicketOrder documents */
  publishManyTicketOrdersConnection: TicketOrderConnection;
  /** Find many TicketOrder documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyTicketOrdersConnection: TicketOrderConnection;
  /**
   * Update many ticketOrders
   * @deprecated Please use the new paginated many mutation (updateManyTicketOrdersConnection)
   */
  updateManyTicketOrders: BatchPayload;
  /**
   * Delete many TicketOrder documents
   * @deprecated Please use the new paginated many mutation (deleteManyTicketOrdersConnection)
   */
  deleteManyTicketOrders: BatchPayload;
  /**
   * Publish many TicketOrder documents
   * @deprecated Please use the new paginated many mutation (publishManyTicketOrdersConnection)
   */
  publishManyTicketOrders: BatchPayload;
  /**
   * Unpublish many TicketOrder documents
   * @deprecated Please use the new paginated many mutation (unpublishManyTicketOrdersConnection)
   */
  unpublishManyTicketOrders: BatchPayload;
  /** Schedule to publish one ticketOrder */
  schedulePublishTicketOrder?: Maybe<TicketOrder>;
  /** Unpublish one ticketOrder from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishTicketOrder?: Maybe<TicketOrder>;
  /** Create one generalFaq */
  createGeneralFaq?: Maybe<GeneralFaq>;
  /** Update one generalFaq */
  updateGeneralFaq?: Maybe<GeneralFaq>;
  /** Delete one generalFaq from _all_ existing stages. Returns deleted document. */
  deleteGeneralFaq?: Maybe<GeneralFaq>;
  /** Upsert one generalFaq */
  upsertGeneralFaq?: Maybe<GeneralFaq>;
  /** Publish one generalFaq */
  publishGeneralFaq?: Maybe<GeneralFaq>;
  /** Unpublish one generalFaq from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishGeneralFaq?: Maybe<GeneralFaq>;
  /** Update many GeneralFaq documents */
  updateManyGeneralFaqsConnection: GeneralFaqConnection;
  /** Delete many GeneralFaq documents, return deleted documents */
  deleteManyGeneralFaqsConnection: GeneralFaqConnection;
  /** Publish many GeneralFaq documents */
  publishManyGeneralFaqsConnection: GeneralFaqConnection;
  /** Find many GeneralFaq documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyGeneralFaqsConnection: GeneralFaqConnection;
  /**
   * Update many generalFaqs
   * @deprecated Please use the new paginated many mutation (updateManyGeneralFaqsConnection)
   */
  updateManyGeneralFaqs: BatchPayload;
  /**
   * Delete many GeneralFaq documents
   * @deprecated Please use the new paginated many mutation (deleteManyGeneralFaqsConnection)
   */
  deleteManyGeneralFaqs: BatchPayload;
  /**
   * Publish many GeneralFaq documents
   * @deprecated Please use the new paginated many mutation (publishManyGeneralFaqsConnection)
   */
  publishManyGeneralFaqs: BatchPayload;
  /**
   * Unpublish many GeneralFaq documents
   * @deprecated Please use the new paginated many mutation (unpublishManyGeneralFaqsConnection)
   */
  unpublishManyGeneralFaqs: BatchPayload;
  /** Schedule to publish one generalFaq */
  schedulePublishGeneralFaq?: Maybe<GeneralFaq>;
  /** Unpublish one generalFaq from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishGeneralFaq?: Maybe<GeneralFaq>;
  /** Create one jobFaq */
  createJobFaq?: Maybe<JobFaq>;
  /** Update one jobFaq */
  updateJobFaq?: Maybe<JobFaq>;
  /** Delete one jobFaq from _all_ existing stages. Returns deleted document. */
  deleteJobFaq?: Maybe<JobFaq>;
  /** Upsert one jobFaq */
  upsertJobFaq?: Maybe<JobFaq>;
  /** Publish one jobFaq */
  publishJobFaq?: Maybe<JobFaq>;
  /** Unpublish one jobFaq from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishJobFaq?: Maybe<JobFaq>;
  /** Update many JobFaq documents */
  updateManyJobFaqsConnection: JobFaqConnection;
  /** Delete many JobFaq documents, return deleted documents */
  deleteManyJobFaqsConnection: JobFaqConnection;
  /** Publish many JobFaq documents */
  publishManyJobFaqsConnection: JobFaqConnection;
  /** Find many JobFaq documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyJobFaqsConnection: JobFaqConnection;
  /**
   * Update many jobFaqs
   * @deprecated Please use the new paginated many mutation (updateManyJobFaqsConnection)
   */
  updateManyJobFaqs: BatchPayload;
  /**
   * Delete many JobFaq documents
   * @deprecated Please use the new paginated many mutation (deleteManyJobFaqsConnection)
   */
  deleteManyJobFaqs: BatchPayload;
  /**
   * Publish many JobFaq documents
   * @deprecated Please use the new paginated many mutation (publishManyJobFaqsConnection)
   */
  publishManyJobFaqs: BatchPayload;
  /**
   * Unpublish many JobFaq documents
   * @deprecated Please use the new paginated many mutation (unpublishManyJobFaqsConnection)
   */
  unpublishManyJobFaqs: BatchPayload;
  /** Schedule to publish one jobFaq */
  schedulePublishJobFaq?: Maybe<JobFaq>;
  /** Unpublish one jobFaq from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishJobFaq?: Maybe<JobFaq>;
  /** Create one speaker */
  createSpeaker?: Maybe<Speaker>;
  /** Update one speaker */
  updateSpeaker?: Maybe<Speaker>;
  /** Delete one speaker from _all_ existing stages. Returns deleted document. */
  deleteSpeaker?: Maybe<Speaker>;
  /** Upsert one speaker */
  upsertSpeaker?: Maybe<Speaker>;
  /** Publish one speaker */
  publishSpeaker?: Maybe<Speaker>;
  /** Unpublish one speaker from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishSpeaker?: Maybe<Speaker>;
  /** Update many Speaker documents */
  updateManySpeakersConnection: SpeakerConnection;
  /** Delete many Speaker documents, return deleted documents */
  deleteManySpeakersConnection: SpeakerConnection;
  /** Publish many Speaker documents */
  publishManySpeakersConnection: SpeakerConnection;
  /** Find many Speaker documents that match criteria in specified stage and unpublish from target stages */
  unpublishManySpeakersConnection: SpeakerConnection;
  /**
   * Update many speakers
   * @deprecated Please use the new paginated many mutation (updateManySpeakersConnection)
   */
  updateManySpeakers: BatchPayload;
  /**
   * Delete many Speaker documents
   * @deprecated Please use the new paginated many mutation (deleteManySpeakersConnection)
   */
  deleteManySpeakers: BatchPayload;
  /**
   * Publish many Speaker documents
   * @deprecated Please use the new paginated many mutation (publishManySpeakersConnection)
   */
  publishManySpeakers: BatchPayload;
  /**
   * Unpublish many Speaker documents
   * @deprecated Please use the new paginated many mutation (unpublishManySpeakersConnection)
   */
  unpublishManySpeakers: BatchPayload;
  /** Schedule to publish one speaker */
  schedulePublishSpeaker?: Maybe<Speaker>;
  /** Unpublish one speaker from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishSpeaker?: Maybe<Speaker>;
  /** Create one extraProgram */
  createExtraProgram?: Maybe<ExtraProgram>;
  /** Update one extraProgram */
  updateExtraProgram?: Maybe<ExtraProgram>;
  /** Delete one extraProgram from _all_ existing stages. Returns deleted document. */
  deleteExtraProgram?: Maybe<ExtraProgram>;
  /** Upsert one extraProgram */
  upsertExtraProgram?: Maybe<ExtraProgram>;
  /** Publish one extraProgram */
  publishExtraProgram?: Maybe<ExtraProgram>;
  /** Unpublish one extraProgram from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishExtraProgram?: Maybe<ExtraProgram>;
  /** Update many ExtraProgram documents */
  updateManyExtraProgramsConnection: ExtraProgramConnection;
  /** Delete many ExtraProgram documents, return deleted documents */
  deleteManyExtraProgramsConnection: ExtraProgramConnection;
  /** Publish many ExtraProgram documents */
  publishManyExtraProgramsConnection: ExtraProgramConnection;
  /** Find many ExtraProgram documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyExtraProgramsConnection: ExtraProgramConnection;
  /**
   * Update many extraPrograms
   * @deprecated Please use the new paginated many mutation (updateManyExtraProgramsConnection)
   */
  updateManyExtraPrograms: BatchPayload;
  /**
   * Delete many ExtraProgram documents
   * @deprecated Please use the new paginated many mutation (deleteManyExtraProgramsConnection)
   */
  deleteManyExtraPrograms: BatchPayload;
  /**
   * Publish many ExtraProgram documents
   * @deprecated Please use the new paginated many mutation (publishManyExtraProgramsConnection)
   */
  publishManyExtraPrograms: BatchPayload;
  /**
   * Unpublish many ExtraProgram documents
   * @deprecated Please use the new paginated many mutation (unpublishManyExtraProgramsConnection)
   */
  unpublishManyExtraPrograms: BatchPayload;
  /** Schedule to publish one extraProgram */
  schedulePublishExtraProgram?: Maybe<ExtraProgram>;
  /** Unpublish one extraProgram from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishExtraProgram?: Maybe<ExtraProgram>;
  /** Create one covidConcept */
  createCovidConcept?: Maybe<CovidConcept>;
  /** Update one covidConcept */
  updateCovidConcept?: Maybe<CovidConcept>;
  /** Delete one covidConcept from _all_ existing stages. Returns deleted document. */
  deleteCovidConcept?: Maybe<CovidConcept>;
  /** Upsert one covidConcept */
  upsertCovidConcept?: Maybe<CovidConcept>;
  /** Publish one covidConcept */
  publishCovidConcept?: Maybe<CovidConcept>;
  /** Unpublish one covidConcept from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishCovidConcept?: Maybe<CovidConcept>;
  /** Update many CovidConcept documents */
  updateManyCovidConceptsConnection: CovidConceptConnection;
  /** Delete many CovidConcept documents, return deleted documents */
  deleteManyCovidConceptsConnection: CovidConceptConnection;
  /** Publish many CovidConcept documents */
  publishManyCovidConceptsConnection: CovidConceptConnection;
  /** Find many CovidConcept documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyCovidConceptsConnection: CovidConceptConnection;
  /**
   * Update many covidConcepts
   * @deprecated Please use the new paginated many mutation (updateManyCovidConceptsConnection)
   */
  updateManyCovidConcepts: BatchPayload;
  /**
   * Delete many CovidConcept documents
   * @deprecated Please use the new paginated many mutation (deleteManyCovidConceptsConnection)
   */
  deleteManyCovidConcepts: BatchPayload;
  /**
   * Publish many CovidConcept documents
   * @deprecated Please use the new paginated many mutation (publishManyCovidConceptsConnection)
   */
  publishManyCovidConcepts: BatchPayload;
  /**
   * Unpublish many CovidConcept documents
   * @deprecated Please use the new paginated many mutation (unpublishManyCovidConceptsConnection)
   */
  unpublishManyCovidConcepts: BatchPayload;
  /** Schedule to publish one covidConcept */
  schedulePublishCovidConcept?: Maybe<CovidConcept>;
  /** Unpublish one covidConcept from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishCovidConcept?: Maybe<CovidConcept>;
  /** Delete and return scheduled operation */
  deleteScheduledOperation?: Maybe<ScheduledOperation>;
  /** Create one scheduledRelease */
  createScheduledRelease?: Maybe<ScheduledRelease>;
  /** Update one scheduledRelease */
  updateScheduledRelease?: Maybe<ScheduledRelease>;
  /** Delete one scheduledRelease from _all_ existing stages. Returns deleted document. */
  deleteScheduledRelease?: Maybe<ScheduledRelease>;
};

export type MutationCreateTicketArgs = {
  data: TicketCreateInput;
};

export type MutationUpdateTicketArgs = {
  where: TicketWhereUniqueInput;
  data: TicketUpdateInput;
};

export type MutationDeleteTicketArgs = {
  where: TicketWhereUniqueInput;
};

export type MutationUpsertTicketArgs = {
  where: TicketWhereUniqueInput;
  upsert: TicketUpsertInput;
};

export type MutationPublishTicketArgs = {
  where: TicketWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
};

export type MutationUnpublishTicketArgs = {
  where: TicketWhereUniqueInput;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyTicketsConnectionArgs = {
  where?: Maybe<TicketManyWhereInput>;
  data: TicketUpdateManyInput;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationDeleteManyTicketsConnectionArgs = {
  where?: Maybe<TicketManyWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationPublishManyTicketsConnectionArgs = {
  where?: Maybe<TicketManyWhereInput>;
  from?: Maybe<Stage>;
  to?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyTicketsConnectionArgs = {
  where?: Maybe<TicketManyWhereInput>;
  stage?: Maybe<Stage>;
  from?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyTicketsArgs = {
  where?: Maybe<TicketManyWhereInput>;
  data: TicketUpdateManyInput;
};

export type MutationDeleteManyTicketsArgs = {
  where?: Maybe<TicketManyWhereInput>;
};

export type MutationPublishManyTicketsArgs = {
  where?: Maybe<TicketManyWhereInput>;
  to?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyTicketsArgs = {
  where?: Maybe<TicketManyWhereInput>;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationSchedulePublishTicketArgs = {
  where: TicketWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationScheduleUnpublishTicketArgs = {
  where: TicketWhereUniqueInput;
  from?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationCreateAssetArgs = {
  data: AssetCreateInput;
};

export type MutationUpdateAssetArgs = {
  where: AssetWhereUniqueInput;
  data: AssetUpdateInput;
};

export type MutationDeleteAssetArgs = {
  where: AssetWhereUniqueInput;
};

export type MutationUpsertAssetArgs = {
  where: AssetWhereUniqueInput;
  upsert: AssetUpsertInput;
};

export type MutationPublishAssetArgs = {
  where: AssetWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
};

export type MutationUnpublishAssetArgs = {
  where: AssetWhereUniqueInput;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyAssetsConnectionArgs = {
  where?: Maybe<AssetManyWhereInput>;
  data: AssetUpdateManyInput;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationDeleteManyAssetsConnectionArgs = {
  where?: Maybe<AssetManyWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationPublishManyAssetsConnectionArgs = {
  where?: Maybe<AssetManyWhereInput>;
  from?: Maybe<Stage>;
  to?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyAssetsConnectionArgs = {
  where?: Maybe<AssetManyWhereInput>;
  stage?: Maybe<Stage>;
  from?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyAssetsArgs = {
  where?: Maybe<AssetManyWhereInput>;
  data: AssetUpdateManyInput;
};

export type MutationDeleteManyAssetsArgs = {
  where?: Maybe<AssetManyWhereInput>;
};

export type MutationPublishManyAssetsArgs = {
  where?: Maybe<AssetManyWhereInput>;
  to?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyAssetsArgs = {
  where?: Maybe<AssetManyWhereInput>;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationSchedulePublishAssetArgs = {
  where: AssetWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationScheduleUnpublishAssetArgs = {
  where: AssetWhereUniqueInput;
  from?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationCreateSponsorArgs = {
  data: SponsorCreateInput;
};

export type MutationUpdateSponsorArgs = {
  where: SponsorWhereUniqueInput;
  data: SponsorUpdateInput;
};

export type MutationDeleteSponsorArgs = {
  where: SponsorWhereUniqueInput;
};

export type MutationUpsertSponsorArgs = {
  where: SponsorWhereUniqueInput;
  upsert: SponsorUpsertInput;
};

export type MutationPublishSponsorArgs = {
  where: SponsorWhereUniqueInput;
  to?: Array<Stage>;
};

export type MutationUnpublishSponsorArgs = {
  where: SponsorWhereUniqueInput;
  from?: Array<Stage>;
};

export type MutationUpdateManySponsorsConnectionArgs = {
  where?: Maybe<SponsorManyWhereInput>;
  data: SponsorUpdateManyInput;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationDeleteManySponsorsConnectionArgs = {
  where?: Maybe<SponsorManyWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationPublishManySponsorsConnectionArgs = {
  where?: Maybe<SponsorManyWhereInput>;
  from?: Maybe<Stage>;
  to?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationUnpublishManySponsorsConnectionArgs = {
  where?: Maybe<SponsorManyWhereInput>;
  stage?: Maybe<Stage>;
  from?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationUpdateManySponsorsArgs = {
  where?: Maybe<SponsorManyWhereInput>;
  data: SponsorUpdateManyInput;
};

export type MutationDeleteManySponsorsArgs = {
  where?: Maybe<SponsorManyWhereInput>;
};

export type MutationPublishManySponsorsArgs = {
  where?: Maybe<SponsorManyWhereInput>;
  to?: Array<Stage>;
};

export type MutationUnpublishManySponsorsArgs = {
  where?: Maybe<SponsorManyWhereInput>;
  from?: Array<Stage>;
};

export type MutationSchedulePublishSponsorArgs = {
  where: SponsorWhereUniqueInput;
  to?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationScheduleUnpublishSponsorArgs = {
  where: SponsorWhereUniqueInput;
  from?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationCreateShortInfoTextArgs = {
  data: ShortInfoTextCreateInput;
};

export type MutationUpdateShortInfoTextArgs = {
  where: ShortInfoTextWhereUniqueInput;
  data: ShortInfoTextUpdateInput;
};

export type MutationDeleteShortInfoTextArgs = {
  where: ShortInfoTextWhereUniqueInput;
};

export type MutationUpsertShortInfoTextArgs = {
  where: ShortInfoTextWhereUniqueInput;
  upsert: ShortInfoTextUpsertInput;
};

export type MutationPublishShortInfoTextArgs = {
  where: ShortInfoTextWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
};

export type MutationUnpublishShortInfoTextArgs = {
  where: ShortInfoTextWhereUniqueInput;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyShortInfoTextsConnectionArgs = {
  where?: Maybe<ShortInfoTextManyWhereInput>;
  data: ShortInfoTextUpdateManyInput;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationDeleteManyShortInfoTextsConnectionArgs = {
  where?: Maybe<ShortInfoTextManyWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationPublishManyShortInfoTextsConnectionArgs = {
  where?: Maybe<ShortInfoTextManyWhereInput>;
  from?: Maybe<Stage>;
  to?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyShortInfoTextsConnectionArgs = {
  where?: Maybe<ShortInfoTextManyWhereInput>;
  stage?: Maybe<Stage>;
  from?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyShortInfoTextsArgs = {
  where?: Maybe<ShortInfoTextManyWhereInput>;
  data: ShortInfoTextUpdateManyInput;
};

export type MutationDeleteManyShortInfoTextsArgs = {
  where?: Maybe<ShortInfoTextManyWhereInput>;
};

export type MutationPublishManyShortInfoTextsArgs = {
  where?: Maybe<ShortInfoTextManyWhereInput>;
  to?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyShortInfoTextsArgs = {
  where?: Maybe<ShortInfoTextManyWhereInput>;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationSchedulePublishShortInfoTextArgs = {
  where: ShortInfoTextWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationScheduleUnpublishShortInfoTextArgs = {
  where: ShortInfoTextWhereUniqueInput;
  from?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationCreateContactArgs = {
  data: ContactCreateInput;
};

export type MutationUpdateContactArgs = {
  where: ContactWhereUniqueInput;
  data: ContactUpdateInput;
};

export type MutationDeleteContactArgs = {
  where: ContactWhereUniqueInput;
};

export type MutationUpsertContactArgs = {
  where: ContactWhereUniqueInput;
  upsert: ContactUpsertInput;
};

export type MutationPublishContactArgs = {
  where: ContactWhereUniqueInput;
  to?: Array<Stage>;
};

export type MutationUnpublishContactArgs = {
  where: ContactWhereUniqueInput;
  from?: Array<Stage>;
};

export type MutationUpdateManyContactsConnectionArgs = {
  where?: Maybe<ContactManyWhereInput>;
  data: ContactUpdateManyInput;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationDeleteManyContactsConnectionArgs = {
  where?: Maybe<ContactManyWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationPublishManyContactsConnectionArgs = {
  where?: Maybe<ContactManyWhereInput>;
  from?: Maybe<Stage>;
  to?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationUnpublishManyContactsConnectionArgs = {
  where?: Maybe<ContactManyWhereInput>;
  stage?: Maybe<Stage>;
  from?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationUpdateManyContactsArgs = {
  where?: Maybe<ContactManyWhereInput>;
  data: ContactUpdateManyInput;
};

export type MutationDeleteManyContactsArgs = {
  where?: Maybe<ContactManyWhereInput>;
};

export type MutationPublishManyContactsArgs = {
  where?: Maybe<ContactManyWhereInput>;
  to?: Array<Stage>;
};

export type MutationUnpublishManyContactsArgs = {
  where?: Maybe<ContactManyWhereInput>;
  from?: Array<Stage>;
};

export type MutationSchedulePublishContactArgs = {
  where: ContactWhereUniqueInput;
  to?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationScheduleUnpublishContactArgs = {
  where: ContactWhereUniqueInput;
  from?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationCreateTeamArgs = {
  data: TeamCreateInput;
};

export type MutationUpdateTeamArgs = {
  where: TeamWhereUniqueInput;
  data: TeamUpdateInput;
};

export type MutationDeleteTeamArgs = {
  where: TeamWhereUniqueInput;
};

export type MutationUpsertTeamArgs = {
  where: TeamWhereUniqueInput;
  upsert: TeamUpsertInput;
};

export type MutationPublishTeamArgs = {
  where: TeamWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
};

export type MutationUnpublishTeamArgs = {
  where: TeamWhereUniqueInput;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyTeamsConnectionArgs = {
  where?: Maybe<TeamManyWhereInput>;
  data: TeamUpdateManyInput;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationDeleteManyTeamsConnectionArgs = {
  where?: Maybe<TeamManyWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationPublishManyTeamsConnectionArgs = {
  where?: Maybe<TeamManyWhereInput>;
  from?: Maybe<Stage>;
  to?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyTeamsConnectionArgs = {
  where?: Maybe<TeamManyWhereInput>;
  stage?: Maybe<Stage>;
  from?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyTeamsArgs = {
  where?: Maybe<TeamManyWhereInput>;
  data: TeamUpdateManyInput;
};

export type MutationDeleteManyTeamsArgs = {
  where?: Maybe<TeamManyWhereInput>;
};

export type MutationPublishManyTeamsArgs = {
  where?: Maybe<TeamManyWhereInput>;
  to?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyTeamsArgs = {
  where?: Maybe<TeamManyWhereInput>;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationSchedulePublishTeamArgs = {
  where: TeamWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationScheduleUnpublishTeamArgs = {
  where: TeamWhereUniqueInput;
  from?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationCreateJobArgs = {
  data: JobCreateInput;
};

export type MutationUpdateJobArgs = {
  where: JobWhereUniqueInput;
  data: JobUpdateInput;
};

export type MutationDeleteJobArgs = {
  where: JobWhereUniqueInput;
};

export type MutationUpsertJobArgs = {
  where: JobWhereUniqueInput;
  upsert: JobUpsertInput;
};

export type MutationPublishJobArgs = {
  where: JobWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
};

export type MutationUnpublishJobArgs = {
  where: JobWhereUniqueInput;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyJobsConnectionArgs = {
  where?: Maybe<JobManyWhereInput>;
  data: JobUpdateManyInput;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationDeleteManyJobsConnectionArgs = {
  where?: Maybe<JobManyWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationPublishManyJobsConnectionArgs = {
  where?: Maybe<JobManyWhereInput>;
  from?: Maybe<Stage>;
  to?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyJobsConnectionArgs = {
  where?: Maybe<JobManyWhereInput>;
  stage?: Maybe<Stage>;
  from?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyJobsArgs = {
  where?: Maybe<JobManyWhereInput>;
  data: JobUpdateManyInput;
};

export type MutationDeleteManyJobsArgs = {
  where?: Maybe<JobManyWhereInput>;
};

export type MutationPublishManyJobsArgs = {
  where?: Maybe<JobManyWhereInput>;
  to?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyJobsArgs = {
  where?: Maybe<JobManyWhereInput>;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationSchedulePublishJobArgs = {
  where: JobWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationScheduleUnpublishJobArgs = {
  where: JobWhereUniqueInput;
  from?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationCreateTeamMemberArgs = {
  data: TeamMemberCreateInput;
};

export type MutationUpdateTeamMemberArgs = {
  where: TeamMemberWhereUniqueInput;
  data: TeamMemberUpdateInput;
};

export type MutationDeleteTeamMemberArgs = {
  where: TeamMemberWhereUniqueInput;
};

export type MutationUpsertTeamMemberArgs = {
  where: TeamMemberWhereUniqueInput;
  upsert: TeamMemberUpsertInput;
};

export type MutationPublishTeamMemberArgs = {
  where: TeamMemberWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
};

export type MutationUnpublishTeamMemberArgs = {
  where: TeamMemberWhereUniqueInput;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyTeamMembersConnectionArgs = {
  where?: Maybe<TeamMemberManyWhereInput>;
  data: TeamMemberUpdateManyInput;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationDeleteManyTeamMembersConnectionArgs = {
  where?: Maybe<TeamMemberManyWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationPublishManyTeamMembersConnectionArgs = {
  where?: Maybe<TeamMemberManyWhereInput>;
  from?: Maybe<Stage>;
  to?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyTeamMembersConnectionArgs = {
  where?: Maybe<TeamMemberManyWhereInput>;
  stage?: Maybe<Stage>;
  from?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyTeamMembersArgs = {
  where?: Maybe<TeamMemberManyWhereInput>;
  data: TeamMemberUpdateManyInput;
};

export type MutationDeleteManyTeamMembersArgs = {
  where?: Maybe<TeamMemberManyWhereInput>;
};

export type MutationPublishManyTeamMembersArgs = {
  where?: Maybe<TeamMemberManyWhereInput>;
  to?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyTeamMembersArgs = {
  where?: Maybe<TeamMemberManyWhereInput>;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationSchedulePublishTeamMemberArgs = {
  where: TeamMemberWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationScheduleUnpublishTeamMemberArgs = {
  where: TeamMemberWhereUniqueInput;
  from?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationCreateTestimonialArgs = {
  data: TestimonialCreateInput;
};

export type MutationUpdateTestimonialArgs = {
  where: TestimonialWhereUniqueInput;
  data: TestimonialUpdateInput;
};

export type MutationDeleteTestimonialArgs = {
  where: TestimonialWhereUniqueInput;
};

export type MutationUpsertTestimonialArgs = {
  where: TestimonialWhereUniqueInput;
  upsert: TestimonialUpsertInput;
};

export type MutationPublishTestimonialArgs = {
  where: TestimonialWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
};

export type MutationUnpublishTestimonialArgs = {
  where: TestimonialWhereUniqueInput;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyTestimonialsConnectionArgs = {
  where?: Maybe<TestimonialManyWhereInput>;
  data: TestimonialUpdateManyInput;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationDeleteManyTestimonialsConnectionArgs = {
  where?: Maybe<TestimonialManyWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationPublishManyTestimonialsConnectionArgs = {
  where?: Maybe<TestimonialManyWhereInput>;
  from?: Maybe<Stage>;
  to?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyTestimonialsConnectionArgs = {
  where?: Maybe<TestimonialManyWhereInput>;
  stage?: Maybe<Stage>;
  from?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyTestimonialsArgs = {
  where?: Maybe<TestimonialManyWhereInput>;
  data: TestimonialUpdateManyInput;
};

export type MutationDeleteManyTestimonialsArgs = {
  where?: Maybe<TestimonialManyWhereInput>;
};

export type MutationPublishManyTestimonialsArgs = {
  where?: Maybe<TestimonialManyWhereInput>;
  to?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyTestimonialsArgs = {
  where?: Maybe<TestimonialManyWhereInput>;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationSchedulePublishTestimonialArgs = {
  where: TestimonialWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationScheduleUnpublishTestimonialArgs = {
  where: TestimonialWhereUniqueInput;
  from?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationCreateTicketOrderArgs = {
  data: TicketOrderCreateInput;
};

export type MutationUpdateTicketOrderArgs = {
  where: TicketOrderWhereUniqueInput;
  data: TicketOrderUpdateInput;
};

export type MutationDeleteTicketOrderArgs = {
  where: TicketOrderWhereUniqueInput;
};

export type MutationUpsertTicketOrderArgs = {
  where: TicketOrderWhereUniqueInput;
  upsert: TicketOrderUpsertInput;
};

export type MutationPublishTicketOrderArgs = {
  where: TicketOrderWhereUniqueInput;
  to?: Array<Stage>;
};

export type MutationUnpublishTicketOrderArgs = {
  where: TicketOrderWhereUniqueInput;
  from?: Array<Stage>;
};

export type MutationUpdateManyTicketOrdersConnectionArgs = {
  where?: Maybe<TicketOrderManyWhereInput>;
  data: TicketOrderUpdateManyInput;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationDeleteManyTicketOrdersConnectionArgs = {
  where?: Maybe<TicketOrderManyWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationPublishManyTicketOrdersConnectionArgs = {
  where?: Maybe<TicketOrderManyWhereInput>;
  from?: Maybe<Stage>;
  to?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationUnpublishManyTicketOrdersConnectionArgs = {
  where?: Maybe<TicketOrderManyWhereInput>;
  stage?: Maybe<Stage>;
  from?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationUpdateManyTicketOrdersArgs = {
  where?: Maybe<TicketOrderManyWhereInput>;
  data: TicketOrderUpdateManyInput;
};

export type MutationDeleteManyTicketOrdersArgs = {
  where?: Maybe<TicketOrderManyWhereInput>;
};

export type MutationPublishManyTicketOrdersArgs = {
  where?: Maybe<TicketOrderManyWhereInput>;
  to?: Array<Stage>;
};

export type MutationUnpublishManyTicketOrdersArgs = {
  where?: Maybe<TicketOrderManyWhereInput>;
  from?: Array<Stage>;
};

export type MutationSchedulePublishTicketOrderArgs = {
  where: TicketOrderWhereUniqueInput;
  to?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationScheduleUnpublishTicketOrderArgs = {
  where: TicketOrderWhereUniqueInput;
  from?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationCreateGeneralFaqArgs = {
  data: GeneralFaqCreateInput;
};

export type MutationUpdateGeneralFaqArgs = {
  where: GeneralFaqWhereUniqueInput;
  data: GeneralFaqUpdateInput;
};

export type MutationDeleteGeneralFaqArgs = {
  where: GeneralFaqWhereUniqueInput;
};

export type MutationUpsertGeneralFaqArgs = {
  where: GeneralFaqWhereUniqueInput;
  upsert: GeneralFaqUpsertInput;
};

export type MutationPublishGeneralFaqArgs = {
  where: GeneralFaqWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
};

export type MutationUnpublishGeneralFaqArgs = {
  where: GeneralFaqWhereUniqueInput;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyGeneralFaqsConnectionArgs = {
  where?: Maybe<GeneralFaqManyWhereInput>;
  data: GeneralFaqUpdateManyInput;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationDeleteManyGeneralFaqsConnectionArgs = {
  where?: Maybe<GeneralFaqManyWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationPublishManyGeneralFaqsConnectionArgs = {
  where?: Maybe<GeneralFaqManyWhereInput>;
  from?: Maybe<Stage>;
  to?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyGeneralFaqsConnectionArgs = {
  where?: Maybe<GeneralFaqManyWhereInput>;
  stage?: Maybe<Stage>;
  from?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyGeneralFaqsArgs = {
  where?: Maybe<GeneralFaqManyWhereInput>;
  data: GeneralFaqUpdateManyInput;
};

export type MutationDeleteManyGeneralFaqsArgs = {
  where?: Maybe<GeneralFaqManyWhereInput>;
};

export type MutationPublishManyGeneralFaqsArgs = {
  where?: Maybe<GeneralFaqManyWhereInput>;
  to?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyGeneralFaqsArgs = {
  where?: Maybe<GeneralFaqManyWhereInput>;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationSchedulePublishGeneralFaqArgs = {
  where: GeneralFaqWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationScheduleUnpublishGeneralFaqArgs = {
  where: GeneralFaqWhereUniqueInput;
  from?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationCreateJobFaqArgs = {
  data: JobFaqCreateInput;
};

export type MutationUpdateJobFaqArgs = {
  where: JobFaqWhereUniqueInput;
  data: JobFaqUpdateInput;
};

export type MutationDeleteJobFaqArgs = {
  where: JobFaqWhereUniqueInput;
};

export type MutationUpsertJobFaqArgs = {
  where: JobFaqWhereUniqueInput;
  upsert: JobFaqUpsertInput;
};

export type MutationPublishJobFaqArgs = {
  where: JobFaqWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
};

export type MutationUnpublishJobFaqArgs = {
  where: JobFaqWhereUniqueInput;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyJobFaqsConnectionArgs = {
  where?: Maybe<JobFaqManyWhereInput>;
  data: JobFaqUpdateManyInput;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationDeleteManyJobFaqsConnectionArgs = {
  where?: Maybe<JobFaqManyWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationPublishManyJobFaqsConnectionArgs = {
  where?: Maybe<JobFaqManyWhereInput>;
  from?: Maybe<Stage>;
  to?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyJobFaqsConnectionArgs = {
  where?: Maybe<JobFaqManyWhereInput>;
  stage?: Maybe<Stage>;
  from?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyJobFaqsArgs = {
  where?: Maybe<JobFaqManyWhereInput>;
  data: JobFaqUpdateManyInput;
};

export type MutationDeleteManyJobFaqsArgs = {
  where?: Maybe<JobFaqManyWhereInput>;
};

export type MutationPublishManyJobFaqsArgs = {
  where?: Maybe<JobFaqManyWhereInput>;
  to?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyJobFaqsArgs = {
  where?: Maybe<JobFaqManyWhereInput>;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationSchedulePublishJobFaqArgs = {
  where: JobFaqWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationScheduleUnpublishJobFaqArgs = {
  where: JobFaqWhereUniqueInput;
  from?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationCreateSpeakerArgs = {
  data: SpeakerCreateInput;
};

export type MutationUpdateSpeakerArgs = {
  where: SpeakerWhereUniqueInput;
  data: SpeakerUpdateInput;
};

export type MutationDeleteSpeakerArgs = {
  where: SpeakerWhereUniqueInput;
};

export type MutationUpsertSpeakerArgs = {
  where: SpeakerWhereUniqueInput;
  upsert: SpeakerUpsertInput;
};

export type MutationPublishSpeakerArgs = {
  where: SpeakerWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
};

export type MutationUnpublishSpeakerArgs = {
  where: SpeakerWhereUniqueInput;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManySpeakersConnectionArgs = {
  where?: Maybe<SpeakerManyWhereInput>;
  data: SpeakerUpdateManyInput;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationDeleteManySpeakersConnectionArgs = {
  where?: Maybe<SpeakerManyWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationPublishManySpeakersConnectionArgs = {
  where?: Maybe<SpeakerManyWhereInput>;
  from?: Maybe<Stage>;
  to?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManySpeakersConnectionArgs = {
  where?: Maybe<SpeakerManyWhereInput>;
  stage?: Maybe<Stage>;
  from?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManySpeakersArgs = {
  where?: Maybe<SpeakerManyWhereInput>;
  data: SpeakerUpdateManyInput;
};

export type MutationDeleteManySpeakersArgs = {
  where?: Maybe<SpeakerManyWhereInput>;
};

export type MutationPublishManySpeakersArgs = {
  where?: Maybe<SpeakerManyWhereInput>;
  to?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManySpeakersArgs = {
  where?: Maybe<SpeakerManyWhereInput>;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationSchedulePublishSpeakerArgs = {
  where: SpeakerWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationScheduleUnpublishSpeakerArgs = {
  where: SpeakerWhereUniqueInput;
  from?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationCreateExtraProgramArgs = {
  data: ExtraProgramCreateInput;
};

export type MutationUpdateExtraProgramArgs = {
  where: ExtraProgramWhereUniqueInput;
  data: ExtraProgramUpdateInput;
};

export type MutationDeleteExtraProgramArgs = {
  where: ExtraProgramWhereUniqueInput;
};

export type MutationUpsertExtraProgramArgs = {
  where: ExtraProgramWhereUniqueInput;
  upsert: ExtraProgramUpsertInput;
};

export type MutationPublishExtraProgramArgs = {
  where: ExtraProgramWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
};

export type MutationUnpublishExtraProgramArgs = {
  where: ExtraProgramWhereUniqueInput;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyExtraProgramsConnectionArgs = {
  where?: Maybe<ExtraProgramManyWhereInput>;
  data: ExtraProgramUpdateManyInput;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationDeleteManyExtraProgramsConnectionArgs = {
  where?: Maybe<ExtraProgramManyWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationPublishManyExtraProgramsConnectionArgs = {
  where?: Maybe<ExtraProgramManyWhereInput>;
  from?: Maybe<Stage>;
  to?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyExtraProgramsConnectionArgs = {
  where?: Maybe<ExtraProgramManyWhereInput>;
  stage?: Maybe<Stage>;
  from?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyExtraProgramsArgs = {
  where?: Maybe<ExtraProgramManyWhereInput>;
  data: ExtraProgramUpdateManyInput;
};

export type MutationDeleteManyExtraProgramsArgs = {
  where?: Maybe<ExtraProgramManyWhereInput>;
};

export type MutationPublishManyExtraProgramsArgs = {
  where?: Maybe<ExtraProgramManyWhereInput>;
  to?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyExtraProgramsArgs = {
  where?: Maybe<ExtraProgramManyWhereInput>;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationSchedulePublishExtraProgramArgs = {
  where: ExtraProgramWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationScheduleUnpublishExtraProgramArgs = {
  where: ExtraProgramWhereUniqueInput;
  from?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationCreateCovidConceptArgs = {
  data: CovidConceptCreateInput;
};

export type MutationUpdateCovidConceptArgs = {
  where: CovidConceptWhereUniqueInput;
  data: CovidConceptUpdateInput;
};

export type MutationDeleteCovidConceptArgs = {
  where: CovidConceptWhereUniqueInput;
};

export type MutationUpsertCovidConceptArgs = {
  where: CovidConceptWhereUniqueInput;
  upsert: CovidConceptUpsertInput;
};

export type MutationPublishCovidConceptArgs = {
  where: CovidConceptWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
};

export type MutationUnpublishCovidConceptArgs = {
  where: CovidConceptWhereUniqueInput;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyCovidConceptsConnectionArgs = {
  where?: Maybe<CovidConceptManyWhereInput>;
  data: CovidConceptUpdateManyInput;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationDeleteManyCovidConceptsConnectionArgs = {
  where?: Maybe<CovidConceptManyWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
};

export type MutationPublishManyCovidConceptsConnectionArgs = {
  where?: Maybe<CovidConceptManyWhereInput>;
  from?: Maybe<Stage>;
  to?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyCovidConceptsConnectionArgs = {
  where?: Maybe<CovidConceptManyWhereInput>;
  stage?: Maybe<Stage>;
  from?: Array<Stage>;
  skip?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["ID"]>;
  after?: Maybe<Scalars["ID"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationUpdateManyCovidConceptsArgs = {
  where?: Maybe<CovidConceptManyWhereInput>;
  data: CovidConceptUpdateManyInput;
};

export type MutationDeleteManyCovidConceptsArgs = {
  where?: Maybe<CovidConceptManyWhereInput>;
};

export type MutationPublishManyCovidConceptsArgs = {
  where?: Maybe<CovidConceptManyWhereInput>;
  to?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
};

export type MutationUnpublishManyCovidConceptsArgs = {
  where?: Maybe<CovidConceptManyWhereInput>;
  from?: Array<Stage>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationSchedulePublishCovidConceptArgs = {
  where: CovidConceptWhereUniqueInput;
  locales?: Maybe<Array<Locale>>;
  publishBase?: Maybe<Scalars["Boolean"]>;
  withDefaultLocale?: Maybe<Scalars["Boolean"]>;
  to?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
};

export type MutationScheduleUnpublishCovidConceptArgs = {
  where: CovidConceptWhereUniqueInput;
  from?: Array<Stage>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  releaseId?: Maybe<Scalars["String"]>;
  locales?: Maybe<Array<Locale>>;
  unpublishBase?: Maybe<Scalars["Boolean"]>;
};

export type MutationDeleteScheduledOperationArgs = {
  where: ScheduledOperationWhereUniqueInput;
};

export type MutationCreateScheduledReleaseArgs = {
  data: ScheduledReleaseCreateInput;
};

export type MutationUpdateScheduledReleaseArgs = {
  where: ScheduledReleaseWhereUniqueInput;
  data: ScheduledReleaseUpdateInput;
};

export type MutationDeleteScheduledReleaseArgs = {
  where: ScheduledReleaseWhereUniqueInput;
};

/** An object with an ID */
export type Node = {
  /** The id of the object. */
  id: Scalars["ID"];
  /** The Stage of an object */
  stage: Stage;
};

/** Information about pagination in a connection. */
export type PageInfo = {
  __typename?: "PageInfo";
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars["Boolean"];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars["Boolean"];
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars["String"]>;
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars["String"]>;
  /** Number of items in the current page. */
  pageSize?: Maybe<Scalars["Int"]>;
};

export type PublishLocaleInput = {
  /** Locales to publish */
  locale: Locale;
  /** Stages to publish selected locales to */
  stages: Array<Stage>;
};

export type Query = {
  __typename?: "Query";
  /** Fetches an object given its ID */
  node?: Maybe<Node>;
  /** Retrieve multiple tickets */
  tickets: Array<Ticket>;
  /** Retrieve a single ticket */
  ticket?: Maybe<Ticket>;
  /** Retrieve multiple tickets using the Relay connection interface */
  ticketsConnection: TicketConnection;
  /** Retrieve document version */
  ticketVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple assets */
  assets: Array<Asset>;
  /** Retrieve a single asset */
  asset?: Maybe<Asset>;
  /** Retrieve multiple assets using the Relay connection interface */
  assetsConnection: AssetConnection;
  /** Retrieve document version */
  assetVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple users */
  users: Array<User>;
  /** Retrieve a single user */
  user?: Maybe<User>;
  /** Retrieve multiple users using the Relay connection interface */
  usersConnection: UserConnection;
  /** Retrieve multiple sponsors */
  sponsors: Array<Sponsor>;
  /** Retrieve a single sponsor */
  sponsor?: Maybe<Sponsor>;
  /** Retrieve multiple sponsors using the Relay connection interface */
  sponsorsConnection: SponsorConnection;
  /** Retrieve document version */
  sponsorVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple shortInfoTexts */
  shortInfoTexts: Array<ShortInfoText>;
  /** Retrieve a single shortInfoText */
  shortInfoText?: Maybe<ShortInfoText>;
  /** Retrieve multiple shortInfoTexts using the Relay connection interface */
  shortInfoTextsConnection: ShortInfoTextConnection;
  /** Retrieve document version */
  shortInfoTextVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple contacts */
  contacts: Array<Contact>;
  /** Retrieve a single contact */
  contact?: Maybe<Contact>;
  /** Retrieve multiple contacts using the Relay connection interface */
  contactsConnection: ContactConnection;
  /** Retrieve document version */
  contactVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple teams */
  teams: Array<Team>;
  /** Retrieve a single team */
  team?: Maybe<Team>;
  /** Retrieve multiple teams using the Relay connection interface */
  teamsConnection: TeamConnection;
  /** Retrieve document version */
  teamVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple jobs */
  jobs: Array<Job>;
  /** Retrieve a single job */
  job?: Maybe<Job>;
  /** Retrieve multiple jobs using the Relay connection interface */
  jobsConnection: JobConnection;
  /** Retrieve document version */
  jobVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple teamMembers */
  teamMembers: Array<TeamMember>;
  /** Retrieve a single teamMember */
  teamMember?: Maybe<TeamMember>;
  /** Retrieve multiple teamMembers using the Relay connection interface */
  teamMembersConnection: TeamMemberConnection;
  /** Retrieve document version */
  teamMemberVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple testimonials */
  testimonials: Array<Testimonial>;
  /** Retrieve a single testimonial */
  testimonial?: Maybe<Testimonial>;
  /** Retrieve multiple testimonials using the Relay connection interface */
  testimonialsConnection: TestimonialConnection;
  /** Retrieve document version */
  testimonialVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple ticketOrders */
  ticketOrders: Array<TicketOrder>;
  /** Retrieve a single ticketOrder */
  ticketOrder?: Maybe<TicketOrder>;
  /** Retrieve multiple ticketOrders using the Relay connection interface */
  ticketOrdersConnection: TicketOrderConnection;
  /** Retrieve document version */
  ticketOrderVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple generalFaqs */
  generalFaqs: Array<GeneralFaq>;
  /** Retrieve a single generalFaq */
  generalFaq?: Maybe<GeneralFaq>;
  /** Retrieve multiple generalFaqs using the Relay connection interface */
  generalFaqsConnection: GeneralFaqConnection;
  /** Retrieve document version */
  generalFaqVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple jobFaqs */
  jobFaqs: Array<JobFaq>;
  /** Retrieve a single jobFaq */
  jobFaq?: Maybe<JobFaq>;
  /** Retrieve multiple jobFaqs using the Relay connection interface */
  jobFaqsConnection: JobFaqConnection;
  /** Retrieve document version */
  jobFaqVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple speakers */
  speakers: Array<Speaker>;
  /** Retrieve a single speaker */
  speaker?: Maybe<Speaker>;
  /** Retrieve multiple speakers using the Relay connection interface */
  speakersConnection: SpeakerConnection;
  /** Retrieve document version */
  speakerVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple extraPrograms */
  extraPrograms: Array<ExtraProgram>;
  /** Retrieve a single extraProgram */
  extraProgram?: Maybe<ExtraProgram>;
  /** Retrieve multiple extraPrograms using the Relay connection interface */
  extraProgramsConnection: ExtraProgramConnection;
  /** Retrieve document version */
  extraProgramVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple covidConcepts */
  covidConcepts: Array<CovidConcept>;
  /** Retrieve a single covidConcept */
  covidConcept?: Maybe<CovidConcept>;
  /** Retrieve multiple covidConcepts using the Relay connection interface */
  covidConceptsConnection: CovidConceptConnection;
  /** Retrieve document version */
  covidConceptVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple scheduledOperations */
  scheduledOperations: Array<ScheduledOperation>;
  /** Retrieve a single scheduledOperation */
  scheduledOperation?: Maybe<ScheduledOperation>;
  /** Retrieve multiple scheduledOperations using the Relay connection interface */
  scheduledOperationsConnection: ScheduledOperationConnection;
  /** Retrieve multiple scheduledReleases */
  scheduledReleases: Array<ScheduledRelease>;
  /** Retrieve a single scheduledRelease */
  scheduledRelease?: Maybe<ScheduledRelease>;
  /** Retrieve multiple scheduledReleases using the Relay connection interface */
  scheduledReleasesConnection: ScheduledReleaseConnection;
};

export type QueryNodeArgs = {
  id: Scalars["ID"];
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryTicketsArgs = {
  where?: Maybe<TicketWhereInput>;
  orderBy?: Maybe<TicketOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryTicketArgs = {
  where: TicketWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryTicketsConnectionArgs = {
  where?: Maybe<TicketWhereInput>;
  orderBy?: Maybe<TicketOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryTicketVersionArgs = {
  where: VersionWhereInput;
};

export type QueryAssetsArgs = {
  where?: Maybe<AssetWhereInput>;
  orderBy?: Maybe<AssetOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryAssetArgs = {
  where: AssetWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryAssetsConnectionArgs = {
  where?: Maybe<AssetWhereInput>;
  orderBy?: Maybe<AssetOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryAssetVersionArgs = {
  where: VersionWhereInput;
};

export type QueryUsersArgs = {
  where?: Maybe<UserWhereInput>;
  orderBy?: Maybe<UserOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryUserArgs = {
  where: UserWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryUsersConnectionArgs = {
  where?: Maybe<UserWhereInput>;
  orderBy?: Maybe<UserOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QuerySponsorsArgs = {
  where?: Maybe<SponsorWhereInput>;
  orderBy?: Maybe<SponsorOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QuerySponsorArgs = {
  where: SponsorWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QuerySponsorsConnectionArgs = {
  where?: Maybe<SponsorWhereInput>;
  orderBy?: Maybe<SponsorOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QuerySponsorVersionArgs = {
  where: VersionWhereInput;
};

export type QueryShortInfoTextsArgs = {
  where?: Maybe<ShortInfoTextWhereInput>;
  orderBy?: Maybe<ShortInfoTextOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryShortInfoTextArgs = {
  where: ShortInfoTextWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryShortInfoTextsConnectionArgs = {
  where?: Maybe<ShortInfoTextWhereInput>;
  orderBy?: Maybe<ShortInfoTextOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryShortInfoTextVersionArgs = {
  where: VersionWhereInput;
};

export type QueryContactsArgs = {
  where?: Maybe<ContactWhereInput>;
  orderBy?: Maybe<ContactOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryContactArgs = {
  where: ContactWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryContactsConnectionArgs = {
  where?: Maybe<ContactWhereInput>;
  orderBy?: Maybe<ContactOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryContactVersionArgs = {
  where: VersionWhereInput;
};

export type QueryTeamsArgs = {
  where?: Maybe<TeamWhereInput>;
  orderBy?: Maybe<TeamOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryTeamArgs = {
  where: TeamWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryTeamsConnectionArgs = {
  where?: Maybe<TeamWhereInput>;
  orderBy?: Maybe<TeamOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryTeamVersionArgs = {
  where: VersionWhereInput;
};

export type QueryJobsArgs = {
  where?: Maybe<JobWhereInput>;
  orderBy?: Maybe<JobOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryJobArgs = {
  where: JobWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryJobsConnectionArgs = {
  where?: Maybe<JobWhereInput>;
  orderBy?: Maybe<JobOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryJobVersionArgs = {
  where: VersionWhereInput;
};

export type QueryTeamMembersArgs = {
  where?: Maybe<TeamMemberWhereInput>;
  orderBy?: Maybe<TeamMemberOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryTeamMemberArgs = {
  where: TeamMemberWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryTeamMembersConnectionArgs = {
  where?: Maybe<TeamMemberWhereInput>;
  orderBy?: Maybe<TeamMemberOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryTeamMemberVersionArgs = {
  where: VersionWhereInput;
};

export type QueryTestimonialsArgs = {
  where?: Maybe<TestimonialWhereInput>;
  orderBy?: Maybe<TestimonialOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryTestimonialArgs = {
  where: TestimonialWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryTestimonialsConnectionArgs = {
  where?: Maybe<TestimonialWhereInput>;
  orderBy?: Maybe<TestimonialOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryTestimonialVersionArgs = {
  where: VersionWhereInput;
};

export type QueryTicketOrdersArgs = {
  where?: Maybe<TicketOrderWhereInput>;
  orderBy?: Maybe<TicketOrderOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryTicketOrderArgs = {
  where: TicketOrderWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryTicketOrdersConnectionArgs = {
  where?: Maybe<TicketOrderWhereInput>;
  orderBy?: Maybe<TicketOrderOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryTicketOrderVersionArgs = {
  where: VersionWhereInput;
};

export type QueryGeneralFaqsArgs = {
  where?: Maybe<GeneralFaqWhereInput>;
  orderBy?: Maybe<GeneralFaqOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryGeneralFaqArgs = {
  where: GeneralFaqWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryGeneralFaqsConnectionArgs = {
  where?: Maybe<GeneralFaqWhereInput>;
  orderBy?: Maybe<GeneralFaqOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryGeneralFaqVersionArgs = {
  where: VersionWhereInput;
};

export type QueryJobFaqsArgs = {
  where?: Maybe<JobFaqWhereInput>;
  orderBy?: Maybe<JobFaqOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryJobFaqArgs = {
  where: JobFaqWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryJobFaqsConnectionArgs = {
  where?: Maybe<JobFaqWhereInput>;
  orderBy?: Maybe<JobFaqOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryJobFaqVersionArgs = {
  where: VersionWhereInput;
};

export type QuerySpeakersArgs = {
  where?: Maybe<SpeakerWhereInput>;
  orderBy?: Maybe<SpeakerOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QuerySpeakerArgs = {
  where: SpeakerWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QuerySpeakersConnectionArgs = {
  where?: Maybe<SpeakerWhereInput>;
  orderBy?: Maybe<SpeakerOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QuerySpeakerVersionArgs = {
  where: VersionWhereInput;
};

export type QueryExtraProgramsArgs = {
  where?: Maybe<ExtraProgramWhereInput>;
  orderBy?: Maybe<ExtraProgramOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryExtraProgramArgs = {
  where: ExtraProgramWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryExtraProgramsConnectionArgs = {
  where?: Maybe<ExtraProgramWhereInput>;
  orderBy?: Maybe<ExtraProgramOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryExtraProgramVersionArgs = {
  where: VersionWhereInput;
};

export type QueryCovidConceptsArgs = {
  where?: Maybe<CovidConceptWhereInput>;
  orderBy?: Maybe<CovidConceptOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryCovidConceptArgs = {
  where: CovidConceptWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryCovidConceptsConnectionArgs = {
  where?: Maybe<CovidConceptWhereInput>;
  orderBy?: Maybe<CovidConceptOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryCovidConceptVersionArgs = {
  where: VersionWhereInput;
};

export type QueryScheduledOperationsArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  orderBy?: Maybe<ScheduledOperationOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryScheduledOperationArgs = {
  where: ScheduledOperationWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryScheduledOperationsConnectionArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  orderBy?: Maybe<ScheduledOperationOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryScheduledReleasesArgs = {
  where?: Maybe<ScheduledReleaseWhereInput>;
  orderBy?: Maybe<ScheduledReleaseOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryScheduledReleaseArgs = {
  where: ScheduledReleaseWhereUniqueInput;
  stage?: Stage;
  locales?: Array<Locale>;
};

export type QueryScheduledReleasesConnectionArgs = {
  where?: Maybe<ScheduledReleaseWhereInput>;
  orderBy?: Maybe<ScheduledReleaseOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  stage?: Stage;
  locales?: Array<Locale>;
};

/** Representing a RGBA color value: https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#rgb()_and_rgba() */
export type Rgba = {
  __typename?: "RGBA";
  r: Scalars["RGBAHue"];
  g: Scalars["RGBAHue"];
  b: Scalars["RGBAHue"];
  a: Scalars["RGBATransparency"];
};

/** Input type representing a RGBA color value: https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#rgb()_and_rgba() */
export type RgbaInput = {
  r: Scalars["RGBAHue"];
  g: Scalars["RGBAHue"];
  b: Scalars["RGBAHue"];
  a: Scalars["RGBATransparency"];
};

/** Custom type representing a rich text value comprising of raw rich text ast, html, markdown and text values */
export type RichText = {
  __typename?: "RichText";
  /** Returns AST representation */
  raw: Scalars["RichTextAST"];
  /** Returns HTMl representation */
  html: Scalars["String"];
  /** Returns Markdown representation */
  markdown: Scalars["String"];
  /** Returns plain-text contents of RichText */
  text: Scalars["String"];
};

/** Scheduled Operation system model */
export type ScheduledOperation = Node & {
  __typename?: "ScheduledOperation";
  /** System stage field */
  stage: Stage;
  /** Get the document in other stages */
  documentInStages: Array<ScheduledOperation>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** Operation description */
  description?: Maybe<Scalars["String"]>;
  /** Operation error message */
  errorMessage?: Maybe<Scalars["String"]>;
  /** Raw operation payload including all details, this field is subject to change */
  rawPayload: Scalars["Json"];
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  /** The release this operation is scheduled for */
  release?: Maybe<ScheduledRelease>;
  /** operation Status */
  status: ScheduledOperationStatus;
  affectedDocuments: Array<ScheduledOperationAffectedDocument>;
};

/** Scheduled Operation system model */
export type ScheduledOperationDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

/** Scheduled Operation system model */
export type ScheduledOperationCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** Scheduled Operation system model */
export type ScheduledOperationUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** Scheduled Operation system model */
export type ScheduledOperationPublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** Scheduled Operation system model */
export type ScheduledOperationReleaseArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** Scheduled Operation system model */
export type ScheduledOperationAffectedDocumentsArgs = {
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

export type ScheduledOperationAffectedDocument =
  | Asset
  | Contact
  | CovidConcept
  | ExtraProgram
  | GeneralFaq
  | Job
  | JobFaq
  | ShortInfoText
  | Speaker
  | Sponsor
  | Team
  | TeamMember
  | Testimonial
  | Ticket
  | TicketOrder;

export type ScheduledOperationConnectInput = {
  /** Document to connect */
  where: ScheduledOperationWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type ScheduledOperationConnection = {
  __typename?: "ScheduledOperationConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<ScheduledOperationEdge>;
  aggregate: Aggregate;
};

export type ScheduledOperationCreateManyInlineInput = {
  /** Connect multiple existing ScheduledOperation documents */
  connect?: Maybe<Array<ScheduledOperationWhereUniqueInput>>;
};

export type ScheduledOperationCreateOneInlineInput = {
  /** Connect one existing ScheduledOperation document */
  connect?: Maybe<ScheduledOperationWhereUniqueInput>;
};

/** An edge in a connection. */
export type ScheduledOperationEdge = {
  __typename?: "ScheduledOperationEdge";
  /** The item at the end of the edge. */
  node: ScheduledOperation;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** Identifies documents */
export type ScheduledOperationManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<ScheduledOperationWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<ScheduledOperationWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<ScheduledOperationWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  description?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  description_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  description_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  description_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  description_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  description_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  description_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  description_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  description_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  description_not_ends_with?: Maybe<Scalars["String"]>;
  errorMessage?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  errorMessage_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  errorMessage_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  errorMessage_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  errorMessage_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  errorMessage_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  errorMessage_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  errorMessage_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  errorMessage_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  errorMessage_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  release?: Maybe<ScheduledReleaseWhereInput>;
  status?: Maybe<ScheduledOperationStatus>;
  /** All values that are not equal to given value. */
  status_not?: Maybe<ScheduledOperationStatus>;
  /** All values that are contained in given list. */
  status_in?: Maybe<Array<ScheduledOperationStatus>>;
  /** All values that are not contained in given list. */
  status_not_in?: Maybe<Array<ScheduledOperationStatus>>;
};

export enum ScheduledOperationOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  DescriptionAsc = "description_ASC",
  DescriptionDesc = "description_DESC",
  ErrorMessageAsc = "errorMessage_ASC",
  ErrorMessageDesc = "errorMessage_DESC",
  StatusAsc = "status_ASC",
  StatusDesc = "status_DESC",
}

/** System Scheduled Operation Status */
export enum ScheduledOperationStatus {
  Pending = "PENDING",
  InProgress = "IN_PROGRESS",
  Completed = "COMPLETED",
  Failed = "FAILED",
  Canceled = "CANCELED",
}

export type ScheduledOperationUpdateManyInlineInput = {
  /** Connect multiple existing ScheduledOperation documents */
  connect?: Maybe<Array<ScheduledOperationConnectInput>>;
  /** Override currently-connected documents with multiple existing ScheduledOperation documents */
  set?: Maybe<Array<ScheduledOperationWhereUniqueInput>>;
  /** Disconnect multiple ScheduledOperation documents */
  disconnect?: Maybe<Array<ScheduledOperationWhereUniqueInput>>;
};

export type ScheduledOperationUpdateOneInlineInput = {
  /** Connect existing ScheduledOperation document */
  connect?: Maybe<ScheduledOperationWhereUniqueInput>;
  /** Disconnect currently connected ScheduledOperation document */
  disconnect?: Maybe<Scalars["Boolean"]>;
};

/** Identifies documents */
export type ScheduledOperationWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<ScheduledOperationWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<ScheduledOperationWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<ScheduledOperationWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  description?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  description_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  description_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  description_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  description_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  description_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  description_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  description_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  description_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  description_not_ends_with?: Maybe<Scalars["String"]>;
  errorMessage?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  errorMessage_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  errorMessage_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  errorMessage_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  errorMessage_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  errorMessage_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  errorMessage_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  errorMessage_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  errorMessage_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  errorMessage_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  release?: Maybe<ScheduledReleaseWhereInput>;
  status?: Maybe<ScheduledOperationStatus>;
  /** All values that are not equal to given value. */
  status_not?: Maybe<ScheduledOperationStatus>;
  /** All values that are contained in given list. */
  status_in?: Maybe<Array<ScheduledOperationStatus>>;
  /** All values that are not contained in given list. */
  status_not_in?: Maybe<Array<ScheduledOperationStatus>>;
};

/** References ScheduledOperation record uniquely */
export type ScheduledOperationWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

/** Scheduled Release system model */
export type ScheduledRelease = Node & {
  __typename?: "ScheduledRelease";
  /** System stage field */
  stage: Stage;
  /** Get the document in other stages */
  documentInStages: Array<ScheduledRelease>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** Release Title */
  title?: Maybe<Scalars["String"]>;
  /** Release description */
  description?: Maybe<Scalars["String"]>;
  /** Release error message */
  errorMessage?: Maybe<Scalars["String"]>;
  /** Whether scheduled release should be run */
  isActive: Scalars["Boolean"];
  /** Whether scheduled release is implicit */
  isImplicit: Scalars["Boolean"];
  /** Release date and time */
  releaseAt?: Maybe<Scalars["DateTime"]>;
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  /** Operations to run with this release */
  operations: Array<ScheduledOperation>;
  /** Release Status */
  status: ScheduledReleaseStatus;
};

/** Scheduled Release system model */
export type ScheduledReleaseDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

/** Scheduled Release system model */
export type ScheduledReleaseCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** Scheduled Release system model */
export type ScheduledReleaseUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** Scheduled Release system model */
export type ScheduledReleasePublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** Scheduled Release system model */
export type ScheduledReleaseOperationsArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  orderBy?: Maybe<ScheduledOperationOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

export type ScheduledReleaseConnectInput = {
  /** Document to connect */
  where: ScheduledReleaseWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type ScheduledReleaseConnection = {
  __typename?: "ScheduledReleaseConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<ScheduledReleaseEdge>;
  aggregate: Aggregate;
};

export type ScheduledReleaseCreateInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  title?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  errorMessage?: Maybe<Scalars["String"]>;
  isActive?: Maybe<Scalars["Boolean"]>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
};

export type ScheduledReleaseCreateManyInlineInput = {
  /** Create and connect multiple existing ScheduledRelease documents */
  create?: Maybe<Array<ScheduledReleaseCreateInput>>;
  /** Connect multiple existing ScheduledRelease documents */
  connect?: Maybe<Array<ScheduledReleaseWhereUniqueInput>>;
};

export type ScheduledReleaseCreateOneInlineInput = {
  /** Create and connect one ScheduledRelease document */
  create?: Maybe<ScheduledReleaseCreateInput>;
  /** Connect one existing ScheduledRelease document */
  connect?: Maybe<ScheduledReleaseWhereUniqueInput>;
};

/** An edge in a connection. */
export type ScheduledReleaseEdge = {
  __typename?: "ScheduledReleaseEdge";
  /** The item at the end of the edge. */
  node: ScheduledRelease;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** Identifies documents */
export type ScheduledReleaseManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<ScheduledReleaseWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<ScheduledReleaseWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<ScheduledReleaseWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  title?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  title_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  title_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  title_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  title_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  title_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  title_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  title_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  title_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  title_not_ends_with?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  description_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  description_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  description_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  description_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  description_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  description_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  description_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  description_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  description_not_ends_with?: Maybe<Scalars["String"]>;
  errorMessage?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  errorMessage_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  errorMessage_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  errorMessage_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  errorMessage_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  errorMessage_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  errorMessage_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  errorMessage_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  errorMessage_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  errorMessage_not_ends_with?: Maybe<Scalars["String"]>;
  isActive?: Maybe<Scalars["Boolean"]>;
  /** All values that are not equal to given value. */
  isActive_not?: Maybe<Scalars["Boolean"]>;
  isImplicit?: Maybe<Scalars["Boolean"]>;
  /** All values that are not equal to given value. */
  isImplicit_not?: Maybe<Scalars["Boolean"]>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  releaseAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  releaseAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  releaseAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  releaseAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  releaseAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  releaseAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  releaseAt_gte?: Maybe<Scalars["DateTime"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  operations_every?: Maybe<ScheduledOperationWhereInput>;
  operations_some?: Maybe<ScheduledOperationWhereInput>;
  operations_none?: Maybe<ScheduledOperationWhereInput>;
  status?: Maybe<ScheduledReleaseStatus>;
  /** All values that are not equal to given value. */
  status_not?: Maybe<ScheduledReleaseStatus>;
  /** All values that are contained in given list. */
  status_in?: Maybe<Array<ScheduledReleaseStatus>>;
  /** All values that are not contained in given list. */
  status_not_in?: Maybe<Array<ScheduledReleaseStatus>>;
};

export enum ScheduledReleaseOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  TitleAsc = "title_ASC",
  TitleDesc = "title_DESC",
  DescriptionAsc = "description_ASC",
  DescriptionDesc = "description_DESC",
  ErrorMessageAsc = "errorMessage_ASC",
  ErrorMessageDesc = "errorMessage_DESC",
  IsActiveAsc = "isActive_ASC",
  IsActiveDesc = "isActive_DESC",
  IsImplicitAsc = "isImplicit_ASC",
  IsImplicitDesc = "isImplicit_DESC",
  ReleaseAtAsc = "releaseAt_ASC",
  ReleaseAtDesc = "releaseAt_DESC",
  StatusAsc = "status_ASC",
  StatusDesc = "status_DESC",
}

/** System Scheduled Release Status */
export enum ScheduledReleaseStatus {
  Pending = "PENDING",
  InProgress = "IN_PROGRESS",
  Completed = "COMPLETED",
  Failed = "FAILED",
}

export type ScheduledReleaseUpdateInput = {
  title?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  errorMessage?: Maybe<Scalars["String"]>;
  isActive?: Maybe<Scalars["Boolean"]>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
};

export type ScheduledReleaseUpdateManyInlineInput = {
  /** Create and connect multiple ScheduledRelease documents */
  create?: Maybe<Array<ScheduledReleaseCreateInput>>;
  /** Connect multiple existing ScheduledRelease documents */
  connect?: Maybe<Array<ScheduledReleaseConnectInput>>;
  /** Override currently-connected documents with multiple existing ScheduledRelease documents */
  set?: Maybe<Array<ScheduledReleaseWhereUniqueInput>>;
  /** Update multiple ScheduledRelease documents */
  update?: Maybe<Array<ScheduledReleaseUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple ScheduledRelease documents */
  upsert?: Maybe<Array<ScheduledReleaseUpsertWithNestedWhereUniqueInput>>;
  /** Disconnect multiple ScheduledRelease documents */
  disconnect?: Maybe<Array<ScheduledReleaseWhereUniqueInput>>;
  /** Delete multiple ScheduledRelease documents */
  delete?: Maybe<Array<ScheduledReleaseWhereUniqueInput>>;
};

export type ScheduledReleaseUpdateManyInput = {
  title?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  errorMessage?: Maybe<Scalars["String"]>;
  isActive?: Maybe<Scalars["Boolean"]>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
};

export type ScheduledReleaseUpdateManyWithNestedWhereInput = {
  /** Document search */
  where: ScheduledReleaseWhereInput;
  /** Update many input */
  data: ScheduledReleaseUpdateManyInput;
};

export type ScheduledReleaseUpdateOneInlineInput = {
  /** Create and connect one ScheduledRelease document */
  create?: Maybe<ScheduledReleaseCreateInput>;
  /** Update single ScheduledRelease document */
  update?: Maybe<ScheduledReleaseUpdateWithNestedWhereUniqueInput>;
  /** Upsert single ScheduledRelease document */
  upsert?: Maybe<ScheduledReleaseUpsertWithNestedWhereUniqueInput>;
  /** Connect existing ScheduledRelease document */
  connect?: Maybe<ScheduledReleaseWhereUniqueInput>;
  /** Disconnect currently connected ScheduledRelease document */
  disconnect?: Maybe<Scalars["Boolean"]>;
  /** Delete currently connected ScheduledRelease document */
  delete?: Maybe<Scalars["Boolean"]>;
};

export type ScheduledReleaseUpdateWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: ScheduledReleaseWhereUniqueInput;
  /** Document to update */
  data: ScheduledReleaseUpdateInput;
};

export type ScheduledReleaseUpsertInput = {
  /** Create document if it didn't exist */
  create: ScheduledReleaseCreateInput;
  /** Update document if it exists */
  update: ScheduledReleaseUpdateInput;
};

export type ScheduledReleaseUpsertWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: ScheduledReleaseWhereUniqueInput;
  /** Upsert data */
  data: ScheduledReleaseUpsertInput;
};

/** Identifies documents */
export type ScheduledReleaseWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<ScheduledReleaseWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<ScheduledReleaseWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<ScheduledReleaseWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  title?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  title_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  title_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  title_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  title_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  title_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  title_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  title_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  title_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  title_not_ends_with?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  description_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  description_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  description_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  description_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  description_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  description_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  description_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  description_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  description_not_ends_with?: Maybe<Scalars["String"]>;
  errorMessage?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  errorMessage_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  errorMessage_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  errorMessage_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  errorMessage_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  errorMessage_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  errorMessage_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  errorMessage_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  errorMessage_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  errorMessage_not_ends_with?: Maybe<Scalars["String"]>;
  isActive?: Maybe<Scalars["Boolean"]>;
  /** All values that are not equal to given value. */
  isActive_not?: Maybe<Scalars["Boolean"]>;
  isImplicit?: Maybe<Scalars["Boolean"]>;
  /** All values that are not equal to given value. */
  isImplicit_not?: Maybe<Scalars["Boolean"]>;
  releaseAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  releaseAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  releaseAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  releaseAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  releaseAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  releaseAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  releaseAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  releaseAt_gte?: Maybe<Scalars["DateTime"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  operations_every?: Maybe<ScheduledOperationWhereInput>;
  operations_some?: Maybe<ScheduledOperationWhereInput>;
  operations_none?: Maybe<ScheduledOperationWhereInput>;
  status?: Maybe<ScheduledReleaseStatus>;
  /** All values that are not equal to given value. */
  status_not?: Maybe<ScheduledReleaseStatus>;
  /** All values that are contained in given list. */
  status_in?: Maybe<Array<ScheduledReleaseStatus>>;
  /** All values that are not contained in given list. */
  status_not_in?: Maybe<Array<ScheduledReleaseStatus>>;
};

/** References ScheduledRelease record uniquely */
export type ScheduledReleaseWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

/** Short informational texts that are shown on the homepage */
export type ShortInfoText = Node & {
  __typename?: "ShortInfoText";
  /** System stage field */
  stage: Stage;
  /** System Locale field */
  locale: Locale;
  /** Get the other localizations for this document */
  localizations: Array<ShortInfoText>;
  /** Get the document in other stages */
  documentInStages: Array<ShortInfoText>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  header: Scalars["String"];
  content: Scalars["String"];
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  image?: Maybe<Asset>;
  scheduledIn: Array<ScheduledOperation>;
  /** List of ShortInfoText versions */
  history: Array<Version>;
};

/** Short informational texts that are shown on the homepage */
export type ShortInfoTextLocalizationsArgs = {
  locales?: Array<Locale>;
  includeCurrent?: Scalars["Boolean"];
};

/** Short informational texts that are shown on the homepage */
export type ShortInfoTextDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

/** Short informational texts that are shown on the homepage */
export type ShortInfoTextCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

/** Short informational texts that are shown on the homepage */
export type ShortInfoTextUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

/** Short informational texts that are shown on the homepage */
export type ShortInfoTextPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

/** Short informational texts that are shown on the homepage */
export type ShortInfoTextCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** Short informational texts that are shown on the homepage */
export type ShortInfoTextUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** Short informational texts that are shown on the homepage */
export type ShortInfoTextPublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** Short informational texts that are shown on the homepage */
export type ShortInfoTextImageArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** Short informational texts that are shown on the homepage */
export type ShortInfoTextScheduledInArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

/** Short informational texts that are shown on the homepage */
export type ShortInfoTextHistoryArgs = {
  limit?: Scalars["Int"];
  skip?: Scalars["Int"];
  stageOverride?: Maybe<Stage>;
};

export type ShortInfoTextConnectInput = {
  /** Document to connect */
  where: ShortInfoTextWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type ShortInfoTextConnection = {
  __typename?: "ShortInfoTextConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<ShortInfoTextEdge>;
  aggregate: Aggregate;
};

export type ShortInfoTextCreateInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** header input for default locale (en) */
  header: Scalars["String"];
  /** content input for default locale (en) */
  content: Scalars["String"];
  image?: Maybe<AssetCreateOneInlineInput>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: Maybe<ShortInfoTextCreateLocalizationsInput>;
};

export type ShortInfoTextCreateLocalizationDataInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  header: Scalars["String"];
  content: Scalars["String"];
};

export type ShortInfoTextCreateLocalizationInput = {
  /** Localization input */
  data: ShortInfoTextCreateLocalizationDataInput;
  locale: Locale;
};

export type ShortInfoTextCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: Maybe<Array<ShortInfoTextCreateLocalizationInput>>;
};

export type ShortInfoTextCreateManyInlineInput = {
  /** Create and connect multiple existing ShortInfoText documents */
  create?: Maybe<Array<ShortInfoTextCreateInput>>;
  /** Connect multiple existing ShortInfoText documents */
  connect?: Maybe<Array<ShortInfoTextWhereUniqueInput>>;
};

export type ShortInfoTextCreateOneInlineInput = {
  /** Create and connect one ShortInfoText document */
  create?: Maybe<ShortInfoTextCreateInput>;
  /** Connect one existing ShortInfoText document */
  connect?: Maybe<ShortInfoTextWhereUniqueInput>;
};

/** An edge in a connection. */
export type ShortInfoTextEdge = {
  __typename?: "ShortInfoTextEdge";
  /** The item at the end of the edge. */
  node: ShortInfoText;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** Identifies documents */
export type ShortInfoTextManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<ShortInfoTextWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<ShortInfoTextWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<ShortInfoTextWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  image?: Maybe<AssetWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

export enum ShortInfoTextOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  HeaderAsc = "header_ASC",
  HeaderDesc = "header_DESC",
  ContentAsc = "content_ASC",
  ContentDesc = "content_DESC",
}

export type ShortInfoTextUpdateInput = {
  /** header input for default locale (en) */
  header?: Maybe<Scalars["String"]>;
  /** content input for default locale (en) */
  content?: Maybe<Scalars["String"]>;
  image?: Maybe<AssetUpdateOneInlineInput>;
  /** Manage document localizations */
  localizations?: Maybe<ShortInfoTextUpdateLocalizationsInput>;
};

export type ShortInfoTextUpdateLocalizationDataInput = {
  header?: Maybe<Scalars["String"]>;
  content?: Maybe<Scalars["String"]>;
};

export type ShortInfoTextUpdateLocalizationInput = {
  data: ShortInfoTextUpdateLocalizationDataInput;
  locale: Locale;
};

export type ShortInfoTextUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: Maybe<Array<ShortInfoTextCreateLocalizationInput>>;
  /** Localizations to update */
  update?: Maybe<Array<ShortInfoTextUpdateLocalizationInput>>;
  upsert?: Maybe<Array<ShortInfoTextUpsertLocalizationInput>>;
  /** Localizations to delete */
  delete?: Maybe<Array<Locale>>;
};

export type ShortInfoTextUpdateManyInlineInput = {
  /** Create and connect multiple ShortInfoText documents */
  create?: Maybe<Array<ShortInfoTextCreateInput>>;
  /** Connect multiple existing ShortInfoText documents */
  connect?: Maybe<Array<ShortInfoTextConnectInput>>;
  /** Override currently-connected documents with multiple existing ShortInfoText documents */
  set?: Maybe<Array<ShortInfoTextWhereUniqueInput>>;
  /** Update multiple ShortInfoText documents */
  update?: Maybe<Array<ShortInfoTextUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple ShortInfoText documents */
  upsert?: Maybe<Array<ShortInfoTextUpsertWithNestedWhereUniqueInput>>;
  /** Disconnect multiple ShortInfoText documents */
  disconnect?: Maybe<Array<ShortInfoTextWhereUniqueInput>>;
  /** Delete multiple ShortInfoText documents */
  delete?: Maybe<Array<ShortInfoTextWhereUniqueInput>>;
};

export type ShortInfoTextUpdateManyInput = {
  /** header input for default locale (en) */
  header?: Maybe<Scalars["String"]>;
  /** content input for default locale (en) */
  content?: Maybe<Scalars["String"]>;
  /** Optional updates to localizations */
  localizations?: Maybe<ShortInfoTextUpdateManyLocalizationsInput>;
};

export type ShortInfoTextUpdateManyLocalizationDataInput = {
  header?: Maybe<Scalars["String"]>;
  content?: Maybe<Scalars["String"]>;
};

export type ShortInfoTextUpdateManyLocalizationInput = {
  data: ShortInfoTextUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type ShortInfoTextUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: Maybe<Array<ShortInfoTextUpdateManyLocalizationInput>>;
};

export type ShortInfoTextUpdateManyWithNestedWhereInput = {
  /** Document search */
  where: ShortInfoTextWhereInput;
  /** Update many input */
  data: ShortInfoTextUpdateManyInput;
};

export type ShortInfoTextUpdateOneInlineInput = {
  /** Create and connect one ShortInfoText document */
  create?: Maybe<ShortInfoTextCreateInput>;
  /** Update single ShortInfoText document */
  update?: Maybe<ShortInfoTextUpdateWithNestedWhereUniqueInput>;
  /** Upsert single ShortInfoText document */
  upsert?: Maybe<ShortInfoTextUpsertWithNestedWhereUniqueInput>;
  /** Connect existing ShortInfoText document */
  connect?: Maybe<ShortInfoTextWhereUniqueInput>;
  /** Disconnect currently connected ShortInfoText document */
  disconnect?: Maybe<Scalars["Boolean"]>;
  /** Delete currently connected ShortInfoText document */
  delete?: Maybe<Scalars["Boolean"]>;
};

export type ShortInfoTextUpdateWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: ShortInfoTextWhereUniqueInput;
  /** Document to update */
  data: ShortInfoTextUpdateInput;
};

export type ShortInfoTextUpsertInput = {
  /** Create document if it didn't exist */
  create: ShortInfoTextCreateInput;
  /** Update document if it exists */
  update: ShortInfoTextUpdateInput;
};

export type ShortInfoTextUpsertLocalizationInput = {
  update: ShortInfoTextUpdateLocalizationDataInput;
  create: ShortInfoTextCreateLocalizationDataInput;
  locale: Locale;
};

export type ShortInfoTextUpsertWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: ShortInfoTextWhereUniqueInput;
  /** Upsert data */
  data: ShortInfoTextUpsertInput;
};

/** Identifies documents */
export type ShortInfoTextWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<ShortInfoTextWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<ShortInfoTextWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<ShortInfoTextWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  header?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  header_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  header_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  header_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  header_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  header_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  header_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  header_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  header_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  header_not_ends_with?: Maybe<Scalars["String"]>;
  content?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  content_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  content_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  content_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  content_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  content_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  content_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  content_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  content_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  content_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  image?: Maybe<AssetWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

/** References ShortInfoText record uniquely */
export type ShortInfoTextWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

/** People or groups that will hold workshops at the festival */
export type Speaker = Node & {
  __typename?: "Speaker";
  /** System stage field */
  stage: Stage;
  /** System Locale field */
  locale: Locale;
  /** Get the other localizations for this document */
  localizations: Array<Speaker>;
  /** Get the document in other stages */
  documentInStages: Array<Speaker>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  subtitle?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  descriptionMarkdown?: Maybe<Scalars["String"]>;
  /** Speakers are sorted by this, so Position 1 means they appear first and so on. */
  position?: Maybe<Scalars["Int"]>;
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  image?: Maybe<Asset>;
  scheduledIn: Array<ScheduledOperation>;
  /** List of Speaker versions */
  history: Array<Version>;
};

/** People or groups that will hold workshops at the festival */
export type SpeakerLocalizationsArgs = {
  locales?: Array<Locale>;
  includeCurrent?: Scalars["Boolean"];
};

/** People or groups that will hold workshops at the festival */
export type SpeakerDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

/** People or groups that will hold workshops at the festival */
export type SpeakerCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

/** People or groups that will hold workshops at the festival */
export type SpeakerUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

/** People or groups that will hold workshops at the festival */
export type SpeakerPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

/** People or groups that will hold workshops at the festival */
export type SpeakerCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** People or groups that will hold workshops at the festival */
export type SpeakerUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** People or groups that will hold workshops at the festival */
export type SpeakerPublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** People or groups that will hold workshops at the festival */
export type SpeakerImageArgs = {
  locales?: Maybe<Array<Locale>>;
};

/** People or groups that will hold workshops at the festival */
export type SpeakerScheduledInArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

/** People or groups that will hold workshops at the festival */
export type SpeakerHistoryArgs = {
  limit?: Scalars["Int"];
  skip?: Scalars["Int"];
  stageOverride?: Maybe<Stage>;
};

export type SpeakerConnectInput = {
  /** Document to connect */
  where: SpeakerWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type SpeakerConnection = {
  __typename?: "SpeakerConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<SpeakerEdge>;
  aggregate: Aggregate;
};

export type SpeakerCreateInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** subtitle input for default locale (en) */
  subtitle?: Maybe<Scalars["String"]>;
  /** description input for default locale (en) */
  description?: Maybe<Scalars["String"]>;
  /** descriptionMarkdown input for default locale (en) */
  descriptionMarkdown?: Maybe<Scalars["String"]>;
  position?: Maybe<Scalars["Int"]>;
  image?: Maybe<AssetCreateOneInlineInput>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: Maybe<SpeakerCreateLocalizationsInput>;
};

export type SpeakerCreateLocalizationDataInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  subtitle?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  descriptionMarkdown?: Maybe<Scalars["String"]>;
};

export type SpeakerCreateLocalizationInput = {
  /** Localization input */
  data: SpeakerCreateLocalizationDataInput;
  locale: Locale;
};

export type SpeakerCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: Maybe<Array<SpeakerCreateLocalizationInput>>;
};

export type SpeakerCreateManyInlineInput = {
  /** Create and connect multiple existing Speaker documents */
  create?: Maybe<Array<SpeakerCreateInput>>;
  /** Connect multiple existing Speaker documents */
  connect?: Maybe<Array<SpeakerWhereUniqueInput>>;
};

export type SpeakerCreateOneInlineInput = {
  /** Create and connect one Speaker document */
  create?: Maybe<SpeakerCreateInput>;
  /** Connect one existing Speaker document */
  connect?: Maybe<SpeakerWhereUniqueInput>;
};

/** An edge in a connection. */
export type SpeakerEdge = {
  __typename?: "SpeakerEdge";
  /** The item at the end of the edge. */
  node: Speaker;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** Identifies documents */
export type SpeakerManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<SpeakerWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<SpeakerWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<SpeakerWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  name_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  name_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  name_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  name_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  name_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  name_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  name_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  name_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  name_not_ends_with?: Maybe<Scalars["String"]>;
  position?: Maybe<Scalars["Int"]>;
  /** All values that are not equal to given value. */
  position_not?: Maybe<Scalars["Int"]>;
  /** All values that are contained in given list. */
  position_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values that are not contained in given list. */
  position_not_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values less than the given value. */
  position_lt?: Maybe<Scalars["Int"]>;
  /** All values less than or equal the given value. */
  position_lte?: Maybe<Scalars["Int"]>;
  /** All values greater than the given value. */
  position_gt?: Maybe<Scalars["Int"]>;
  /** All values greater than or equal the given value. */
  position_gte?: Maybe<Scalars["Int"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  image?: Maybe<AssetWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

export enum SpeakerOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  NameAsc = "name_ASC",
  NameDesc = "name_DESC",
  SubtitleAsc = "subtitle_ASC",
  SubtitleDesc = "subtitle_DESC",
  DescriptionAsc = "description_ASC",
  DescriptionDesc = "description_DESC",
  DescriptionMarkdownAsc = "descriptionMarkdown_ASC",
  DescriptionMarkdownDesc = "descriptionMarkdown_DESC",
  PositionAsc = "position_ASC",
  PositionDesc = "position_DESC",
}

export type SpeakerUpdateInput = {
  name?: Maybe<Scalars["String"]>;
  /** subtitle input for default locale (en) */
  subtitle?: Maybe<Scalars["String"]>;
  /** description input for default locale (en) */
  description?: Maybe<Scalars["String"]>;
  /** descriptionMarkdown input for default locale (en) */
  descriptionMarkdown?: Maybe<Scalars["String"]>;
  position?: Maybe<Scalars["Int"]>;
  image?: Maybe<AssetUpdateOneInlineInput>;
  /** Manage document localizations */
  localizations?: Maybe<SpeakerUpdateLocalizationsInput>;
};

export type SpeakerUpdateLocalizationDataInput = {
  subtitle?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  descriptionMarkdown?: Maybe<Scalars["String"]>;
};

export type SpeakerUpdateLocalizationInput = {
  data: SpeakerUpdateLocalizationDataInput;
  locale: Locale;
};

export type SpeakerUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: Maybe<Array<SpeakerCreateLocalizationInput>>;
  /** Localizations to update */
  update?: Maybe<Array<SpeakerUpdateLocalizationInput>>;
  upsert?: Maybe<Array<SpeakerUpsertLocalizationInput>>;
  /** Localizations to delete */
  delete?: Maybe<Array<Locale>>;
};

export type SpeakerUpdateManyInlineInput = {
  /** Create and connect multiple Speaker documents */
  create?: Maybe<Array<SpeakerCreateInput>>;
  /** Connect multiple existing Speaker documents */
  connect?: Maybe<Array<SpeakerConnectInput>>;
  /** Override currently-connected documents with multiple existing Speaker documents */
  set?: Maybe<Array<SpeakerWhereUniqueInput>>;
  /** Update multiple Speaker documents */
  update?: Maybe<Array<SpeakerUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple Speaker documents */
  upsert?: Maybe<Array<SpeakerUpsertWithNestedWhereUniqueInput>>;
  /** Disconnect multiple Speaker documents */
  disconnect?: Maybe<Array<SpeakerWhereUniqueInput>>;
  /** Delete multiple Speaker documents */
  delete?: Maybe<Array<SpeakerWhereUniqueInput>>;
};

export type SpeakerUpdateManyInput = {
  name?: Maybe<Scalars["String"]>;
  /** subtitle input for default locale (en) */
  subtitle?: Maybe<Scalars["String"]>;
  /** description input for default locale (en) */
  description?: Maybe<Scalars["String"]>;
  /** descriptionMarkdown input for default locale (en) */
  descriptionMarkdown?: Maybe<Scalars["String"]>;
  position?: Maybe<Scalars["Int"]>;
  /** Optional updates to localizations */
  localizations?: Maybe<SpeakerUpdateManyLocalizationsInput>;
};

export type SpeakerUpdateManyLocalizationDataInput = {
  subtitle?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  descriptionMarkdown?: Maybe<Scalars["String"]>;
};

export type SpeakerUpdateManyLocalizationInput = {
  data: SpeakerUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type SpeakerUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: Maybe<Array<SpeakerUpdateManyLocalizationInput>>;
};

export type SpeakerUpdateManyWithNestedWhereInput = {
  /** Document search */
  where: SpeakerWhereInput;
  /** Update many input */
  data: SpeakerUpdateManyInput;
};

export type SpeakerUpdateOneInlineInput = {
  /** Create and connect one Speaker document */
  create?: Maybe<SpeakerCreateInput>;
  /** Update single Speaker document */
  update?: Maybe<SpeakerUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Speaker document */
  upsert?: Maybe<SpeakerUpsertWithNestedWhereUniqueInput>;
  /** Connect existing Speaker document */
  connect?: Maybe<SpeakerWhereUniqueInput>;
  /** Disconnect currently connected Speaker document */
  disconnect?: Maybe<Scalars["Boolean"]>;
  /** Delete currently connected Speaker document */
  delete?: Maybe<Scalars["Boolean"]>;
};

export type SpeakerUpdateWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: SpeakerWhereUniqueInput;
  /** Document to update */
  data: SpeakerUpdateInput;
};

export type SpeakerUpsertInput = {
  /** Create document if it didn't exist */
  create: SpeakerCreateInput;
  /** Update document if it exists */
  update: SpeakerUpdateInput;
};

export type SpeakerUpsertLocalizationInput = {
  update: SpeakerUpdateLocalizationDataInput;
  create: SpeakerCreateLocalizationDataInput;
  locale: Locale;
};

export type SpeakerUpsertWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: SpeakerWhereUniqueInput;
  /** Upsert data */
  data: SpeakerUpsertInput;
};

/** Identifies documents */
export type SpeakerWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<SpeakerWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<SpeakerWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<SpeakerWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  name_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  name_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  name_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  name_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  name_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  name_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  name_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  name_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  name_not_ends_with?: Maybe<Scalars["String"]>;
  subtitle?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  subtitle_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  subtitle_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  subtitle_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  subtitle_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  subtitle_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  subtitle_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  subtitle_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  subtitle_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  subtitle_not_ends_with?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  description_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  description_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  description_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  description_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  description_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  description_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  description_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  description_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  description_not_ends_with?: Maybe<Scalars["String"]>;
  descriptionMarkdown?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  descriptionMarkdown_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  descriptionMarkdown_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  descriptionMarkdown_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  descriptionMarkdown_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  descriptionMarkdown_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  descriptionMarkdown_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  descriptionMarkdown_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  descriptionMarkdown_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  descriptionMarkdown_not_ends_with?: Maybe<Scalars["String"]>;
  position?: Maybe<Scalars["Int"]>;
  /** All values that are not equal to given value. */
  position_not?: Maybe<Scalars["Int"]>;
  /** All values that are contained in given list. */
  position_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values that are not contained in given list. */
  position_not_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values less than the given value. */
  position_lt?: Maybe<Scalars["Int"]>;
  /** All values less than or equal the given value. */
  position_lte?: Maybe<Scalars["Int"]>;
  /** All values greater than the given value. */
  position_gt?: Maybe<Scalars["Int"]>;
  /** All values greater than or equal the given value. */
  position_gte?: Maybe<Scalars["Int"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  image?: Maybe<AssetWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

/** References Speaker record uniquely */
export type SpeakerWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

export type Sponsor = Node & {
  __typename?: "Sponsor";
  /** System stage field */
  stage: Stage;
  /** Get the document in other stages */
  documentInStages: Array<Sponsor>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  url?: Maybe<Scalars["String"]>;
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  logo?: Maybe<Asset>;
  scheduledIn: Array<ScheduledOperation>;
  /** List of Sponsor versions */
  history: Array<Version>;
};

export type SponsorDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

export type SponsorCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type SponsorUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type SponsorPublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type SponsorLogoArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type SponsorScheduledInArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

export type SponsorHistoryArgs = {
  limit?: Scalars["Int"];
  skip?: Scalars["Int"];
  stageOverride?: Maybe<Stage>;
};

export type SponsorConnectInput = {
  /** Document to connect */
  where: SponsorWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type SponsorConnection = {
  __typename?: "SponsorConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<SponsorEdge>;
  aggregate: Aggregate;
};

export type SponsorCreateInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  url?: Maybe<Scalars["String"]>;
  logo?: Maybe<AssetCreateOneInlineInput>;
};

export type SponsorCreateManyInlineInput = {
  /** Create and connect multiple existing Sponsor documents */
  create?: Maybe<Array<SponsorCreateInput>>;
  /** Connect multiple existing Sponsor documents */
  connect?: Maybe<Array<SponsorWhereUniqueInput>>;
};

export type SponsorCreateOneInlineInput = {
  /** Create and connect one Sponsor document */
  create?: Maybe<SponsorCreateInput>;
  /** Connect one existing Sponsor document */
  connect?: Maybe<SponsorWhereUniqueInput>;
};

/** An edge in a connection. */
export type SponsorEdge = {
  __typename?: "SponsorEdge";
  /** The item at the end of the edge. */
  node: Sponsor;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** Identifies documents */
export type SponsorManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<SponsorWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<SponsorWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<SponsorWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  name_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  name_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  name_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  name_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  name_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  name_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  name_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  name_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  name_not_ends_with?: Maybe<Scalars["String"]>;
  url?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  url_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  url_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  url_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  url_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  url_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  url_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  url_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  url_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  url_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  logo?: Maybe<AssetWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

export enum SponsorOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  NameAsc = "name_ASC",
  NameDesc = "name_DESC",
  UrlAsc = "url_ASC",
  UrlDesc = "url_DESC",
}

export type SponsorUpdateInput = {
  name?: Maybe<Scalars["String"]>;
  url?: Maybe<Scalars["String"]>;
  logo?: Maybe<AssetUpdateOneInlineInput>;
};

export type SponsorUpdateManyInlineInput = {
  /** Create and connect multiple Sponsor documents */
  create?: Maybe<Array<SponsorCreateInput>>;
  /** Connect multiple existing Sponsor documents */
  connect?: Maybe<Array<SponsorConnectInput>>;
  /** Override currently-connected documents with multiple existing Sponsor documents */
  set?: Maybe<Array<SponsorWhereUniqueInput>>;
  /** Update multiple Sponsor documents */
  update?: Maybe<Array<SponsorUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple Sponsor documents */
  upsert?: Maybe<Array<SponsorUpsertWithNestedWhereUniqueInput>>;
  /** Disconnect multiple Sponsor documents */
  disconnect?: Maybe<Array<SponsorWhereUniqueInput>>;
  /** Delete multiple Sponsor documents */
  delete?: Maybe<Array<SponsorWhereUniqueInput>>;
};

export type SponsorUpdateManyInput = {
  name?: Maybe<Scalars["String"]>;
  url?: Maybe<Scalars["String"]>;
};

export type SponsorUpdateManyWithNestedWhereInput = {
  /** Document search */
  where: SponsorWhereInput;
  /** Update many input */
  data: SponsorUpdateManyInput;
};

export type SponsorUpdateOneInlineInput = {
  /** Create and connect one Sponsor document */
  create?: Maybe<SponsorCreateInput>;
  /** Update single Sponsor document */
  update?: Maybe<SponsorUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Sponsor document */
  upsert?: Maybe<SponsorUpsertWithNestedWhereUniqueInput>;
  /** Connect existing Sponsor document */
  connect?: Maybe<SponsorWhereUniqueInput>;
  /** Disconnect currently connected Sponsor document */
  disconnect?: Maybe<Scalars["Boolean"]>;
  /** Delete currently connected Sponsor document */
  delete?: Maybe<Scalars["Boolean"]>;
};

export type SponsorUpdateWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: SponsorWhereUniqueInput;
  /** Document to update */
  data: SponsorUpdateInput;
};

export type SponsorUpsertInput = {
  /** Create document if it didn't exist */
  create: SponsorCreateInput;
  /** Update document if it exists */
  update: SponsorUpdateInput;
};

export type SponsorUpsertWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: SponsorWhereUniqueInput;
  /** Upsert data */
  data: SponsorUpsertInput;
};

/** Identifies documents */
export type SponsorWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<SponsorWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<SponsorWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<SponsorWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  name_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  name_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  name_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  name_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  name_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  name_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  name_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  name_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  name_not_ends_with?: Maybe<Scalars["String"]>;
  url?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  url_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  url_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  url_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  url_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  url_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  url_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  url_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  url_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  url_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  logo?: Maybe<AssetWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

/** References Sponsor record uniquely */
export type SponsorWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

/** Stage system enumeration */
export enum Stage {
  /** The Draft is the default stage for all your content. */
  Draft = "DRAFT",
  /** The Published stage is where you can publish your content to. */
  Published = "PUBLISHED",
}

export enum SystemDateTimeFieldVariation {
  Base = "BASE",
  Localization = "LOCALIZATION",
  Combined = "COMBINED",
}

export type Team = Node & {
  __typename?: "Team";
  /** System stage field */
  stage: Stage;
  /** System Locale field */
  locale: Locale;
  /** Get the other localizations for this document */
  localizations: Array<Team>;
  /** Get the document in other stages */
  documentInStages: Array<Team>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  name: Scalars["String"];
  description?: Maybe<Scalars["String"]>;
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  teamMembers: Array<TeamMember>;
  scheduledIn: Array<ScheduledOperation>;
  /** List of Team versions */
  history: Array<Version>;
};

export type TeamLocalizationsArgs = {
  locales?: Array<Locale>;
  includeCurrent?: Scalars["Boolean"];
};

export type TeamDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

export type TeamCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type TeamUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type TeamPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type TeamCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TeamUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TeamPublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TeamTeamMembersArgs = {
  where?: Maybe<TeamMemberWhereInput>;
  orderBy?: Maybe<TeamMemberOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

export type TeamScheduledInArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

export type TeamHistoryArgs = {
  limit?: Scalars["Int"];
  skip?: Scalars["Int"];
  stageOverride?: Maybe<Stage>;
};

export type TeamConnectInput = {
  /** Document to connect */
  where: TeamWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type TeamConnection = {
  __typename?: "TeamConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<TeamEdge>;
  aggregate: Aggregate;
};

export type TeamCreateInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** name input for default locale (en) */
  name: Scalars["String"];
  /** description input for default locale (en) */
  description?: Maybe<Scalars["String"]>;
  teamMembers?: Maybe<TeamMemberCreateManyInlineInput>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: Maybe<TeamCreateLocalizationsInput>;
};

export type TeamCreateLocalizationDataInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  name: Scalars["String"];
  description?: Maybe<Scalars["String"]>;
};

export type TeamCreateLocalizationInput = {
  /** Localization input */
  data: TeamCreateLocalizationDataInput;
  locale: Locale;
};

export type TeamCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: Maybe<Array<TeamCreateLocalizationInput>>;
};

export type TeamCreateManyInlineInput = {
  /** Create and connect multiple existing Team documents */
  create?: Maybe<Array<TeamCreateInput>>;
  /** Connect multiple existing Team documents */
  connect?: Maybe<Array<TeamWhereUniqueInput>>;
};

export type TeamCreateOneInlineInput = {
  /** Create and connect one Team document */
  create?: Maybe<TeamCreateInput>;
  /** Connect one existing Team document */
  connect?: Maybe<TeamWhereUniqueInput>;
};

/** An edge in a connection. */
export type TeamEdge = {
  __typename?: "TeamEdge";
  /** The item at the end of the edge. */
  node: Team;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** Identifies documents */
export type TeamManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<TeamWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<TeamWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<TeamWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  teamMembers_every?: Maybe<TeamMemberWhereInput>;
  teamMembers_some?: Maybe<TeamMemberWhereInput>;
  teamMembers_none?: Maybe<TeamMemberWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

export type TeamMember = Node & {
  __typename?: "TeamMember";
  /** System stage field */
  stage: Stage;
  /** System Locale field */
  locale: Locale;
  /** Get the other localizations for this document */
  localizations: Array<TeamMember>;
  /** Get the document in other stages */
  documentInStages: Array<TeamMember>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  role?: Maybe<Scalars["String"]>;
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  image?: Maybe<Asset>;
  team?: Maybe<Team>;
  scheduledIn: Array<ScheduledOperation>;
  /** List of TeamMember versions */
  history: Array<Version>;
};

export type TeamMemberLocalizationsArgs = {
  locales?: Array<Locale>;
  includeCurrent?: Scalars["Boolean"];
};

export type TeamMemberDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

export type TeamMemberCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type TeamMemberUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type TeamMemberPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type TeamMemberCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TeamMemberUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TeamMemberPublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TeamMemberImageArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TeamMemberTeamArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TeamMemberScheduledInArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

export type TeamMemberHistoryArgs = {
  limit?: Scalars["Int"];
  skip?: Scalars["Int"];
  stageOverride?: Maybe<Stage>;
};

export type TeamMemberConnectInput = {
  /** Document to connect */
  where: TeamMemberWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type TeamMemberConnection = {
  __typename?: "TeamMemberConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<TeamMemberEdge>;
  aggregate: Aggregate;
};

export type TeamMemberCreateInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** role input for default locale (en) */
  role?: Maybe<Scalars["String"]>;
  image?: Maybe<AssetCreateOneInlineInput>;
  team?: Maybe<TeamCreateOneInlineInput>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: Maybe<TeamMemberCreateLocalizationsInput>;
};

export type TeamMemberCreateLocalizationDataInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  role?: Maybe<Scalars["String"]>;
};

export type TeamMemberCreateLocalizationInput = {
  /** Localization input */
  data: TeamMemberCreateLocalizationDataInput;
  locale: Locale;
};

export type TeamMemberCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: Maybe<Array<TeamMemberCreateLocalizationInput>>;
};

export type TeamMemberCreateManyInlineInput = {
  /** Create and connect multiple existing TeamMember documents */
  create?: Maybe<Array<TeamMemberCreateInput>>;
  /** Connect multiple existing TeamMember documents */
  connect?: Maybe<Array<TeamMemberWhereUniqueInput>>;
};

export type TeamMemberCreateOneInlineInput = {
  /** Create and connect one TeamMember document */
  create?: Maybe<TeamMemberCreateInput>;
  /** Connect one existing TeamMember document */
  connect?: Maybe<TeamMemberWhereUniqueInput>;
};

/** An edge in a connection. */
export type TeamMemberEdge = {
  __typename?: "TeamMemberEdge";
  /** The item at the end of the edge. */
  node: TeamMember;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** Identifies documents */
export type TeamMemberManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<TeamMemberWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<TeamMemberWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<TeamMemberWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  name_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  name_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  name_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  name_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  name_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  name_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  name_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  name_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  name_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  image?: Maybe<AssetWhereInput>;
  team?: Maybe<TeamWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

export enum TeamMemberOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  NameAsc = "name_ASC",
  NameDesc = "name_DESC",
  RoleAsc = "role_ASC",
  RoleDesc = "role_DESC",
}

export type TeamMemberUpdateInput = {
  name?: Maybe<Scalars["String"]>;
  /** role input for default locale (en) */
  role?: Maybe<Scalars["String"]>;
  image?: Maybe<AssetUpdateOneInlineInput>;
  team?: Maybe<TeamUpdateOneInlineInput>;
  /** Manage document localizations */
  localizations?: Maybe<TeamMemberUpdateLocalizationsInput>;
};

export type TeamMemberUpdateLocalizationDataInput = {
  role?: Maybe<Scalars["String"]>;
};

export type TeamMemberUpdateLocalizationInput = {
  data: TeamMemberUpdateLocalizationDataInput;
  locale: Locale;
};

export type TeamMemberUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: Maybe<Array<TeamMemberCreateLocalizationInput>>;
  /** Localizations to update */
  update?: Maybe<Array<TeamMemberUpdateLocalizationInput>>;
  upsert?: Maybe<Array<TeamMemberUpsertLocalizationInput>>;
  /** Localizations to delete */
  delete?: Maybe<Array<Locale>>;
};

export type TeamMemberUpdateManyInlineInput = {
  /** Create and connect multiple TeamMember documents */
  create?: Maybe<Array<TeamMemberCreateInput>>;
  /** Connect multiple existing TeamMember documents */
  connect?: Maybe<Array<TeamMemberConnectInput>>;
  /** Override currently-connected documents with multiple existing TeamMember documents */
  set?: Maybe<Array<TeamMemberWhereUniqueInput>>;
  /** Update multiple TeamMember documents */
  update?: Maybe<Array<TeamMemberUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple TeamMember documents */
  upsert?: Maybe<Array<TeamMemberUpsertWithNestedWhereUniqueInput>>;
  /** Disconnect multiple TeamMember documents */
  disconnect?: Maybe<Array<TeamMemberWhereUniqueInput>>;
  /** Delete multiple TeamMember documents */
  delete?: Maybe<Array<TeamMemberWhereUniqueInput>>;
};

export type TeamMemberUpdateManyInput = {
  name?: Maybe<Scalars["String"]>;
  /** role input for default locale (en) */
  role?: Maybe<Scalars["String"]>;
  /** Optional updates to localizations */
  localizations?: Maybe<TeamMemberUpdateManyLocalizationsInput>;
};

export type TeamMemberUpdateManyLocalizationDataInput = {
  role?: Maybe<Scalars["String"]>;
};

export type TeamMemberUpdateManyLocalizationInput = {
  data: TeamMemberUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type TeamMemberUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: Maybe<Array<TeamMemberUpdateManyLocalizationInput>>;
};

export type TeamMemberUpdateManyWithNestedWhereInput = {
  /** Document search */
  where: TeamMemberWhereInput;
  /** Update many input */
  data: TeamMemberUpdateManyInput;
};

export type TeamMemberUpdateOneInlineInput = {
  /** Create and connect one TeamMember document */
  create?: Maybe<TeamMemberCreateInput>;
  /** Update single TeamMember document */
  update?: Maybe<TeamMemberUpdateWithNestedWhereUniqueInput>;
  /** Upsert single TeamMember document */
  upsert?: Maybe<TeamMemberUpsertWithNestedWhereUniqueInput>;
  /** Connect existing TeamMember document */
  connect?: Maybe<TeamMemberWhereUniqueInput>;
  /** Disconnect currently connected TeamMember document */
  disconnect?: Maybe<Scalars["Boolean"]>;
  /** Delete currently connected TeamMember document */
  delete?: Maybe<Scalars["Boolean"]>;
};

export type TeamMemberUpdateWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: TeamMemberWhereUniqueInput;
  /** Document to update */
  data: TeamMemberUpdateInput;
};

export type TeamMemberUpsertInput = {
  /** Create document if it didn't exist */
  create: TeamMemberCreateInput;
  /** Update document if it exists */
  update: TeamMemberUpdateInput;
};

export type TeamMemberUpsertLocalizationInput = {
  update: TeamMemberUpdateLocalizationDataInput;
  create: TeamMemberCreateLocalizationDataInput;
  locale: Locale;
};

export type TeamMemberUpsertWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: TeamMemberWhereUniqueInput;
  /** Upsert data */
  data: TeamMemberUpsertInput;
};

/** Identifies documents */
export type TeamMemberWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<TeamMemberWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<TeamMemberWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<TeamMemberWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  name_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  name_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  name_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  name_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  name_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  name_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  name_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  name_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  name_not_ends_with?: Maybe<Scalars["String"]>;
  role?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  role_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  role_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  role_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  role_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  role_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  role_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  role_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  role_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  role_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  image?: Maybe<AssetWhereInput>;
  team?: Maybe<TeamWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

/** References TeamMember record uniquely */
export type TeamMemberWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

export enum TeamOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  NameAsc = "name_ASC",
  NameDesc = "name_DESC",
  DescriptionAsc = "description_ASC",
  DescriptionDesc = "description_DESC",
}

export type TeamUpdateInput = {
  /** name input for default locale (en) */
  name?: Maybe<Scalars["String"]>;
  /** description input for default locale (en) */
  description?: Maybe<Scalars["String"]>;
  teamMembers?: Maybe<TeamMemberUpdateManyInlineInput>;
  /** Manage document localizations */
  localizations?: Maybe<TeamUpdateLocalizationsInput>;
};

export type TeamUpdateLocalizationDataInput = {
  name?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
};

export type TeamUpdateLocalizationInput = {
  data: TeamUpdateLocalizationDataInput;
  locale: Locale;
};

export type TeamUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: Maybe<Array<TeamCreateLocalizationInput>>;
  /** Localizations to update */
  update?: Maybe<Array<TeamUpdateLocalizationInput>>;
  upsert?: Maybe<Array<TeamUpsertLocalizationInput>>;
  /** Localizations to delete */
  delete?: Maybe<Array<Locale>>;
};

export type TeamUpdateManyInlineInput = {
  /** Create and connect multiple Team documents */
  create?: Maybe<Array<TeamCreateInput>>;
  /** Connect multiple existing Team documents */
  connect?: Maybe<Array<TeamConnectInput>>;
  /** Override currently-connected documents with multiple existing Team documents */
  set?: Maybe<Array<TeamWhereUniqueInput>>;
  /** Update multiple Team documents */
  update?: Maybe<Array<TeamUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple Team documents */
  upsert?: Maybe<Array<TeamUpsertWithNestedWhereUniqueInput>>;
  /** Disconnect multiple Team documents */
  disconnect?: Maybe<Array<TeamWhereUniqueInput>>;
  /** Delete multiple Team documents */
  delete?: Maybe<Array<TeamWhereUniqueInput>>;
};

export type TeamUpdateManyInput = {
  /** description input for default locale (en) */
  description?: Maybe<Scalars["String"]>;
  /** Optional updates to localizations */
  localizations?: Maybe<TeamUpdateManyLocalizationsInput>;
};

export type TeamUpdateManyLocalizationDataInput = {
  description?: Maybe<Scalars["String"]>;
};

export type TeamUpdateManyLocalizationInput = {
  data: TeamUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type TeamUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: Maybe<Array<TeamUpdateManyLocalizationInput>>;
};

export type TeamUpdateManyWithNestedWhereInput = {
  /** Document search */
  where: TeamWhereInput;
  /** Update many input */
  data: TeamUpdateManyInput;
};

export type TeamUpdateOneInlineInput = {
  /** Create and connect one Team document */
  create?: Maybe<TeamCreateInput>;
  /** Update single Team document */
  update?: Maybe<TeamUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Team document */
  upsert?: Maybe<TeamUpsertWithNestedWhereUniqueInput>;
  /** Connect existing Team document */
  connect?: Maybe<TeamWhereUniqueInput>;
  /** Disconnect currently connected Team document */
  disconnect?: Maybe<Scalars["Boolean"]>;
  /** Delete currently connected Team document */
  delete?: Maybe<Scalars["Boolean"]>;
};

export type TeamUpdateWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: TeamWhereUniqueInput;
  /** Document to update */
  data: TeamUpdateInput;
};

export type TeamUpsertInput = {
  /** Create document if it didn't exist */
  create: TeamCreateInput;
  /** Update document if it exists */
  update: TeamUpdateInput;
};

export type TeamUpsertLocalizationInput = {
  update: TeamUpdateLocalizationDataInput;
  create: TeamCreateLocalizationDataInput;
  locale: Locale;
};

export type TeamUpsertWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: TeamWhereUniqueInput;
  /** Upsert data */
  data: TeamUpsertInput;
};

/** Identifies documents */
export type TeamWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<TeamWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<TeamWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<TeamWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  name_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  name_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  name_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  name_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  name_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  name_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  name_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  name_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  name_not_ends_with?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  description_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  description_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  description_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  description_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  description_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  description_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  description_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  description_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  description_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  teamMembers_every?: Maybe<TeamMemberWhereInput>;
  teamMembers_some?: Maybe<TeamMemberWhereInput>;
  teamMembers_none?: Maybe<TeamMemberWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

/** References Team record uniquely */
export type TeamWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

export type Testimonial = Node & {
  __typename?: "Testimonial";
  /** System stage field */
  stage: Stage;
  /** System Locale field */
  locale: Locale;
  /** Get the other localizations for this document */
  localizations: Array<Testimonial>;
  /** Get the document in other stages */
  documentInStages: Array<Testimonial>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** Who said this */
  from: Scalars["String"];
  /** What the person said */
  said?: Maybe<Scalars["String"]>;
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  image?: Maybe<Asset>;
  scheduledIn: Array<ScheduledOperation>;
  /** List of Testimonial versions */
  history: Array<Version>;
};

export type TestimonialLocalizationsArgs = {
  locales?: Array<Locale>;
  includeCurrent?: Scalars["Boolean"];
};

export type TestimonialDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

export type TestimonialCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type TestimonialUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type TestimonialPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type TestimonialCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TestimonialUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TestimonialPublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TestimonialImageArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TestimonialScheduledInArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

export type TestimonialHistoryArgs = {
  limit?: Scalars["Int"];
  skip?: Scalars["Int"];
  stageOverride?: Maybe<Stage>;
};

export type TestimonialConnectInput = {
  /** Document to connect */
  where: TestimonialWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type TestimonialConnection = {
  __typename?: "TestimonialConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<TestimonialEdge>;
  aggregate: Aggregate;
};

export type TestimonialCreateInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  from: Scalars["String"];
  /** said input for default locale (en) */
  said?: Maybe<Scalars["String"]>;
  image?: Maybe<AssetCreateOneInlineInput>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: Maybe<TestimonialCreateLocalizationsInput>;
};

export type TestimonialCreateLocalizationDataInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  said?: Maybe<Scalars["String"]>;
};

export type TestimonialCreateLocalizationInput = {
  /** Localization input */
  data: TestimonialCreateLocalizationDataInput;
  locale: Locale;
};

export type TestimonialCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: Maybe<Array<TestimonialCreateLocalizationInput>>;
};

export type TestimonialCreateManyInlineInput = {
  /** Create and connect multiple existing Testimonial documents */
  create?: Maybe<Array<TestimonialCreateInput>>;
  /** Connect multiple existing Testimonial documents */
  connect?: Maybe<Array<TestimonialWhereUniqueInput>>;
};

export type TestimonialCreateOneInlineInput = {
  /** Create and connect one Testimonial document */
  create?: Maybe<TestimonialCreateInput>;
  /** Connect one existing Testimonial document */
  connect?: Maybe<TestimonialWhereUniqueInput>;
};

/** An edge in a connection. */
export type TestimonialEdge = {
  __typename?: "TestimonialEdge";
  /** The item at the end of the edge. */
  node: Testimonial;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** Identifies documents */
export type TestimonialManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<TestimonialWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<TestimonialWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<TestimonialWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  from?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  from_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  from_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  from_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  from_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  from_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  from_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  from_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  from_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  from_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  image?: Maybe<AssetWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

export enum TestimonialOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  FromAsc = "from_ASC",
  FromDesc = "from_DESC",
  SaidAsc = "said_ASC",
  SaidDesc = "said_DESC",
}

export type TestimonialUpdateInput = {
  from?: Maybe<Scalars["String"]>;
  /** said input for default locale (en) */
  said?: Maybe<Scalars["String"]>;
  image?: Maybe<AssetUpdateOneInlineInput>;
  /** Manage document localizations */
  localizations?: Maybe<TestimonialUpdateLocalizationsInput>;
};

export type TestimonialUpdateLocalizationDataInput = {
  said?: Maybe<Scalars["String"]>;
};

export type TestimonialUpdateLocalizationInput = {
  data: TestimonialUpdateLocalizationDataInput;
  locale: Locale;
};

export type TestimonialUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: Maybe<Array<TestimonialCreateLocalizationInput>>;
  /** Localizations to update */
  update?: Maybe<Array<TestimonialUpdateLocalizationInput>>;
  upsert?: Maybe<Array<TestimonialUpsertLocalizationInput>>;
  /** Localizations to delete */
  delete?: Maybe<Array<Locale>>;
};

export type TestimonialUpdateManyInlineInput = {
  /** Create and connect multiple Testimonial documents */
  create?: Maybe<Array<TestimonialCreateInput>>;
  /** Connect multiple existing Testimonial documents */
  connect?: Maybe<Array<TestimonialConnectInput>>;
  /** Override currently-connected documents with multiple existing Testimonial documents */
  set?: Maybe<Array<TestimonialWhereUniqueInput>>;
  /** Update multiple Testimonial documents */
  update?: Maybe<Array<TestimonialUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple Testimonial documents */
  upsert?: Maybe<Array<TestimonialUpsertWithNestedWhereUniqueInput>>;
  /** Disconnect multiple Testimonial documents */
  disconnect?: Maybe<Array<TestimonialWhereUniqueInput>>;
  /** Delete multiple Testimonial documents */
  delete?: Maybe<Array<TestimonialWhereUniqueInput>>;
};

export type TestimonialUpdateManyInput = {
  from?: Maybe<Scalars["String"]>;
  /** said input for default locale (en) */
  said?: Maybe<Scalars["String"]>;
  /** Optional updates to localizations */
  localizations?: Maybe<TestimonialUpdateManyLocalizationsInput>;
};

export type TestimonialUpdateManyLocalizationDataInput = {
  said?: Maybe<Scalars["String"]>;
};

export type TestimonialUpdateManyLocalizationInput = {
  data: TestimonialUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type TestimonialUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: Maybe<Array<TestimonialUpdateManyLocalizationInput>>;
};

export type TestimonialUpdateManyWithNestedWhereInput = {
  /** Document search */
  where: TestimonialWhereInput;
  /** Update many input */
  data: TestimonialUpdateManyInput;
};

export type TestimonialUpdateOneInlineInput = {
  /** Create and connect one Testimonial document */
  create?: Maybe<TestimonialCreateInput>;
  /** Update single Testimonial document */
  update?: Maybe<TestimonialUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Testimonial document */
  upsert?: Maybe<TestimonialUpsertWithNestedWhereUniqueInput>;
  /** Connect existing Testimonial document */
  connect?: Maybe<TestimonialWhereUniqueInput>;
  /** Disconnect currently connected Testimonial document */
  disconnect?: Maybe<Scalars["Boolean"]>;
  /** Delete currently connected Testimonial document */
  delete?: Maybe<Scalars["Boolean"]>;
};

export type TestimonialUpdateWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: TestimonialWhereUniqueInput;
  /** Document to update */
  data: TestimonialUpdateInput;
};

export type TestimonialUpsertInput = {
  /** Create document if it didn't exist */
  create: TestimonialCreateInput;
  /** Update document if it exists */
  update: TestimonialUpdateInput;
};

export type TestimonialUpsertLocalizationInput = {
  update: TestimonialUpdateLocalizationDataInput;
  create: TestimonialCreateLocalizationDataInput;
  locale: Locale;
};

export type TestimonialUpsertWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: TestimonialWhereUniqueInput;
  /** Upsert data */
  data: TestimonialUpsertInput;
};

/** Identifies documents */
export type TestimonialWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<TestimonialWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<TestimonialWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<TestimonialWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  from?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  from_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  from_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  from_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  from_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  from_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  from_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  from_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  from_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  from_not_ends_with?: Maybe<Scalars["String"]>;
  said?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  said_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  said_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  said_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  said_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  said_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  said_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  said_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  said_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  said_not_ends_with?: Maybe<Scalars["String"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  image?: Maybe<AssetWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

/** References Testimonial record uniquely */
export type TestimonialWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

export type Ticket = Node & {
  __typename?: "Ticket";
  /** System stage field */
  stage: Stage;
  /** System Locale field */
  locale: Locale;
  /** Get the other localizations for this document */
  localizations: Array<Ticket>;
  /** Get the document in other stages */
  documentInStages: Array<Ticket>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  name: Scalars["String"];
  description?: Maybe<Scalars["String"]>;
  price: Scalars["Float"];
  available?: Maybe<Scalars["Int"]>;
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  ticketOrders: Array<TicketOrder>;
  scheduledIn: Array<ScheduledOperation>;
  /** List of Ticket versions */
  history: Array<Version>;
};

export type TicketLocalizationsArgs = {
  locales?: Array<Locale>;
  includeCurrent?: Scalars["Boolean"];
};

export type TicketDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

export type TicketCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type TicketUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type TicketPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type TicketCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TicketUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TicketPublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TicketTicketOrdersArgs = {
  where?: Maybe<TicketOrderWhereInput>;
  orderBy?: Maybe<TicketOrderOrderByInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

export type TicketScheduledInArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

export type TicketHistoryArgs = {
  limit?: Scalars["Int"];
  skip?: Scalars["Int"];
  stageOverride?: Maybe<Stage>;
};

export type TicketConnectInput = {
  /** Document to connect */
  where: TicketWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type TicketConnection = {
  __typename?: "TicketConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<TicketEdge>;
  aggregate: Aggregate;
};

export type TicketCreateInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** name input for default locale (en) */
  name: Scalars["String"];
  /** description input for default locale (en) */
  description?: Maybe<Scalars["String"]>;
  price: Scalars["Float"];
  available?: Maybe<Scalars["Int"]>;
  ticketOrders?: Maybe<TicketOrderCreateManyInlineInput>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: Maybe<TicketCreateLocalizationsInput>;
};

export type TicketCreateLocalizationDataInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  name: Scalars["String"];
  description?: Maybe<Scalars["String"]>;
};

export type TicketCreateLocalizationInput = {
  /** Localization input */
  data: TicketCreateLocalizationDataInput;
  locale: Locale;
};

export type TicketCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: Maybe<Array<TicketCreateLocalizationInput>>;
};

export type TicketCreateManyInlineInput = {
  /** Create and connect multiple existing Ticket documents */
  create?: Maybe<Array<TicketCreateInput>>;
  /** Connect multiple existing Ticket documents */
  connect?: Maybe<Array<TicketWhereUniqueInput>>;
};

export type TicketCreateOneInlineInput = {
  /** Create and connect one Ticket document */
  create?: Maybe<TicketCreateInput>;
  /** Connect one existing Ticket document */
  connect?: Maybe<TicketWhereUniqueInput>;
};

/** An edge in a connection. */
export type TicketEdge = {
  __typename?: "TicketEdge";
  /** The item at the end of the edge. */
  node: Ticket;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** Identifies documents */
export type TicketManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<TicketWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<TicketWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<TicketWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  price?: Maybe<Scalars["Float"]>;
  /** All values that are not equal to given value. */
  price_not?: Maybe<Scalars["Float"]>;
  /** All values that are contained in given list. */
  price_in?: Maybe<Array<Scalars["Float"]>>;
  /** All values that are not contained in given list. */
  price_not_in?: Maybe<Array<Scalars["Float"]>>;
  /** All values less than the given value. */
  price_lt?: Maybe<Scalars["Float"]>;
  /** All values less than or equal the given value. */
  price_lte?: Maybe<Scalars["Float"]>;
  /** All values greater than the given value. */
  price_gt?: Maybe<Scalars["Float"]>;
  /** All values greater than or equal the given value. */
  price_gte?: Maybe<Scalars["Float"]>;
  available?: Maybe<Scalars["Int"]>;
  /** All values that are not equal to given value. */
  available_not?: Maybe<Scalars["Int"]>;
  /** All values that are contained in given list. */
  available_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values that are not contained in given list. */
  available_not_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values less than the given value. */
  available_lt?: Maybe<Scalars["Int"]>;
  /** All values less than or equal the given value. */
  available_lte?: Maybe<Scalars["Int"]>;
  /** All values greater than the given value. */
  available_gt?: Maybe<Scalars["Int"]>;
  /** All values greater than or equal the given value. */
  available_gte?: Maybe<Scalars["Int"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  ticketOrders_every?: Maybe<TicketOrderWhereInput>;
  ticketOrders_some?: Maybe<TicketOrderWhereInput>;
  ticketOrders_none?: Maybe<TicketOrderWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

export type TicketOrder = Node & {
  __typename?: "TicketOrder";
  /** System stage field */
  stage: Stage;
  /** Get the document in other stages */
  documentInStages: Array<TicketOrder>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  name: Scalars["String"];
  age: Scalars["Int"];
  email: Scalars["String"];
  address: Scalars["String"];
  /** We may set a custom price for peoples ticket. */
  customPrice?: Maybe<Scalars["Int"]>;
  /** Why this order get's a custom price */
  customPriceReason?: Maybe<Scalars["String"]>;
  paidAt?: Maybe<Scalars["DateTime"]>;
  ticketSentAt?: Maybe<Scalars["DateTime"]>;
  needsTranslation?: Maybe<Scalars["Boolean"]>;
  /** Additional notes for this order. */
  additionalNotes?: Maybe<Scalars["String"]>;
  /** On what day this order was cancelled */
  cancelledOn?: Maybe<Scalars["Date"]>;
  /** when this ticket was checked in */
  checkInTime?: Maybe<Scalars["DateTime"]>;
  /** User that created this document */
  createdBy?: Maybe<User>;
  /** User that last updated this document */
  updatedBy?: Maybe<User>;
  /** User that last published this document */
  publishedBy?: Maybe<User>;
  ticket?: Maybe<Ticket>;
  scheduledIn: Array<ScheduledOperation>;
  /** List of TicketOrder versions */
  history: Array<Version>;
};

export type TicketOrderDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

export type TicketOrderCreatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TicketOrderUpdatedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TicketOrderPublishedByArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TicketOrderTicketArgs = {
  locales?: Maybe<Array<Locale>>;
};

export type TicketOrderScheduledInArgs = {
  where?: Maybe<ScheduledOperationWhereInput>;
  skip?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  before?: Maybe<Scalars["String"]>;
  first?: Maybe<Scalars["Int"]>;
  last?: Maybe<Scalars["Int"]>;
  locales?: Maybe<Array<Locale>>;
};

export type TicketOrderHistoryArgs = {
  limit?: Scalars["Int"];
  skip?: Scalars["Int"];
  stageOverride?: Maybe<Stage>;
};

export enum TicketOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  NameAsc = "name_ASC",
  NameDesc = "name_DESC",
  DescriptionAsc = "description_ASC",
  DescriptionDesc = "description_DESC",
  PriceAsc = "price_ASC",
  PriceDesc = "price_DESC",
  AvailableAsc = "available_ASC",
  AvailableDesc = "available_DESC",
}

export type TicketOrderConnectInput = {
  /** Document to connect */
  where: TicketOrderWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type TicketOrderConnection = {
  __typename?: "TicketOrderConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<TicketOrderEdge>;
  aggregate: Aggregate;
};

export type TicketOrderCreateInput = {
  createdAt?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  name: Scalars["String"];
  age: Scalars["Int"];
  email: Scalars["String"];
  address: Scalars["String"];
  customPrice?: Maybe<Scalars["Int"]>;
  customPriceReason?: Maybe<Scalars["String"]>;
  paidAt?: Maybe<Scalars["DateTime"]>;
  ticketSentAt?: Maybe<Scalars["DateTime"]>;
  needsTranslation?: Maybe<Scalars["Boolean"]>;
  additionalNotes?: Maybe<Scalars["String"]>;
  cancelledOn?: Maybe<Scalars["Date"]>;
  checkInTime?: Maybe<Scalars["DateTime"]>;
  ticket?: Maybe<TicketCreateOneInlineInput>;
};

export type TicketOrderCreateManyInlineInput = {
  /** Create and connect multiple existing TicketOrder documents */
  create?: Maybe<Array<TicketOrderCreateInput>>;
  /** Connect multiple existing TicketOrder documents */
  connect?: Maybe<Array<TicketOrderWhereUniqueInput>>;
};

export type TicketOrderCreateOneInlineInput = {
  /** Create and connect one TicketOrder document */
  create?: Maybe<TicketOrderCreateInput>;
  /** Connect one existing TicketOrder document */
  connect?: Maybe<TicketOrderWhereUniqueInput>;
};

/** An edge in a connection. */
export type TicketOrderEdge = {
  __typename?: "TicketOrderEdge";
  /** The item at the end of the edge. */
  node: TicketOrder;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** Identifies documents */
export type TicketOrderManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<TicketOrderWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<TicketOrderWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<TicketOrderWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  name_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  name_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  name_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  name_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  name_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  name_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  name_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  name_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  name_not_ends_with?: Maybe<Scalars["String"]>;
  age?: Maybe<Scalars["Int"]>;
  /** All values that are not equal to given value. */
  age_not?: Maybe<Scalars["Int"]>;
  /** All values that are contained in given list. */
  age_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values that are not contained in given list. */
  age_not_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values less than the given value. */
  age_lt?: Maybe<Scalars["Int"]>;
  /** All values less than or equal the given value. */
  age_lte?: Maybe<Scalars["Int"]>;
  /** All values greater than the given value. */
  age_gt?: Maybe<Scalars["Int"]>;
  /** All values greater than or equal the given value. */
  age_gte?: Maybe<Scalars["Int"]>;
  email?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  email_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  email_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  email_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  email_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  email_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  email_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  email_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  email_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  email_not_ends_with?: Maybe<Scalars["String"]>;
  address?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  address_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  address_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  address_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  address_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  address_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  address_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  address_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  address_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  address_not_ends_with?: Maybe<Scalars["String"]>;
  customPrice?: Maybe<Scalars["Int"]>;
  /** All values that are not equal to given value. */
  customPrice_not?: Maybe<Scalars["Int"]>;
  /** All values that are contained in given list. */
  customPrice_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values that are not contained in given list. */
  customPrice_not_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values less than the given value. */
  customPrice_lt?: Maybe<Scalars["Int"]>;
  /** All values less than or equal the given value. */
  customPrice_lte?: Maybe<Scalars["Int"]>;
  /** All values greater than the given value. */
  customPrice_gt?: Maybe<Scalars["Int"]>;
  /** All values greater than or equal the given value. */
  customPrice_gte?: Maybe<Scalars["Int"]>;
  customPriceReason?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  customPriceReason_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  customPriceReason_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  customPriceReason_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  customPriceReason_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  customPriceReason_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  customPriceReason_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  customPriceReason_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  customPriceReason_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  customPriceReason_not_ends_with?: Maybe<Scalars["String"]>;
  paidAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  paidAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  paidAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  paidAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  paidAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  paidAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  paidAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  paidAt_gte?: Maybe<Scalars["DateTime"]>;
  ticketSentAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  ticketSentAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  ticketSentAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  ticketSentAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  ticketSentAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  ticketSentAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  ticketSentAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  ticketSentAt_gte?: Maybe<Scalars["DateTime"]>;
  needsTranslation?: Maybe<Scalars["Boolean"]>;
  /** All values that are not equal to given value. */
  needsTranslation_not?: Maybe<Scalars["Boolean"]>;
  additionalNotes?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  additionalNotes_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  additionalNotes_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  additionalNotes_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  additionalNotes_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  additionalNotes_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  additionalNotes_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  additionalNotes_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  additionalNotes_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  additionalNotes_not_ends_with?: Maybe<Scalars["String"]>;
  cancelledOn?: Maybe<Scalars["Date"]>;
  /** All values that are not equal to given value. */
  cancelledOn_not?: Maybe<Scalars["Date"]>;
  /** All values that are contained in given list. */
  cancelledOn_in?: Maybe<Array<Scalars["Date"]>>;
  /** All values that are not contained in given list. */
  cancelledOn_not_in?: Maybe<Array<Scalars["Date"]>>;
  /** All values less than the given value. */
  cancelledOn_lt?: Maybe<Scalars["Date"]>;
  /** All values less than or equal the given value. */
  cancelledOn_lte?: Maybe<Scalars["Date"]>;
  /** All values greater than the given value. */
  cancelledOn_gt?: Maybe<Scalars["Date"]>;
  /** All values greater than or equal the given value. */
  cancelledOn_gte?: Maybe<Scalars["Date"]>;
  checkInTime?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  checkInTime_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  checkInTime_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  checkInTime_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  checkInTime_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  checkInTime_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  checkInTime_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  checkInTime_gte?: Maybe<Scalars["DateTime"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  ticket?: Maybe<TicketWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

export enum TicketOrderOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  NameAsc = "name_ASC",
  NameDesc = "name_DESC",
  AgeAsc = "age_ASC",
  AgeDesc = "age_DESC",
  EmailAsc = "email_ASC",
  EmailDesc = "email_DESC",
  AddressAsc = "address_ASC",
  AddressDesc = "address_DESC",
  CustomPriceAsc = "customPrice_ASC",
  CustomPriceDesc = "customPrice_DESC",
  CustomPriceReasonAsc = "customPriceReason_ASC",
  CustomPriceReasonDesc = "customPriceReason_DESC",
  PaidAtAsc = "paidAt_ASC",
  PaidAtDesc = "paidAt_DESC",
  TicketSentAtAsc = "ticketSentAt_ASC",
  TicketSentAtDesc = "ticketSentAt_DESC",
  NeedsTranslationAsc = "needsTranslation_ASC",
  NeedsTranslationDesc = "needsTranslation_DESC",
  AdditionalNotesAsc = "additionalNotes_ASC",
  AdditionalNotesDesc = "additionalNotes_DESC",
  CancelledOnAsc = "cancelledOn_ASC",
  CancelledOnDesc = "cancelledOn_DESC",
  CheckInTimeAsc = "checkInTime_ASC",
  CheckInTimeDesc = "checkInTime_DESC",
}

export type TicketOrderUpdateInput = {
  name?: Maybe<Scalars["String"]>;
  age?: Maybe<Scalars["Int"]>;
  email?: Maybe<Scalars["String"]>;
  address?: Maybe<Scalars["String"]>;
  customPrice?: Maybe<Scalars["Int"]>;
  customPriceReason?: Maybe<Scalars["String"]>;
  paidAt?: Maybe<Scalars["DateTime"]>;
  ticketSentAt?: Maybe<Scalars["DateTime"]>;
  needsTranslation?: Maybe<Scalars["Boolean"]>;
  additionalNotes?: Maybe<Scalars["String"]>;
  cancelledOn?: Maybe<Scalars["Date"]>;
  checkInTime?: Maybe<Scalars["DateTime"]>;
  ticket?: Maybe<TicketUpdateOneInlineInput>;
};

export type TicketOrderUpdateManyInlineInput = {
  /** Create and connect multiple TicketOrder documents */
  create?: Maybe<Array<TicketOrderCreateInput>>;
  /** Connect multiple existing TicketOrder documents */
  connect?: Maybe<Array<TicketOrderConnectInput>>;
  /** Override currently-connected documents with multiple existing TicketOrder documents */
  set?: Maybe<Array<TicketOrderWhereUniqueInput>>;
  /** Update multiple TicketOrder documents */
  update?: Maybe<Array<TicketOrderUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple TicketOrder documents */
  upsert?: Maybe<Array<TicketOrderUpsertWithNestedWhereUniqueInput>>;
  /** Disconnect multiple TicketOrder documents */
  disconnect?: Maybe<Array<TicketOrderWhereUniqueInput>>;
  /** Delete multiple TicketOrder documents */
  delete?: Maybe<Array<TicketOrderWhereUniqueInput>>;
};

export type TicketOrderUpdateManyInput = {
  name?: Maybe<Scalars["String"]>;
  age?: Maybe<Scalars["Int"]>;
  email?: Maybe<Scalars["String"]>;
  address?: Maybe<Scalars["String"]>;
  customPrice?: Maybe<Scalars["Int"]>;
  customPriceReason?: Maybe<Scalars["String"]>;
  paidAt?: Maybe<Scalars["DateTime"]>;
  ticketSentAt?: Maybe<Scalars["DateTime"]>;
  needsTranslation?: Maybe<Scalars["Boolean"]>;
  additionalNotes?: Maybe<Scalars["String"]>;
  cancelledOn?: Maybe<Scalars["Date"]>;
  checkInTime?: Maybe<Scalars["DateTime"]>;
};

export type TicketOrderUpdateManyWithNestedWhereInput = {
  /** Document search */
  where: TicketOrderWhereInput;
  /** Update many input */
  data: TicketOrderUpdateManyInput;
};

export type TicketOrderUpdateOneInlineInput = {
  /** Create and connect one TicketOrder document */
  create?: Maybe<TicketOrderCreateInput>;
  /** Update single TicketOrder document */
  update?: Maybe<TicketOrderUpdateWithNestedWhereUniqueInput>;
  /** Upsert single TicketOrder document */
  upsert?: Maybe<TicketOrderUpsertWithNestedWhereUniqueInput>;
  /** Connect existing TicketOrder document */
  connect?: Maybe<TicketOrderWhereUniqueInput>;
  /** Disconnect currently connected TicketOrder document */
  disconnect?: Maybe<Scalars["Boolean"]>;
  /** Delete currently connected TicketOrder document */
  delete?: Maybe<Scalars["Boolean"]>;
};

export type TicketOrderUpdateWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: TicketOrderWhereUniqueInput;
  /** Document to update */
  data: TicketOrderUpdateInput;
};

export type TicketOrderUpsertInput = {
  /** Create document if it didn't exist */
  create: TicketOrderCreateInput;
  /** Update document if it exists */
  update: TicketOrderUpdateInput;
};

export type TicketOrderUpsertWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: TicketOrderWhereUniqueInput;
  /** Upsert data */
  data: TicketOrderUpsertInput;
};

/** Identifies documents */
export type TicketOrderWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<TicketOrderWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<TicketOrderWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<TicketOrderWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  name_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  name_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  name_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  name_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  name_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  name_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  name_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  name_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  name_not_ends_with?: Maybe<Scalars["String"]>;
  age?: Maybe<Scalars["Int"]>;
  /** All values that are not equal to given value. */
  age_not?: Maybe<Scalars["Int"]>;
  /** All values that are contained in given list. */
  age_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values that are not contained in given list. */
  age_not_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values less than the given value. */
  age_lt?: Maybe<Scalars["Int"]>;
  /** All values less than or equal the given value. */
  age_lte?: Maybe<Scalars["Int"]>;
  /** All values greater than the given value. */
  age_gt?: Maybe<Scalars["Int"]>;
  /** All values greater than or equal the given value. */
  age_gte?: Maybe<Scalars["Int"]>;
  email?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  email_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  email_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  email_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  email_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  email_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  email_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  email_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  email_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  email_not_ends_with?: Maybe<Scalars["String"]>;
  address?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  address_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  address_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  address_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  address_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  address_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  address_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  address_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  address_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  address_not_ends_with?: Maybe<Scalars["String"]>;
  customPrice?: Maybe<Scalars["Int"]>;
  /** All values that are not equal to given value. */
  customPrice_not?: Maybe<Scalars["Int"]>;
  /** All values that are contained in given list. */
  customPrice_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values that are not contained in given list. */
  customPrice_not_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values less than the given value. */
  customPrice_lt?: Maybe<Scalars["Int"]>;
  /** All values less than or equal the given value. */
  customPrice_lte?: Maybe<Scalars["Int"]>;
  /** All values greater than the given value. */
  customPrice_gt?: Maybe<Scalars["Int"]>;
  /** All values greater than or equal the given value. */
  customPrice_gte?: Maybe<Scalars["Int"]>;
  customPriceReason?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  customPriceReason_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  customPriceReason_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  customPriceReason_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  customPriceReason_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  customPriceReason_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  customPriceReason_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  customPriceReason_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  customPriceReason_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  customPriceReason_not_ends_with?: Maybe<Scalars["String"]>;
  paidAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  paidAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  paidAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  paidAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  paidAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  paidAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  paidAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  paidAt_gte?: Maybe<Scalars["DateTime"]>;
  ticketSentAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  ticketSentAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  ticketSentAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  ticketSentAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  ticketSentAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  ticketSentAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  ticketSentAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  ticketSentAt_gte?: Maybe<Scalars["DateTime"]>;
  needsTranslation?: Maybe<Scalars["Boolean"]>;
  /** All values that are not equal to given value. */
  needsTranslation_not?: Maybe<Scalars["Boolean"]>;
  additionalNotes?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  additionalNotes_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  additionalNotes_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  additionalNotes_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  additionalNotes_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  additionalNotes_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  additionalNotes_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  additionalNotes_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  additionalNotes_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  additionalNotes_not_ends_with?: Maybe<Scalars["String"]>;
  cancelledOn?: Maybe<Scalars["Date"]>;
  /** All values that are not equal to given value. */
  cancelledOn_not?: Maybe<Scalars["Date"]>;
  /** All values that are contained in given list. */
  cancelledOn_in?: Maybe<Array<Scalars["Date"]>>;
  /** All values that are not contained in given list. */
  cancelledOn_not_in?: Maybe<Array<Scalars["Date"]>>;
  /** All values less than the given value. */
  cancelledOn_lt?: Maybe<Scalars["Date"]>;
  /** All values less than or equal the given value. */
  cancelledOn_lte?: Maybe<Scalars["Date"]>;
  /** All values greater than the given value. */
  cancelledOn_gt?: Maybe<Scalars["Date"]>;
  /** All values greater than or equal the given value. */
  cancelledOn_gte?: Maybe<Scalars["Date"]>;
  checkInTime?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  checkInTime_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  checkInTime_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  checkInTime_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  checkInTime_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  checkInTime_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  checkInTime_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  checkInTime_gte?: Maybe<Scalars["DateTime"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  ticket?: Maybe<TicketWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

/** References TicketOrder record uniquely */
export type TicketOrderWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

export type TicketUpdateInput = {
  /** name input for default locale (en) */
  name?: Maybe<Scalars["String"]>;
  /** description input for default locale (en) */
  description?: Maybe<Scalars["String"]>;
  price?: Maybe<Scalars["Float"]>;
  available?: Maybe<Scalars["Int"]>;
  ticketOrders?: Maybe<TicketOrderUpdateManyInlineInput>;
  /** Manage document localizations */
  localizations?: Maybe<TicketUpdateLocalizationsInput>;
};

export type TicketUpdateLocalizationDataInput = {
  name?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
};

export type TicketUpdateLocalizationInput = {
  data: TicketUpdateLocalizationDataInput;
  locale: Locale;
};

export type TicketUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: Maybe<Array<TicketCreateLocalizationInput>>;
  /** Localizations to update */
  update?: Maybe<Array<TicketUpdateLocalizationInput>>;
  upsert?: Maybe<Array<TicketUpsertLocalizationInput>>;
  /** Localizations to delete */
  delete?: Maybe<Array<Locale>>;
};

export type TicketUpdateManyInlineInput = {
  /** Create and connect multiple Ticket documents */
  create?: Maybe<Array<TicketCreateInput>>;
  /** Connect multiple existing Ticket documents */
  connect?: Maybe<Array<TicketConnectInput>>;
  /** Override currently-connected documents with multiple existing Ticket documents */
  set?: Maybe<Array<TicketWhereUniqueInput>>;
  /** Update multiple Ticket documents */
  update?: Maybe<Array<TicketUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple Ticket documents */
  upsert?: Maybe<Array<TicketUpsertWithNestedWhereUniqueInput>>;
  /** Disconnect multiple Ticket documents */
  disconnect?: Maybe<Array<TicketWhereUniqueInput>>;
  /** Delete multiple Ticket documents */
  delete?: Maybe<Array<TicketWhereUniqueInput>>;
};

export type TicketUpdateManyInput = {
  /** description input for default locale (en) */
  description?: Maybe<Scalars["String"]>;
  price?: Maybe<Scalars["Float"]>;
  available?: Maybe<Scalars["Int"]>;
  /** Optional updates to localizations */
  localizations?: Maybe<TicketUpdateManyLocalizationsInput>;
};

export type TicketUpdateManyLocalizationDataInput = {
  description?: Maybe<Scalars["String"]>;
};

export type TicketUpdateManyLocalizationInput = {
  data: TicketUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type TicketUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: Maybe<Array<TicketUpdateManyLocalizationInput>>;
};

export type TicketUpdateManyWithNestedWhereInput = {
  /** Document search */
  where: TicketWhereInput;
  /** Update many input */
  data: TicketUpdateManyInput;
};

export type TicketUpdateOneInlineInput = {
  /** Create and connect one Ticket document */
  create?: Maybe<TicketCreateInput>;
  /** Update single Ticket document */
  update?: Maybe<TicketUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Ticket document */
  upsert?: Maybe<TicketUpsertWithNestedWhereUniqueInput>;
  /** Connect existing Ticket document */
  connect?: Maybe<TicketWhereUniqueInput>;
  /** Disconnect currently connected Ticket document */
  disconnect?: Maybe<Scalars["Boolean"]>;
  /** Delete currently connected Ticket document */
  delete?: Maybe<Scalars["Boolean"]>;
};

export type TicketUpdateWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: TicketWhereUniqueInput;
  /** Document to update */
  data: TicketUpdateInput;
};

export type TicketUpsertInput = {
  /** Create document if it didn't exist */
  create: TicketCreateInput;
  /** Update document if it exists */
  update: TicketUpdateInput;
};

export type TicketUpsertLocalizationInput = {
  update: TicketUpdateLocalizationDataInput;
  create: TicketCreateLocalizationDataInput;
  locale: Locale;
};

export type TicketUpsertWithNestedWhereUniqueInput = {
  /** Unique document search */
  where: TicketWhereUniqueInput;
  /** Upsert data */
  data: TicketUpsertInput;
};

/** Identifies documents */
export type TicketWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<TicketWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<TicketWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<TicketWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  name_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  name_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  name_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  name_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  name_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  name_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  name_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  name_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  name_not_ends_with?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  description_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  description_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  description_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  description_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  description_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  description_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  description_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  description_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  description_not_ends_with?: Maybe<Scalars["String"]>;
  price?: Maybe<Scalars["Float"]>;
  /** All values that are not equal to given value. */
  price_not?: Maybe<Scalars["Float"]>;
  /** All values that are contained in given list. */
  price_in?: Maybe<Array<Scalars["Float"]>>;
  /** All values that are not contained in given list. */
  price_not_in?: Maybe<Array<Scalars["Float"]>>;
  /** All values less than the given value. */
  price_lt?: Maybe<Scalars["Float"]>;
  /** All values less than or equal the given value. */
  price_lte?: Maybe<Scalars["Float"]>;
  /** All values greater than the given value. */
  price_gt?: Maybe<Scalars["Float"]>;
  /** All values greater than or equal the given value. */
  price_gte?: Maybe<Scalars["Float"]>;
  available?: Maybe<Scalars["Int"]>;
  /** All values that are not equal to given value. */
  available_not?: Maybe<Scalars["Int"]>;
  /** All values that are contained in given list. */
  available_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values that are not contained in given list. */
  available_not_in?: Maybe<Array<Scalars["Int"]>>;
  /** All values less than the given value. */
  available_lt?: Maybe<Scalars["Int"]>;
  /** All values less than or equal the given value. */
  available_lte?: Maybe<Scalars["Int"]>;
  /** All values greater than the given value. */
  available_gt?: Maybe<Scalars["Int"]>;
  /** All values greater than or equal the given value. */
  available_gte?: Maybe<Scalars["Int"]>;
  createdBy?: Maybe<UserWhereInput>;
  updatedBy?: Maybe<UserWhereInput>;
  publishedBy?: Maybe<UserWhereInput>;
  ticketOrders_every?: Maybe<TicketOrderWhereInput>;
  ticketOrders_some?: Maybe<TicketOrderWhereInput>;
  ticketOrders_none?: Maybe<TicketOrderWhereInput>;
  scheduledIn_every?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: Maybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: Maybe<ScheduledOperationWhereInput>;
};

/** References Ticket record uniquely */
export type TicketWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

export type UnpublishLocaleInput = {
  /** Locales to unpublish */
  locale: Locale;
  /** Stages to unpublish selected locales from */
  stages: Array<Stage>;
};

/** User system model */
export type User = Node & {
  __typename?: "User";
  /** System stage field */
  stage: Stage;
  /** Get the document in other stages */
  documentInStages: Array<User>;
  /** The unique identifier */
  id: Scalars["ID"];
  /** The time the document was created */
  createdAt: Scalars["DateTime"];
  /** The time the document was updated */
  updatedAt: Scalars["DateTime"];
  /** The time the document was published. Null on documents in draft stage. */
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** The username */
  name: Scalars["String"];
  /** Profile Picture url */
  picture?: Maybe<Scalars["String"]>;
  /** Flag to determine if user is active or not */
  isActive: Scalars["Boolean"];
  /** User Kind. Can be either MEMBER, PAT or PUBLIC */
  kind: UserKind;
};

/** User system model */
export type UserDocumentInStagesArgs = {
  stages?: Array<Stage>;
  includeCurrent?: Scalars["Boolean"];
  inheritLocale?: Scalars["Boolean"];
};

export type UserConnectInput = {
  /** Document to connect */
  where: UserWhereUniqueInput;
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: Maybe<ConnectPositionInput>;
};

/** A connection to a list of items. */
export type UserConnection = {
  __typename?: "UserConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges: Array<UserEdge>;
  aggregate: Aggregate;
};

export type UserCreateManyInlineInput = {
  /** Connect multiple existing User documents */
  connect?: Maybe<Array<UserWhereUniqueInput>>;
};

export type UserCreateOneInlineInput = {
  /** Connect one existing User document */
  connect?: Maybe<UserWhereUniqueInput>;
};

/** An edge in a connection. */
export type UserEdge = {
  __typename?: "UserEdge";
  /** The item at the end of the edge. */
  node: User;
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
};

/** System User Kind */
export enum UserKind {
  Member = "MEMBER",
  Pat = "PAT",
  Public = "PUBLIC",
  Webhook = "WEBHOOK",
}

/** Identifies documents */
export type UserManyWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<UserWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<UserWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<UserWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  name_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  name_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  name_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  name_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  name_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  name_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  name_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  name_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  name_not_ends_with?: Maybe<Scalars["String"]>;
  picture?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  picture_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  picture_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  picture_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  picture_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  picture_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  picture_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  picture_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  picture_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  picture_not_ends_with?: Maybe<Scalars["String"]>;
  isActive?: Maybe<Scalars["Boolean"]>;
  /** All values that are not equal to given value. */
  isActive_not?: Maybe<Scalars["Boolean"]>;
  kind?: Maybe<UserKind>;
  /** All values that are not equal to given value. */
  kind_not?: Maybe<UserKind>;
  /** All values that are contained in given list. */
  kind_in?: Maybe<Array<UserKind>>;
  /** All values that are not contained in given list. */
  kind_not_in?: Maybe<Array<UserKind>>;
};

export enum UserOrderByInput {
  IdAsc = "id_ASC",
  IdDesc = "id_DESC",
  CreatedAtAsc = "createdAt_ASC",
  CreatedAtDesc = "createdAt_DESC",
  UpdatedAtAsc = "updatedAt_ASC",
  UpdatedAtDesc = "updatedAt_DESC",
  PublishedAtAsc = "publishedAt_ASC",
  PublishedAtDesc = "publishedAt_DESC",
  NameAsc = "name_ASC",
  NameDesc = "name_DESC",
  PictureAsc = "picture_ASC",
  PictureDesc = "picture_DESC",
  IsActiveAsc = "isActive_ASC",
  IsActiveDesc = "isActive_DESC",
  KindAsc = "kind_ASC",
  KindDesc = "kind_DESC",
}

export type UserUpdateManyInlineInput = {
  /** Connect multiple existing User documents */
  connect?: Maybe<Array<UserConnectInput>>;
  /** Override currently-connected documents with multiple existing User documents */
  set?: Maybe<Array<UserWhereUniqueInput>>;
  /** Disconnect multiple User documents */
  disconnect?: Maybe<Array<UserWhereUniqueInput>>;
};

export type UserUpdateOneInlineInput = {
  /** Connect existing User document */
  connect?: Maybe<UserWhereUniqueInput>;
  /** Disconnect currently connected User document */
  disconnect?: Maybe<Scalars["Boolean"]>;
};

/** Identifies documents */
export type UserWhereInput = {
  /** Contains search across all appropriate fields. */
  _search?: Maybe<Scalars["String"]>;
  /** Logical AND on all given filters. */
  AND?: Maybe<Array<UserWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: Maybe<Array<UserWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: Maybe<Array<UserWhereInput>>;
  id?: Maybe<Scalars["ID"]>;
  /** All values that are not equal to given value. */
  id_not?: Maybe<Scalars["ID"]>;
  /** All values that are contained in given list. */
  id_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values that are not contained in given list. */
  id_not_in?: Maybe<Array<Scalars["ID"]>>;
  /** All values containing the given string. */
  id_contains?: Maybe<Scalars["ID"]>;
  /** All values not containing the given string. */
  id_not_contains?: Maybe<Scalars["ID"]>;
  /** All values starting with the given string. */
  id_starts_with?: Maybe<Scalars["ID"]>;
  /** All values not starting with the given string. */
  id_not_starts_with?: Maybe<Scalars["ID"]>;
  /** All values ending with the given string. */
  id_ends_with?: Maybe<Scalars["ID"]>;
  /** All values not ending with the given string */
  id_not_ends_with?: Maybe<Scalars["ID"]>;
  createdAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  createdAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  createdAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  createdAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  createdAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  createdAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: Maybe<Scalars["DateTime"]>;
  updatedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  updatedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  updatedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  updatedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  updatedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: Maybe<Scalars["DateTime"]>;
  publishedAt?: Maybe<Scalars["DateTime"]>;
  /** All values that are not equal to given value. */
  publishedAt_not?: Maybe<Scalars["DateTime"]>;
  /** All values that are contained in given list. */
  publishedAt_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: Maybe<Array<Scalars["DateTime"]>>;
  /** All values less than the given value. */
  publishedAt_lt?: Maybe<Scalars["DateTime"]>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: Maybe<Scalars["DateTime"]>;
  /** All values greater than the given value. */
  publishedAt_gt?: Maybe<Scalars["DateTime"]>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: Maybe<Scalars["DateTime"]>;
  name?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  name_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  name_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  name_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  name_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  name_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  name_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  name_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  name_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  name_not_ends_with?: Maybe<Scalars["String"]>;
  picture?: Maybe<Scalars["String"]>;
  /** All values that are not equal to given value. */
  picture_not?: Maybe<Scalars["String"]>;
  /** All values that are contained in given list. */
  picture_in?: Maybe<Array<Scalars["String"]>>;
  /** All values that are not contained in given list. */
  picture_not_in?: Maybe<Array<Scalars["String"]>>;
  /** All values containing the given string. */
  picture_contains?: Maybe<Scalars["String"]>;
  /** All values not containing the given string. */
  picture_not_contains?: Maybe<Scalars["String"]>;
  /** All values starting with the given string. */
  picture_starts_with?: Maybe<Scalars["String"]>;
  /** All values not starting with the given string. */
  picture_not_starts_with?: Maybe<Scalars["String"]>;
  /** All values ending with the given string. */
  picture_ends_with?: Maybe<Scalars["String"]>;
  /** All values not ending with the given string */
  picture_not_ends_with?: Maybe<Scalars["String"]>;
  isActive?: Maybe<Scalars["Boolean"]>;
  /** All values that are not equal to given value. */
  isActive_not?: Maybe<Scalars["Boolean"]>;
  kind?: Maybe<UserKind>;
  /** All values that are not equal to given value. */
  kind_not?: Maybe<UserKind>;
  /** All values that are contained in given list. */
  kind_in?: Maybe<Array<UserKind>>;
  /** All values that are not contained in given list. */
  kind_not_in?: Maybe<Array<UserKind>>;
};

/** References User record uniquely */
export type UserWhereUniqueInput = {
  id?: Maybe<Scalars["ID"]>;
};

export type Version = {
  __typename?: "Version";
  id: Scalars["ID"];
  stage: Stage;
  revision: Scalars["Int"];
  createdAt: Scalars["DateTime"];
};

export type VersionWhereInput = {
  id: Scalars["ID"];
  stage: Stage;
  revision: Scalars["Int"];
};

export enum _FilterKind {
  Search = "search",
  And = "AND",
  Or = "OR",
  Not = "NOT",
  Eq = "eq",
  EqNot = "eq_not",
  In = "in",
  NotIn = "not_in",
  Lt = "lt",
  Lte = "lte",
  Gt = "gt",
  Gte = "gte",
  Contains = "contains",
  NotContains = "not_contains",
  StartsWith = "starts_with",
  NotStartsWith = "not_starts_with",
  EndsWith = "ends_with",
  NotEndsWith = "not_ends_with",
  ContainsAll = "contains_all",
  ContainsSome = "contains_some",
  ContainsNone = "contains_none",
  RelationalSingle = "relational_single",
  RelationalEvery = "relational_every",
  RelationalSome = "relational_some",
  RelationalNone = "relational_none",
}

export enum _MutationInputFieldKind {
  Scalar = "scalar",
  RichText = "richText",
  RichTextWithEmbeds = "richTextWithEmbeds",
  Enum = "enum",
  Relation = "relation",
  Union = "union",
  Virtual = "virtual",
}

export enum _MutationKind {
  Create = "create",
  Publish = "publish",
  Unpublish = "unpublish",
  Update = "update",
  Upsert = "upsert",
  Delete = "delete",
  UpdateMany = "updateMany",
  PublishMany = "publishMany",
  UnpublishMany = "unpublishMany",
  DeleteMany = "deleteMany",
  SchedulePublish = "schedulePublish",
  ScheduleUnpublish = "scheduleUnpublish",
}

export enum _OrderDirection {
  Asc = "asc",
  Desc = "desc",
}

export enum _RelationInputCardinality {
  One = "one",
  Many = "many",
}

export enum _RelationInputKind {
  Create = "create",
  Update = "update",
}

export enum _RelationKind {
  Regular = "regular",
  Union = "union",
}

export enum _SystemDateTimeFieldVariation {
  Base = "base",
  Localization = "localization",
  Combined = "combined",
}

export type AddContactMutationVariables = Exact<{
  email: Scalars["String"];
}>;

export type AddContactMutation = { __typename?: "Mutation" } & {
  upsertContact?: Maybe<
    { __typename?: "Contact" } & Pick<Contact, "createdAt">
  >;
};

export type AllSpeakersQueryVariables = Exact<{ [key: string]: never }>;

export type AllSpeakersQuery = { __typename?: "Query" } & {
  speakers: Array<
    { __typename?: "Speaker" } & Pick<Speaker, "name" | "subtitle"> & {
        description: Speaker["descriptionMarkdown"];
      } & { image?: Maybe<{ __typename?: "Asset" } & Pick<Asset, "url">> }
  >;
  extraPrograms: Array<
    { __typename?: "ExtraProgram" } & Pick<
      ExtraProgram,
      "name" | "subtitle"
    > & { image?: Maybe<{ __typename?: "Asset" } & Pick<Asset, "url">> }
  >;
};

export type AllSponsorsQueryVariables = Exact<{ [key: string]: never }>;

export type AllSponsorsQuery = { __typename?: "Query" } & {
  sponsors: Array<
    { __typename?: "Sponsor" } & Pick<Sponsor, "id" | "name" | "url"> & {
        logo?: Maybe<
          { __typename?: "Asset" } & Pick<Asset, "url" | "height" | "width">
        >;
      }
  >;
};

export type AllTeamsQueryVariables = Exact<{ [key: string]: never }>;

export type AllTeamsQuery = { __typename?: "Query" } & {
  teams: Array<
    { __typename?: "Team" } & Pick<Team, "id" | "name" | "description"> & {
        teamMembers: Array<
          { __typename?: "TeamMember" } & Pick<
            TeamMember,
            "id" | "name" | "role"
          > & { image?: Maybe<{ __typename?: "Asset" } & Pick<Asset, "url">> }
        >;
      }
  >;
};

export type AllTestimonialsQueryVariables = Exact<{ [key: string]: never }>;

export type AllTestimonialsQuery = { __typename?: "Query" } & {
  testimonials: Array<
    { __typename?: "Testimonial" } & Pick<
      Testimonial,
      "id" | "from" | "said"
    > & { image?: Maybe<{ __typename?: "Asset" } & Pick<Asset, "url">> }
  >;
};

export type AllTicketsQueryVariables = Exact<{ [key: string]: never }>;

export type AllTicketsQuery = { __typename?: "Query" } & {
  fullTickets: Array<{ __typename?: "Ticket" } & TicketFieldsFragment>;
  halfTickets: Array<{ __typename?: "Ticket" } & TicketFieldsFragment>;
};

export type TicketFieldsFragment = { __typename?: "Ticket" } & Pick<
  Ticket,
  "id" | "name" | "available" | "price" | "description"
>;

export type CovidConceptQueryVariables = Exact<{ [key: string]: never }>;

export type CovidConceptQuery = { __typename?: "Query" } & {
  covidConcepts: Array<
    { __typename?: "CovidConcept" } & Pick<
      CovidConcept,
      "id" | "header" | "description"
    >
  >;
};

export type DecrementTicketMutationVariables = Exact<{
  id: Scalars["ID"];
  updatedAvailable: Scalars["Int"];
}>;

export type DecrementTicketMutation = { __typename?: "Mutation" } & {
  updateTicket?: Maybe<{ __typename?: "Ticket" } & Pick<Ticket, "available">>;
  publishTicket?: Maybe<{ __typename?: "Ticket" } & Pick<Ticket, "id">>;
};

export type OrderTicketMutationVariables = Exact<{
  name: Scalars["String"];
  age: Scalars["Int"];
  email: Scalars["String"];
  address: Scalars["String"];
  ticket: Scalars["ID"];
  englishTranslation: Scalars["Boolean"];
}>;

export type OrderTicketMutation = { __typename?: "Mutation" } & {
  createTicketOrder?: Maybe<
    { __typename?: "TicketOrder" } & {
      ticket?: Maybe<
        { __typename?: "Ticket" } & Pick<Ticket, "name" | "price">
      >;
    }
  >;
};

export type ShortInfoTextsQueryVariables = Exact<{ [key: string]: never }>;

export type ShortInfoTextsQuery = { __typename?: "Query" } & {
  shortInfoTexts: Array<
    { __typename?: "ShortInfoText" } & Pick<
      ShortInfoText,
      "id" | "header" | "content"
    > & { image?: Maybe<{ __typename?: "Asset" } & Pick<Asset, "url">> }
  >;
};

export type TicketQueryVariables = Exact<{
  id: Scalars["ID"];
}>;

export type TicketQuery = { __typename?: "Query" } & {
  ticket?: Maybe<{ __typename?: "Ticket" } & Pick<Ticket, "available">>;
};

export type UnassignedJobsQueryVariables = Exact<{ [key: string]: never }>;

export type UnassignedJobsQuery = { __typename?: "Query" } & {
  jobs: Array<
    { __typename?: "Job" } & Pick<
      Job,
      | "title"
      | "discount"
      | "description"
      | "color"
      | "neededPeople"
      | "commitedWorkers"
    >
  >;
  jobFaqs: Array<
    { __typename?: "JobFaq" } & Pick<JobFaq, "question" | "answer">
  >;
};

export const TicketFieldsFragmentDoc = gql`
  fragment ticketFields on Ticket {
    id
    name
    available
    price
    description
  }
`;
export const AddContactDocument = gql`
  mutation addContact($email: String!) {
    upsertContact(
      where: { email: $email }
      upsert: { create: { email: $email }, update: { email: $email } }
    ) {
      createdAt
    }
  }
`;
export const AllSpeakersDocument = gql`
  query allSpeakers {
    speakers(orderBy: position_ASC) {
      name
      subtitle
      description: descriptionMarkdown
      image {
        url
      }
    }
    extraPrograms {
      name
      subtitle
      image {
        url
      }
    }
  }
`;
export const AllSponsorsDocument = gql`
  query allSponsors {
    sponsors {
      id
      name
      url
      logo {
        url(transformation: { image: { resize: { height: 150 } } })
        height
        width
      }
    }
  }
`;
export const AllTeamsDocument = gql`
  query allTeams {
    teams {
      id
      name
      description
      teamMembers {
        id
        name
        role
        image {
          url
        }
      }
    }
  }
`;
export const AllTestimonialsDocument = gql`
  query allTestimonials {
    testimonials {
      id
      from
      said
      image {
        url
      }
    }
  }
`;
export const AllTicketsDocument = gql`
  query allTickets {
    fullTickets: tickets(
      where: {
        name_not_in: [
          "First Half"
          "Second Half"
          "Erste Hälfte"
          "Zweite Hälfte"
        ]
      }
    ) {
      ...ticketFields
    }
    halfTickets: tickets(
      where: {
        name_in: ["First Half", "Second Half", "Erste Hälfte", "Zweite Hälfte"]
      }
    ) {
      ...ticketFields
    }
  }
  ${TicketFieldsFragmentDoc}
`;
export const CovidConceptDocument = gql`
  query covidConcept {
    covidConcepts {
      id
      header
      description
    }
  }
`;
export const DecrementTicketDocument = gql`
  mutation decrementTicket($id: ID!, $updatedAvailable: Int!) {
    updateTicket(where: { id: $id }, data: { available: $updatedAvailable }) {
      available
    }
    publishTicket(to: [PUBLISHED], where: { id: $id }) {
      id
    }
  }
`;
export const OrderTicketDocument = gql`
  mutation orderTicket(
    $name: String!
    $age: Int!
    $email: String!
    $address: String!
    $ticket: ID!
    $englishTranslation: Boolean!
  ) {
    createTicketOrder(
      data: {
        ticket: { connect: { id: $ticket } }
        name: $name
        age: $age
        email: $email
        address: $address
        needsTranslation: $englishTranslation
      }
    ) {
      ticket {
        name
        price
      }
    }
  }
`;
export const ShortInfoTextsDocument = gql`
  query shortInfoTexts {
    shortInfoTexts {
      id
      header
      content
      image {
        url
      }
    }
  }
`;
export const TicketDocument = gql`
  query ticket($id: ID!) {
    ticket(where: { id: $id }) {
      available
    }
  }
`;
export const UnassignedJobsDocument = gql`
  query unassignedJobs {
    jobs(orderBy: position_ASC) {
      title
      discount
      description
      color
      neededPeople
      commitedWorkers
    }
    jobFaqs {
      question
      answer
    }
  }
`;

export type SdkFunctionWrapper = <T>(action: () => Promise<T>) => Promise<T>;

const defaultWrapper: SdkFunctionWrapper = (sdkFunction) => sdkFunction();

export function getSdk(
  client: GraphQLClient,
  withWrapper: SdkFunctionWrapper = defaultWrapper
) {
  return {
    addContact(
      variables: AddContactMutationVariables,
      requestHeaders?: Dom.RequestInit["headers"]
    ): Promise<AddContactMutation> {
      return withWrapper(() =>
        client.request<AddContactMutation>(
          AddContactDocument,
          variables,
          requestHeaders
        )
      );
    },
    allSpeakers(
      variables?: AllSpeakersQueryVariables,
      requestHeaders?: Dom.RequestInit["headers"]
    ): Promise<AllSpeakersQuery> {
      return withWrapper(() =>
        client.request<AllSpeakersQuery>(
          AllSpeakersDocument,
          variables,
          requestHeaders
        )
      );
    },
    allSponsors(
      variables?: AllSponsorsQueryVariables,
      requestHeaders?: Dom.RequestInit["headers"]
    ): Promise<AllSponsorsQuery> {
      return withWrapper(() =>
        client.request<AllSponsorsQuery>(
          AllSponsorsDocument,
          variables,
          requestHeaders
        )
      );
    },
    allTeams(
      variables?: AllTeamsQueryVariables,
      requestHeaders?: Dom.RequestInit["headers"]
    ): Promise<AllTeamsQuery> {
      return withWrapper(() =>
        client.request<AllTeamsQuery>(
          AllTeamsDocument,
          variables,
          requestHeaders
        )
      );
    },
    allTestimonials(
      variables?: AllTestimonialsQueryVariables,
      requestHeaders?: Dom.RequestInit["headers"]
    ): Promise<AllTestimonialsQuery> {
      return withWrapper(() =>
        client.request<AllTestimonialsQuery>(
          AllTestimonialsDocument,
          variables,
          requestHeaders
        )
      );
    },
    allTickets(
      variables?: AllTicketsQueryVariables,
      requestHeaders?: Dom.RequestInit["headers"]
    ): Promise<AllTicketsQuery> {
      return withWrapper(() =>
        client.request<AllTicketsQuery>(
          AllTicketsDocument,
          variables,
          requestHeaders
        )
      );
    },
    covidConcept(
      variables?: CovidConceptQueryVariables,
      requestHeaders?: Dom.RequestInit["headers"]
    ): Promise<CovidConceptQuery> {
      return withWrapper(() =>
        client.request<CovidConceptQuery>(
          CovidConceptDocument,
          variables,
          requestHeaders
        )
      );
    },
    decrementTicket(
      variables: DecrementTicketMutationVariables,
      requestHeaders?: Dom.RequestInit["headers"]
    ): Promise<DecrementTicketMutation> {
      return withWrapper(() =>
        client.request<DecrementTicketMutation>(
          DecrementTicketDocument,
          variables,
          requestHeaders
        )
      );
    },
    orderTicket(
      variables: OrderTicketMutationVariables,
      requestHeaders?: Dom.RequestInit["headers"]
    ): Promise<OrderTicketMutation> {
      return withWrapper(() =>
        client.request<OrderTicketMutation>(
          OrderTicketDocument,
          variables,
          requestHeaders
        )
      );
    },
    shortInfoTexts(
      variables?: ShortInfoTextsQueryVariables,
      requestHeaders?: Dom.RequestInit["headers"]
    ): Promise<ShortInfoTextsQuery> {
      return withWrapper(() =>
        client.request<ShortInfoTextsQuery>(
          ShortInfoTextsDocument,
          variables,
          requestHeaders
        )
      );
    },
    ticket(
      variables: TicketQueryVariables,
      requestHeaders?: Dom.RequestInit["headers"]
    ): Promise<TicketQuery> {
      return withWrapper(() =>
        client.request<TicketQuery>(TicketDocument, variables, requestHeaders)
      );
    },
    unassignedJobs(
      variables?: UnassignedJobsQueryVariables,
      requestHeaders?: Dom.RequestInit["headers"]
    ): Promise<UnassignedJobsQuery> {
      return withWrapper(() =>
        client.request<UnassignedJobsQuery>(
          UnassignedJobsDocument,
          variables,
          requestHeaders
        )
      );
    },
  };
}
export type Sdk = ReturnType<typeof getSdk>;
