import { OrderDetails } from "../pages/api/buyTypes";
import { useField as formikUseField } from "formik";
import {
  FieldHelperProps,
  FieldInputProps,
  FieldMetaProps,
} from "formik/dist/types";

export type FieldNameType = keyof OrderDetails;
type FieldReturnType<T> = [
  FieldInputProps<T>,
  FieldMetaProps<T>,
  FieldHelperProps<T>
];

export function useField<T = unknown>(name: FieldNameType): FieldReturnType<T> {
  return formikUseField(name);
}
