1. Open the Web IDE
   ![open Web IDE.png](./.gitlab/graphics/open Web IDE.png)
2. Choose the file to edit
   ![choose file.png](./.gitlab/graphics/choose file.png)
3. Commit your changes
   ![stash and start commit.png](./.gitlab/graphics/stash and start commit.png)
4. Make sure the settings are correct
   ![commit to branch.png](./.gitlab/graphics/commit to branch.png)
5. Create a new Merge Request
   ![create MR.png](./.gitlab/graphics/create MR.png)
6. Success!
