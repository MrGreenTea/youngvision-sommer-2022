// eslint-disable-next-line @typescript-eslint/no-var-requires
const colors = require("tailwindcss/colors");

module.exports = {
  purge: ["**/*.tsx"],
  content: ["**/*.tsx"],
  theme: {
    extend: {
      colors: {
        rose: colors.rose,
        cyan: colors.cyan,
        teal: colors.teal,
        lime: colors.lime,
      },
      fontFamily: {
        moontype: ["Moontype"],
        inter: ["Inter var"],
      },
    },
  },
  variants: {
    extend: {
      cursor: ["disabled"],
      boxShadow: ["disabled"],
      ringWidth: ["disabled"],
      scale: ["active"],
      backgroundColor: ["active", "disabled"],
      textColor: ["disabled"],
      opacity: ["disabled"],
      pointerEvents: ["disabled"],
      transitionProperty: ["motion-safe", "motion-reduce"],
    },
  },
  plugins: [
    require("@tailwindcss/forms"),
    require("@tailwindcss/aspect-ratio"),
    require("@tailwindcss/typography"),
  ],
};
