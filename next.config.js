const config = {
  i18n: {
    defaultLocale: "de",
    locales: ["de", "en"],
  },
  images: {
    domains: ["media.graphassets.com"],
    deviceSizes: [64, 80, 128, 256, 384, 640, 784, 1024, 1280, 1536, 1872],
  },
};

module.exports = config;
