import React from "react";
import DurationSelect from "./DurationSelect";
import { ErrorMessage } from "formik";
import { useField } from "../lib/forms";
import { useI18n } from "../i18n";

const FIELD_NAME = "commitment";

const SignCommitment: React.FC = () => {
  const [field, meta] = useField<string>(FIELD_NAME);
  const { t } = useI18n();

  const localization = t("signUp").commitment;

  const isError = meta.touched && meta.error;
  const inputClasses = isError
    ? "focus:ring-red-500 focus:border-red-500 border-red-300 text-red-900 placeholder-red-300"
    : "focus:ring-teal-500 focus:border-teal-500 border-gray-300 text-teal-600";

  return (
    <div className="my-4 relative flex items-start">
      <div className="flex items-center h-5">
        <input
          {...field}
          type="checkbox"
          className={`h-4 w-4 rounded ${inputClasses}`}
        />
        {isError && (
          <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
            <svg
              className="h-5 w-5 text-red-500"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              aria-hidden="true"
            >
              <path
                fillRule="evenodd"
                d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z"
                clipRule="evenodd"
              />
            </svg>
          </div>
        )}
      </div>
      <div className="ml-3 text-sm">
        <label htmlFor={FIELD_NAME} className="sr-only">
          {localization.signing.title}
        </label>
        <p id={`${FIELD_NAME}-description`} className="text-gray-500">
          {localization.signing.description}
        </p>
        <p className="mt-2 h-4 text-sm text-red-600" id={`${FIELD_NAME}-error`}>
          <ErrorMessage name={FIELD_NAME} />
        </p>
      </div>
    </div>
  );
};

const SummerCommitment: React.FC = () => {
  const { t } = useI18n();
  const localization = t("signUp").commitment;

  return (
    <div>
      <h2 className="text-lg leading-6 font-medium text-gray-900">
        {localization.info.title}
      </h2>
      <div className="prose mt-2 text-gray-600 text-md">
        <p className="text-justify">{localization.info.description}</p>
        <h3 className="mt-2 font-medium">{localization.contract.title}:</h3>
        <p className="text-justify">
          {localization.contract.description}
          <br />
          <span className="text-indigo-500 mt-2">
            {localization.contract.hashtags}
          </span>
        </p>
      </div>
      <SignCommitment />
      <DurationSelect />
    </div>
  );
};

export default SummerCommitment;
