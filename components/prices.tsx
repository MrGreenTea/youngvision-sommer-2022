import React from "react";
import { AllTicketsQuery, TicketFieldsFragment } from "../lib/youngvision-sdk";
import { useI18n } from "../i18n";
import { classNames } from "../lib/tailwindUtilities";

type TicketType = TicketFieldsFragment;

type PricesProps = {
  onBuy: (ticketName: TicketType) => void;
};

const BuyButton: React.FC<{ onClick: () => void; disabled: boolean }> = ({
  onClick,
  disabled,
}) => {
  const { t } = useI18n();
  return (
    <div className="mt-8">
      <div className="rounded-lg">
        <button
          onClick={onClick}
          disabled={disabled}
          className="disabled:cursor-not-allowed disabled:text-gray-400 disabled:shadow shadow-md w-full text-center rounded-lg border border-transparent bg-white px-6 py-3 text-base font-medium text-teal-600 hover:bg-gray-50"
        >
          {t("buy")}
        </button>
      </div>
    </div>
  );
};

const StandardTicket: React.FC<PricesProps & { ticket: TicketType }> = ({
  onBuy,
  ticket,
}) => {
  const { t } = useI18n();
  const { name, price, description, available } = ticket;
  const noneLeft = available != null && available <= 0;
  const ticketsLeft = t("ticketsLeft");
  const ticketsLeftMessage = noneLeft
    ? t("noTicketsLeft")
    : `(${available} ${ticketsLeft})`;
  return (
    <div
      className={classNames(
        noneLeft ? "cursor-not-allowed opacity-95" : "",
        "mt-10 mx-auto max-w-md lg:m-0 lg:max-w-none lg:row-start-2 lg:row-end-3"
      )}
    >
      <div
        className={classNames(
          noneLeft ? "shadow" : "shadow-lg",
          "h-full flex flex-col rounded-lg overflow-hidden"
        )}
      >
        <div className="flex-1 flex flex-col">
          <div className="bg-white px-6 py-10">
            <div>
              <h3
                className={classNames(
                  noneLeft ? "text-gray-500" : "text-gray-900",
                  "text-center text-2xl font-medium"
                )}
                id="tier-scale"
              >
                {name}{" "}
                <span
                  className={classNames(
                    noneLeft ? "text-gray-400" : "text-gray-500",
                    "text-lg"
                  )}
                >
                  {available != null ? ticketsLeftMessage : ""}
                </span>
              </h3>
              <div className="mt-4 flex items-center justify-center">
                <span
                  className={classNames(
                    noneLeft ? "text-gray-500" : "text-gray-900",
                    "px-3 flex items-start text-6xl tracking-tight"
                  )}
                >
                  <span className="mt-2 mr-2 text-4xl font-medium">€</span>
                  <span className="font-extrabold">{price}</span>
                </span>
              </div>
            </div>
          </div>
          <div className="flex-1 flex flex-col justify-between border-t-2 border-gray-100 p-6 bg-gray-50 sm:p-10 lg:p-6 xl:p-10">
            <ul className="space-y-4">
              {description?.split("\n").map((l) => (
                <li key={l} className="flex items-start">
                  <div className="flex-shrink-0">
                    {/* Heroicon name: outline/check */}
                    <svg
                      className={classNames(
                        noneLeft ? "text-gray-300" : "text-green-500",
                        "flex-shrink-0 h-6 w-6"
                      )}
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      aria-hidden="true"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        d="M5 13l4 4L19 7"
                      />
                    </svg>
                  </div>
                  <p
                    className={classNames(
                      noneLeft ? "text-gray-300" : "text-gray-500",
                      "ml-3 text-base font-medium"
                    )}
                  >
                    {l}
                  </p>
                </li>
              ))}
            </ul>
            <BuyButton disabled={noneLeft} onClick={() => onBuy(ticket)} />
          </div>
        </div>
      </div>
    </div>
  );
};

const HalfTickets: React.FC<
  PricesProps & Pick<AllTicketsQuery, "halfTickets">
> = ({ halfTickets, onBuy }) => {
  const { t } = useI18n();

  const [first, second] = halfTickets;
  const noneLeft = false;

  return (
    <div
      className={classNames(
        noneLeft ? "cursor-not-allowed opacity-95" : "",
        "mt-10 mx-auto max-w-md lg:m-0 lg:max-w-none lg:row-start-2 lg:row-end-3"
      )}
    >
      <div
        className={classNames(
          noneLeft ? "shadow" : "shadow-lg",
          "h-full flex flex-col rounded-lg overflow-hidden"
        )}
      >
        <div className="flex-1 flex flex-col">
          <div className="bg-white px-6 py-10">
            <div>
              <h3
                className={classNames(
                  noneLeft ? "text-gray-500" : "text-gray-900",
                  "text-center text-2xl font-medium"
                )}
                id="tier-scale"
              >
                {t("halfTicket")}
              </h3>
              <div className="mt-4 flex items-center justify-center">
                <span
                  className={classNames(
                    noneLeft ? "text-gray-500" : "text-gray-900",
                    "px-3 flex items-start text-6xl tracking-tight"
                  )}
                >
                  <span className="mt-2 mr-2 text-4xl font-medium">€</span>
                  <span className="font-extrabold">{first.price}</span>
                </span>
              </div>
            </div>
          </div>
          <div className="flex-1 flex flex-col justify-between border-t-2 border-gray-100 p-6 bg-gray-50 sm:p-10 lg:p-6 xl:p-10">
            <ul className="space-y-4">
              {first.description?.split("\n").map((l) => (
                <li key={l} className="flex items-start">
                  <div className="flex-shrink-0">
                    {/* Heroicon name: outline/check */}
                    <svg
                      className={classNames(
                        noneLeft ? "text-gray-300" : "text-green-500",
                        "flex-shrink-0 h-6 w-6"
                      )}
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      aria-hidden="true"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        d="M5 13l4 4L19 7"
                      />
                    </svg>
                  </div>
                  <p
                    className={classNames(
                      noneLeft ? "text-gray-300" : "text-gray-500",
                      "ml-3 text-base font-medium"
                    )}
                  >
                    {l}
                  </p>
                </li>
              ))}
            </ul>
            <div className="mt-8">
              <div className="rounded-lg flex space-x-3">
                <button
                  onClick={() => onBuy(first)}
                  disabled={noneLeft}
                  className="disabled:cursor-not-allowed disabled:text-gray-400 disabled:shadow shadow-md w-full text-center rounded-lg rounded-r-none border border-transparent bg-white px-6 py-3 text-base font-medium text-teal-600 hover:bg-gray-50"
                >
                  13. - 18.
                </button>
                <button
                  onClick={() => onBuy(second)}
                  disabled={noneLeft}
                  className="disabled:cursor-not-allowed disabled:text-gray-400 disabled:shadow shadow-md w-full text-center rounded-lg rounded-l-none border border-transparent bg-white px-6 py-3 text-base font-medium text-teal-600 hover:bg-gray-50"
                >
                  18. - 22.
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const Prices: React.FC<PricesProps & Omit<AllTicketsQuery, "__typename">> = ({
  onBuy,
  fullTickets,
  halfTickets,
}) => {
  const { t } = useI18n();
  return (
    <div className="bg-cyan-900">
      <div className="pt-12 px-4 sm:px-6 lg:px-8 lg:pt-20">
        <div className="text-center">
          <h2 className="text-lg leading-6 font-semibold text-gray-300 uppercase tracking-wider">
            Tickets
          </h2>
          <p className="mt- text-3xl font-extrabold text-white sm:text-4xl lg:text-5xl">
            {t("perfectTicket")}
          </p>
        </div>
      </div>
      <div className="mt-16 bg-white pb-12 lg:mt-20 lg:pb-20">
        <div className="relative z-0">
          <div className="absolute inset-0 h-5/6 bg-cyan-900 lg:h-2/3" />
          <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <div className="relative lg:grid lg:grid-cols-3 gap-x-20">
              {fullTickets.map((t) => (
                <StandardTicket key={t.id} onBuy={onBuy} ticket={t} />
              ))}
              <HalfTickets halfTickets={halfTickets} onBuy={onBuy} />
            </div>
          </div>
        </div>
      </div>
      <div className="mt-16 bg-white pb-12 lg:mt-20 lg:pb-20 text-center text-gray-800">
        Du kannst dein Ticket bis zu eine Woche vor dem Festivalstart
        stornieren.
      </div>
    </div>
  );
};

export default Prices;
