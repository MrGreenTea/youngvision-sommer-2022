import React from "react";
import { RadioGroup } from "@headlessui/react";
import { CheckCircleIcon } from "@heroicons/react/solid";
import { ErrorMessage } from "formik";
import { useField } from "../lib/forms";
import { Locale, useI18n } from "../i18n";
import { classNames } from "../lib/tailwindUtilities";

const FIELD_NAME = "duration";

type DurationChoiceType = keyof Locale["signUp"]["durations"];

// we use german values in Coda, so we need to make sure the correct value is sent by the form
const ChoiceMap: {
  [P in DurationChoiceType]: string;
} = {
  fullTime: "Volle Zeit",
  firstHalf: "1. Hälfte",
  secondHalf: "2. Hälfte",
};

const DurationChoice: React.FC<{
  duration: DurationChoiceType;
}> = ({ duration }) => {
  const { t } = useI18n();

  const durationLocalization = t("signUp").durations[duration];

  return (
    <RadioGroup.Option
      key={duration}
      value={ChoiceMap[duration]}
      className={({ checked, active }) =>
        classNames(
          checked ? "border-transparent" : "border-gray-300",
          active ? "border-indigo-500 ring-2 ring-indigo-500" : "",
          "relative bg-white border rounded-lg shadow-sm p-4 flex cursor-pointer focus:outline-none"
        )
      }
    >
      {({ checked, active }) => (
        <>
          <span className="flex-1 flex">
            <span className="flex flex-col">
              <RadioGroup.Label
                as="span"
                className="block text-sm font-medium text-gray-900"
              >
                {durationLocalization.title}
              </RadioGroup.Label>
              <RadioGroup.Description
                as="span"
                className="mt-1 flex items-center text-sm text-gray-500"
              >
                {durationLocalization.description}
              </RadioGroup.Description>
            </span>
          </span>
          <CheckCircleIcon
            className={classNames(
              !checked ? "invisible" : "",
              "h-5 w-5 text-indigo-600"
            )}
            aria-hidden="true"
          />
          <span
            className={classNames(
              active ? "border" : "border-2",
              checked ? "border-indigo-500" : "border-transparent",
              "absolute -inset-px rounded-lg pointer-events-none"
            )}
            aria-hidden="true"
          />
        </>
      )}
    </RadioGroup.Option>
  );
};

const DurationSelect: React.FC = () => {
  const [field, , helpers] = useField(FIELD_NAME);

  return (
    <RadioGroup {...field} value={field.value} onChange={helpers.setValue}>
      <RadioGroup.Label className="text-lg leading-6 font-medium text-gray-900">
        Ich bin dabei
      </RadioGroup.Label>

      <div className="mt-4 grid grid-cols-1 gap-y-6 sm:grid-cols-3 sm:gap-x-4">
        <DurationChoice duration="fullTime" />
        <DurationChoice duration="firstHalf" />
        <DurationChoice duration="secondHalf" />
      </div>
      <p className="mt-2 h-4 text-sm text-red-600" id={`${FIELD_NAME}-error`}>
        <ErrorMessage name={FIELD_NAME} />
      </p>
    </RadioGroup>
  );
};
export default DurationSelect;
