import React from "react";
import { Carousel, PaginationProps } from "./carousel";
import { AllTestimonialsQuery } from "../lib/youngvision-sdk";
import { ValuesType } from "utility-types";
import Image from "next/image";
import { resortToPlaceholderPortait } from "../lib/resources";

type TestimonialsType = AllTestimonialsQuery["testimonials"];
type TestimonialType = ValuesType<TestimonialsType>;

const Testimonial: React.FC<{ testimonial: TestimonialType }> = ({
  testimonial,
}) => {
  return (
    <div className="pt-16 lg:py-24">
      <div className="pb-16 lg:pb-0 lg:z-10 lg:relative">
        <div className="rounded-2xl bg-gradient-to-r from-cyan-500 lg:from-teal-500 to-cyan-600 lg:mx-auto lg:max-w-7xl lg:px-8 lg:grid lg:grid-cols-3 lg:gap-8">
          <div className="relative lg:-my-8">
            <div
              aria-hidden="true"
              className="absolute inset-x-0 top-0 h-1/2 bg-white lg:hidden"
            />
            <div className="mx-auto max-w-md px-4 sm:max-w-3xl sm:px-6 lg:p-0 lg:h-full">
              <div className="aspect-w-10 aspect-h-6 rounded-xl shadow-xl overflow-hidden sm:aspect-w-16 sm:aspect-h-7 lg:aspect-none lg:h-full">
                <Image
                  className="rounded-xl lg:h-full lg:w-full"
                  layout="fill"
                  objectFit="cover"
                  src={resortToPlaceholderPortait(testimonial.image?.url)}
                  alt={testimonial.from}
                />
              </div>
            </div>
          </div>
          <div className="mt-12 lg:m-0 lg:col-span-2 lg:pl-8">
            <div className="mx-auto max-w-md px-4 sm:max-w-2xl sm:px-6 lg:px-0 lg:py-20 lg:max-w-none">
              <blockquote>
                <div>
                  <svg
                    className="h-12 w-12 text-white opacity-25"
                    fill="currentColor"
                    viewBox="0 0 32 32"
                    aria-hidden="true"
                  >
                    <path d="M9.352 4C4.456 7.456 1 13.12 1 19.36c0 5.088 3.072 8.064 6.624 8.064 3.36 0 5.856-2.688 5.856-5.856 0-3.168-2.208-5.472-5.088-5.472-.576 0-1.344.096-1.536.192.48-3.264 3.552-7.104 6.624-9.024L9.352 4zm16.512 0c-4.8 3.456-8.256 9.12-8.256 15.36 0 5.088 3.072 8.064 6.624 8.064 3.264 0 5.856-2.688 5.856-5.856 0-3.168-2.304-5.472-5.184-5.472-.576 0-1.248.096-1.44.192.48-3.264 3.456-7.104 6.528-9.024L25.864 4z" />
                  </svg>
                  <p className="mt-6 text-2xl font-medium font-inter text-white">
                    {testimonial.said}
                  </p>
                </div>
                <footer className="mt-6 flex justify-end">
                  <p className="text-base font-medium text-white">
                    {testimonial.from}
                  </p>
                </footer>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const Pagination: (props: PaginationProps) => JSX.Element = ({
  pages,
  activePage,
  onClick,
}) => {
  return (
    <div className="hidden w-full px-4 py-3 flex items-center justify-center sm:px-6 sm:flex-1 sm:flex sm:items-center sm:justify-center">
      <nav aria-label="Pagination">
        {pages.map((p) => (
          <button
            key={p}
            aria-label={`Switch to ${p}`}
            onClick={() => onClick(p)}
            className={`transition-colors relative mx-5 px-4 py-2 text-sm font-medium hover:bg-gray-700 active:bg-gray-800 active:shadow w-8 h-8 rounded-full ${
              activePage === p
                ? "bg-gray-800 shadow"
                : "bg-gray-500 opacity-80 shadow-inner"
            }`}
          />
        ))}
      </nav>
    </div>
  );
};

export const Testimonials: React.FC<{ testimonials: TestimonialsType }> = ({
  testimonials,
}) => {
  if (testimonials.length === 0) {
    return null;
  }
  return (
    <section className="my-2.5">
      <div className="w-full lg:max-w-8xl mx-auto pt-20 px-4 sm:px-6 lg:px-8 lg:py-20">
        <Carousel Pagination={Pagination}>
          {testimonials.map((t) => (
            <Testimonial key={t.id} testimonial={t} />
          ))}
        </Carousel>
      </div>
    </section>
  );
};

export default Testimonials;
