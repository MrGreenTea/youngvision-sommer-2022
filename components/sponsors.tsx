import React from "react";
import Image from "next/image";

const Sponsors: React.FC = () => {
  return (
    <section className="bg-white w-full mt-2 lg:mt-12">
      <div className="flex flex-row overflow-x-auto">
        <Image
          width={2284}
          height={335}
          alt="Logos des Auf!Leben Programms"
          src="/images/BMFSFJ_Corona_Aufholpaket.png"
        />
      </div>
    </section>
  );
};

export default Sponsors;
