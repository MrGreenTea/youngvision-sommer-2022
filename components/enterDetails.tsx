import React, { Fragment, useRef, useState } from "react";
import { OrderDetails, schema } from "../pages/api/buyTypes";
import { useI18n } from "../i18n";
import {
  ErrorMessage,
  Field,
  FieldProps,
  Form,
  Formik,
  setNestedObjectValues,
} from "formik";
import { classNames } from "../lib/tailwindUtilities";
import axios from "axios";
import { toast } from "react-hot-toast";
import { Dialog, Transition } from "@headlessui/react";
import { CheckIcon } from "@heroicons/react/outline";
import SummerCommitment from "./SummerCommitment";
import Motivation from "./Motivation";
import HealthInfo from "./HealthInfo";
import { parseDateString } from "../lib/dates";

// TODO localization of Haftungsauschluss

type InputProps = React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
>;

function ConfirmPayment({
  open,
  setOpen,
  submit,
  setSubmitting,
}: {
  open: boolean;
  setOpen: (arg0: boolean) => void;
  setSubmitting: (arg0: boolean) => void;
  submit: () => void;
}) {
  const { t } = useI18n();
  const cancelButtonRef = useRef(null);

  const confirmTexts = t("signUp").confirmation;

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        static
        className="fixed z-10 inset-0 overflow-y-auto"
        initialFocus={cancelButtonRef}
        open={open}
        onClose={setOpen}
      >
        <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span
            className="hidden sm:inline-block sm:align-middle sm:h-screen"
            aria-hidden="true"
          >
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          >
            <div className="inline-block align-bottom bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full sm:p-6">
              <div>
                <div className="mx-auto flex items-center justify-center h-12 w-12 rounded-full bg-green-100">
                  <CheckIcon
                    className="h-6 w-6 text-green-600"
                    aria-hidden="true"
                  />
                </div>
                <div className="mt-3 sm:mt-5">
                  <Dialog.Title
                    as="h3"
                    className="text-center text-lg leading-6 font-medium text-gray-900"
                  >
                    {t("commitToPurchase")}
                  </Dialog.Title>
                  <div className="prose mt-2 text-sm text-gray-500">
                    <p>
                      <b>{confirmTexts.privacy.title}</b>:{" "}
                      {confirmTexts.privacy.description}{" "}
                      <a className="" href="mailto:contact@youngvision.info">
                        contact@youngvision.info
                      </a>
                    </p>
                    <p>
                      <b>{confirmTexts.commitment.title}</b>:{" "}
                      {confirmTexts.commitment.description}
                    </p>
                    <p>
                      <b>{confirmTexts.covid.title}</b>:{" "}
                      {confirmTexts.covid.description}{" "}
                      <span className="font-medium">
                        {confirmTexts.covid.test}
                      </span>
                    </p>
                  </div>
                </div>
              </div>
              <div className="mt-5 sm:mt-6 sm:grid sm:grid-cols-2 sm:gap-3 sm:grid-flow-row-dense">
                <button
                  type="submit"
                  className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-teal-600 text-base font-medium text-white hover:bg-teal-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-teal-500 sm:col-start-2 sm:text-sm"
                  onClick={() => {
                    setOpen(false);
                    submit();
                  }}
                >
                  {t("buy")}
                </button>
                <button
                  type="button"
                  className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-teal-500 sm:mt-0 sm:col-start-1 sm:text-sm"
                  onClick={() => {
                    setSubmitting(false);
                    setOpen(false);
                  }}
                  ref={cancelButtonRef}
                >
                  {t("cancel")}
                </button>
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  );
}

const FormField: React.FC<InputProps & { name: keyof OrderDetails }> = ({
  name,
  autoComplete,
  type = "text",
  ...inputProps
}) => {
  const { t } = useI18n();

  const errorId = `${name}-error`;
  const renderField = ({
    field: { value, ...field }, // { name, value, onChange, onBlur }
    meta: { touched, error },
  }: FieldProps) => {
    const isError = touched && error;
    const inputClasses = isError
      ? "focus:ring-red-500 focus:border-red-500 border-red-300 text-red-900 placeholder-red-300"
      : "focus:ring-teal-500 focus:border-teal-500 border-gray-300";
    return (
      <div className="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
        <label
          htmlFor={name}
          className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
        >
          {t("orderDetails")[name]}
        </label>
        <div className="mt-1 relative sm:mt-0 sm:col-span-2">
          <input
            autoComplete={autoComplete}
            type={type}
            value={value ?? ""}
            {...field}
            {...inputProps}
            className={classNames(
              inputClasses,
              type == "checkbox" ? "w-4 h-4 text-teal-600" : "w-full",
              "max-w-lg block shadow-sm rounded-md sm:max-w-xs sm:text-sm"
            )}
            aria-describedby={errorId}
          />
          {isError && (
            <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
              <svg
                className="h-5 w-5 text-red-500"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                aria-hidden="true"
              >
                <path
                  fillRule="evenodd"
                  d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
          )}
        </div>
        <p className="mt-2 h-4 text-sm text-red-600" id={errorId}>
          <ErrorMessage name={name} />
        </p>
      </div>
    );
  };
  return <Field name={name}>{renderField}</Field>;
};
const Disclaimer: React.FC<{ birthday: string | null }> = ({ birthday }) => {
  const { t } = useI18n();

  const renderField = ({ field, meta: { touched, error } }: FieldProps) => {
    const { name } = field;
    const isError = touched && error;
    const errorId = `${name}-error`;
    const inputClasses = isError
      ? "focus:ring-red-500 focus:border-red-500 border-red-300 text-red-900 placeholder-red-300"
      : "focus:ring-teal-500 focus:border-teal-500 border-gray-300 text-teal-600";

    const age = birthday == null ? birthday : parseDateString(birthday);
    const over18 = age != null ? age < new Date(2004, 8, 13) : undefined;
    const disclaimer = over18
      ? t("orderDetails").disclaimer.adult
      : t("orderDetails").disclaimer.minor;

    return (
      <div className="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
        <label
          htmlFor={name}
          className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
        >
          {disclaimer}
          <br />
          <a
            href="/Haftungsausschluss 2022.pdf"
            className="text-indigo-800"
            download
          >
            Diesen Haftungsauschluss
          </a>
        </label>
        <div className="mt-1 relative sm:mt-0 sm:col-span-2">
          <input
            {...field}
            className={`h-4 w-4 rounded ${inputClasses}`}
            type="checkbox"
          />
          {isError && (
            <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
              <svg
                className="h-5 w-5 text-red-500"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                aria-hidden="true"
              >
                <path
                  fillRule="evenodd"
                  d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
          )}
        </div>
        <p className="mt-2 h-4 text-sm text-red-600" id={errorId}>
          <ErrorMessage name={name} />
        </p>
      </div>
    );
  };

  return (
    <Field type="checkbox" name="disclaimer">
      {renderField}
    </Field>
  );
};

type InitialValueType = {
  [P in keyof OrderDetails]: OrderDetails[P] | null;
};

const INITIAL_VALUES: InitialValueType = {
  name: "",
  birthday: null,
  email: "",
  country: "Deutschland",
  streetAddress: "",
  city: "",
  zipCode: "",
  phone: "",
  disclaimer: false,
  englishTranslation: false,
  glutenfree: false,
  healthinfo: "",
  duration: "",
  commitment: false,
  motivation: "",
};

const EnterDetails: React.FC<{
  onSubmit: () => void;
}> = ({ onSubmit }) => {
  const { t } = useI18n();
  const [confirm, setConfirm] = useState(false);

  const signupTexts = t("signUp");

  return (
    <>
      <div className="prose mb-16">
        <p className="text-justify">
          {signupTexts.introTexts.ageInfo.general}
          <em>{signupTexts.introTexts.ageInfo.footnote}</em>
        </p>
        <p className="text-justify">{signupTexts.introTexts.psychHealth}</p>
        <p className="text-justify">{signupTexts.introTexts.camping}</p>
      </div>
      <Formik
        initialValues={INITIAL_VALUES}
        validationSchema={schema}
        onSubmit={(values, { setSubmitting }) => {
          return axios
            .post("/api/buy", values)
            .then(() => onSubmit())
            .catch(() => toast.error(t("somethingWentWrong")))
            .then(() => setSubmitting(false));
        }}
      >
        {({
          setSubmitting,
          isSubmitting,
          submitCount,
          submitForm,
          validateForm,
          setTouched,
          values: { birthday },
        }) => (
          <Form className="space-y-8 divide-y divide-gray-200">
            <div className="space-y-8 divide-y divide-gray-200 sm:space-y-5">
              <div>
                <h3 className="text-lg leading-6 font-medium text-gray-900">
                  {t("personalInformation")}
                </h3>
                <span className="text-gray-600 text-sm">
                  {t("ticketOwner")}
                </span>
              </div>
              <div className="space-y-6 sm:space-y-5">
                {submitCount >= 3 ? (
                  <div>
                    Contact us at{" "}
                    <a href="mailto:anmeldung@youngvisionsommer.de">
                      anmeldung@youngvisionsommer.de
                    </a>{" "}
                    if you are having problems
                  </div>
                ) : null}
                <FormField name="name" autoComplete="name" />
                <FormField
                  name="birthday"
                  placeholder="DD.MM.YYYY"
                  type="text"
                  autoComplete="bday"
                />
                <FormField name="email" type="email" autoComplete="email" />
                <FormField name="phone" type="tel" autoComplete="tel" />
                <FormField
                  name="streetAddress"
                  autoComplete="home street-address"
                />
                <FormField name="city" autoComplete="home city" />
                <FormField name="zipCode" autoComplete="home postal-code" />
                <FormField name="country" autoComplete="home country-name" />
                <FormField name="englishTranslation" type="checkbox" />
                <Disclaimer birthday={birthday} />
                <SummerCommitment />
                <Motivation />
                <HealthInfo />
              </div>
              <div className="pt-5">
                <div className="flex justify-end">
                  <button
                    type="button"
                    onClick={() => {
                      validateForm().then((errors) => {
                        setTouched(setNestedObjectValues(errors, true));
                        // errors are set on the object as properties
                        // so if age field has an error errors.age = "message"
                        // so if no values are set we assume no errors
                        if (Object.values(errors).length == 0) {
                          setSubmitting(true);
                          setConfirm(true);
                        }
                      });
                    }}
                    disabled={isSubmitting}
                    className="px-4 py-2 border border-transparent leading-6 ml-3 inline-flex items-center justify-center shadow-sm text-sm font-medium rounded-md text-white bg-teal-600 hover:bg-teal-700 focus:outline-none disabled:ring-0 focus:ring-2 focus:ring-offset-2 focus:ring-teal-500 disabled:bg-gray-500"
                  >
                    {isSubmitting && (
                      <svg
                        className="animate-spin -ml-1 mr-3 h-5 w-5 text-white"
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                      >
                        <circle
                          className="opacity-25"
                          cx="12"
                          cy="12"
                          r="10"
                          stroke="currentColor"
                          strokeWidth="4"
                        />
                        <path
                          className="opacity-75"
                          fill="currentColor"
                          d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                        />
                      </svg>
                    )}
                    {t("commitToPurchase")}
                  </button>
                </div>
              </div>
            </div>
            <ConfirmPayment
              open={confirm}
              setOpen={setConfirm}
              submit={submitForm}
              setSubmitting={setSubmitting}
            />
          </Form>
        )}
      </Formik>
    </>
  );
};

export default EnterDetails;
