import React from "react";
import { useI18n } from "../i18n";
import ReactMarkdown from "react-markdown";
import { CovidConceptQuery } from "../lib/youngvision-sdk";

export type covidConcepts = CovidConceptQuery["covidConcepts"];
const CovidInfo: React.FC<{
  onAgree: () => void;
  covidConcepts: covidConcepts;
}> = ({ onAgree, covidConcepts }) => {
  const { t } = useI18n();
  const covidFAQlocale = t("covidFAQ");
  return (
    <div className="bg-teal-700">
      <div className="max-w-7xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:px-8">
        <h2 className="text-3xl font-extrabold text-white">
          {covidFAQlocale.title}
        </h2>
        <div className="mt-6 border-t border-teal-300 border-opacity-25 pt-10 text-white text-justify text-lg">
          <div className="space-y-4">
            {covidFAQlocale.intro.map((i) => (
              <p key={i}>{i}</p>
            ))}
          </div>
          <dl className="space-y-10 md:space-y-0 md:grid md:grid-cols-2 md:grid-rows-2 md:gap-x-8 md:gap-y-12 mt-10">
            {covidConcepts.map((c) => {
              return (
                <div key={c.id}>
                  <dt className="text-lg leading-6 font-medium text-white">
                    {c.header}
                  </dt>
                  <dd>
                    <ReactMarkdown className="prose-teal mt-2 text-base text-teal-200">
                      {c.description}
                    </ReactMarkdown>
                  </dd>
                </div>
              );
            })}
          </dl>
          <div className="mt-12 space-y-4">
            {covidFAQlocale.outro.map((t) => (
              <ReactMarkdown key={t}>{t}</ReactMarkdown>
            ))}
          </div>
        </div>
        <div className="mt-8">
          <div className="rounded-lg shadow-md">
            <a
              onClick={() => onAgree()}
              href="#"
              className="block w-full text-center rounded-lg border border-transparent bg-white px-6 py-3 text-base font-bold text-teal-600 hover:bg-gray-50"
            >
              {covidFAQlocale.accept}
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CovidInfo;
