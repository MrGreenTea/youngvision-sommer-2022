import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useI18n } from "../i18n";
import { Disclosure } from "@headlessui/react";
import { MenuIcon, XIcon } from "@heroicons/react/outline";
import { classNames } from "../lib/tailwindUtilities";

export const LINKS = {
  Home: "/",
  Location: "/location",
};

export const NavBar: React.FC = () => {
  const { t } = useI18n();
  const { pathname, locale, push } = useRouter();

  const nonMobileEntries: JSX.Element[] = [];
  const mobileEntries: JSX.Element[] = [];
  let page: keyof typeof LINKS;
  for (page in LINKS) {
    const link = LINKS[page];
    const isCurrent = pathname == link;
    const element = (
      <Link key={page} href={link}>
        <a
          className={classNames(
            isCurrent
              ? "border-cyan-500 text-gray-900"
              : "border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700",
            "inline-flex items-center px-1 pt-1 border-b-2 text-sm font-medium"
          )}
        >
          {t("menu")[page]}
        </a>
      </Link>
    );
    const mobileElement = (
      <Link key={page} href={link}>
        <a
          className={classNames(
            isCurrent
              ? "bg-cyan-50 border-cyan-500 text-cyan-700"
              : "border-transparent text-gray-600 hover:bg-gray-50 hover:border-gray-300 hover:text-gray-800",
            "block pl-3 pr-4 py-2 border-l-4 text-base font-medium"
          )}
        >
          {t("menu")[page]}
        </a>
      </Link>
    );
    nonMobileEntries.push(element);
    mobileEntries.push(mobileElement);
  }

  return (
    <Disclosure as="nav" className="bg-white shadow">
      {({ open }) => (
        <>
          <div className="max-w-7xl mx-auto px-2 sm:px-4 lg:px-8">
            <div className="flex justify-between h-16">
              <div className="flex px-2 lg:px-0">
                <div className="hidden lg:ml-6 lg:flex lg:space-x-8">
                  {nonMobileEntries}
                </div>
              </div>
              <a
                onClick={async (event) => {
                  event.preventDefault();
                  await push(pathname, pathname, {
                    locale: locale === "de" ? "en" : "de",
                  });
                }}
                href="#"
                className="hidden lg:inline-flex items-center justify-center p-2 rounded-md text-gray-800 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-cyan-500 w-16"
              >
                {t("menu").changeLanguage}
              </a>
              <div className="flex items-center lg:hidden">
                {/* Mobile menu button */}
                <Disclosure.Button className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-cyan-500">
                  <span className="sr-only">Open main menu</span>
                  {open ? (
                    <XIcon className="block h-6 w-6" aria-hidden="true" />
                  ) : (
                    <MenuIcon className="block h-6 w-6" aria-hidden="true" />
                  )}
                </Disclosure.Button>
              </div>
            </div>
          </div>

          <Disclosure.Panel className="lg:hidden">
            <div className="pt-2 pb-3">
              {mobileEntries}
              <a
                onClick={async (event) => {
                  event.preventDefault();
                  await push(pathname, pathname, {
                    locale: locale === "de" ? "en" : "de",
                  });
                }}
                href="#"
                className="mt-4 border-transparent text-gray-800 hover:bg-gray-50 hover:border-gray-300 hover:text-gray-800 block pl-3 pr-4 py-2 border-l-4 text-base font-medium"
              >
                {t("menu").changeLanguage}
              </a>
            </div>
          </Disclosure.Panel>
        </>
      )}
    </Disclosure>
  );
};

export default NavBar;
