import React, { useEffect, useState } from "react";
import { useSwipeable } from "react-swipeable";
import { motion } from "framer-motion";

export type PaginationProps = {
  pages: number[];
  activePage: number;
  onClick: (index: number) => void;
};

export const Carousel: React.FC<{
  Pagination: React.FC<PaginationProps>;
}> = ({ Pagination, children }) => {
  const slides = React.Children.toArray(children);
  const [current, _setCurrent] = useState(0);
  const [offset, setOffset] = useState(0);

  const setCurrent = (c: number | ((c: number) => number)) => {
    _setCurrent(c);
    setOffset(0);
  };

  useEffect(() => {
    const timeout = setTimeout(
      () => setCurrent((c) => (c + 1) % slides.length),
      30000
    );
    return () => clearTimeout(timeout);
  }, [current, setCurrent]);
  const handlers = useSwipeable({
    onSwipedLeft: (e) => {
      if (current < slides.length - 1 && Math.abs(e.absX) >= 100) {
        setCurrent((c) => (c + 1) % slides.length);
      }
    },
    onSwipedRight: (e) => {
      if (current > 0 && Math.abs(e.absX) >= 100) {
        setCurrent((c) => (c - 1) % slides.length);
      }
    },
    onSwiping: (e) => {
      setOffset(e.deltaX);
      if (current === 0) {
        setOffset(Math.min(e.deltaX, 100));
      } else if (current === slides.length - 1) {
        setOffset(Math.max(e.deltaX, -100));
      } else {
        setOffset(e.deltaX);
      }
    },
    onSwiped: () => setOffset(0),
    trackTouch: true,
  });

  const transition = offset === 0 ? undefined : { duration: 0 };

  return (
    <div {...handlers} className="overflow-hidden relative w-full">
      <div
        style={{
          width: `${slides.length * 100}%`,
        }}
      >
        <motion.div
          className="w-full flex flex-row flex-nowrap overflow-hidden relative motion-reduce:transition-none"
          animate={{
            x: `calc(-${(100 / slides.length) * current}% + ${offset}px)`,
          }}
          initial={true}
          transition={transition}
        >
          {slides.map((t, i) => (
            <div key={i} className="w-full scroll-snap-align-start">
              {t}
            </div>
          ))}
        </motion.div>
      </div>

      <Pagination
        pages={slides.map((_, i) => i)}
        activePage={current}
        onClick={setCurrent}
      />
    </div>
  );
};
