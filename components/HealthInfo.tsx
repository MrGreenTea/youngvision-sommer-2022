import React from "react";
import { useI18n } from "../i18n";
import { useField } from "../lib/forms";

const GLUTEN_FREE_FIELD_NAME = "glutenfree";

const GlutenFree: React.FC = () => {
  const [field] = useField<string>(GLUTEN_FREE_FIELD_NAME);
  const { t } = useI18n();

  const glutenfree = t("signUp").healthInfo.glutenfree;

  return (
    <div className="my-4 relative flex items-start">
      <div className="flex items-center h-5">
        <input
          {...field}
          id={GLUTEN_FREE_FIELD_NAME}
          aria-describedby={`${GLUTEN_FREE_FIELD_NAME}-description`}
          type="checkbox"
          className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
        />
      </div>
      <div className="ml-3 text-sm">
        <label
          htmlFor={GLUTEN_FREE_FIELD_NAME}
          className="font-medium text-gray-700"
        >
          {glutenfree.title}
        </label>
        <p
          id={`${GLUTEN_FREE_FIELD_NAME}-description`}
          className="text-gray-500"
        >
          {glutenfree.description}
        </p>
      </div>
    </div>
  );
};

const HEALTH_INFO_FIELD_NAME = "healthinfo";

const HealthInfo: React.FC = () => {
  const [field] = useField<string>(HEALTH_INFO_FIELD_NAME);
  return (
    <div>
      <label
        htmlFor={HEALTH_INFO_FIELD_NAME}
        className="text-lg leading-6 font-medium text-gray-900"
      >
        Was sollten wir wissen?
      </label>
      <div className="mt-1">
        <textarea
          {...field}
          rows={4}
          id={HEALTH_INFO_FIELD_NAME}
          className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
          placeholder="Besondere Unverträglichkeiten, Medikamente, Allergien, körperl. oder psych. Einschränkungen, therapeutische Vorgeschichte, etc."
        />
      </div>
      <GlutenFree />
    </div>
  );
};

export default HealthInfo;
