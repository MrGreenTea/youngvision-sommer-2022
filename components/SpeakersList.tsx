import React from "react";
import Image from "next/image";
import { MDXRemote } from "next-mdx-remote";

import { AllSpeakersQuery } from "../lib/youngvision-sdk";
import { useI18n } from "../i18n";
import { resortToPlaceholderPortait } from "../lib/resources";
import { ValuesType } from "utility-types";
import { MDXRemoteSerializeResult } from "next-mdx-remote/dist/types";

export type SerializedSpeaker = Omit<
  ValuesType<AllSpeakersQuery["speakers"]>,
  "description"
> & {
  description: MDXRemoteSerializeResult;
};
export type Speakers = SerializedSpeaker[];

const SpeakersList: React.FC<{ speakers: Speakers }> = ({ speakers }) => {
  const { t } = useI18n();
  return (
    <div className="space-y-12">
      <h2 className="text-3xl font-extrabold tracking-tight sm:text-4xl">
        {t("program").speakers}
      </h2>

      <ul className="space-y-12 lg:grid lg:grid-cols-2 lg:items-start lg:gap-x-8 lg:gap-y-12 lg:space-y-0">
        {speakers.map((person) => (
          <li key={person.name}>
            <div className="space-y-4 sm:grid sm:grid-cols-3 sm:gap-6 sm:space-y-0 lg:gap-8">
              <div className="h-0 aspect-w-3 aspect-h-2 sm:aspect-w-3 sm:aspect-h-4">
                <Image
                  className="object-cover shadow-lg rounded-lg"
                  src={resortToPlaceholderPortait(person.image?.url)}
                  layout="fill"
                  alt={`${person.name} portrait`}
                />
              </div>
              <div className="sm:col-span-2">
                <div className="space-y-4">
                  <div className="text-lg leading-6 font-medium space-y-1">
                    <h3>{person.name}</h3>
                    <p className="text-indigo-600">{person.subtitle}</p>
                  </div>
                  <div className="text-lg prose text-justify leading-snug text-gray-500">
                    <MDXRemote {...person.description} lazy />
                  </div>
                </div>
              </div>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
};
export default SpeakersList;
