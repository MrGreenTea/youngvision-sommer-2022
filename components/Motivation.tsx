import React from "react";
import { ErrorMessage } from "formik";
import { useField } from "../lib/forms";
import { useI18n } from "../i18n";

const FIELD_NAME = "motivation";

const Motivation: React.FC = () => {
  const [field] = useField<string>(FIELD_NAME);
  const { t } = useI18n();
  const localization = t("signUp").motivation;

  return (
    <div>
      <label
        htmlFor={FIELD_NAME}
        className="text-lg leading-6 font-medium text-gray-900"
      >
        {localization.title}
      </label>
      <div className="mt-1">
        <textarea
          {...field}
          rows={4}
          id={FIELD_NAME}
          className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
          placeholder={localization.description}
        />
      </div>
      <p className="mt-2 h-4 text-sm text-red-600" id={`${FIELD_NAME}-error`}>
        <ErrorMessage name={FIELD_NAME} />
      </p>
    </div>
  );
};

export default Motivation;
