import React from "react";
import { useI18n } from "../i18n";
import LiteYouTubeEmbed from "react-lite-youtube-embed";

const Trailer: React.FC = () => (
  <LiteYouTubeEmbed
    poster="maxresdefault"
    wrapperClass="yt-lite w-full bg-no-repeat bg-center bg-cover aspect-h-9 aspect-w-16 rounded-md ring-1 ring-black ring-opacity-5 shadow-xl"
    id="TUTlfmwuoAc"
    title="YoungVision Sommer 2022 Trailer"
  />
);

export const HeroSection: React.FC = () => {
  const { t } = useI18n();

  const greetings = t("greetings");

  return (
    <>
      <div className="bg-teal-700 overflow-hidden sm:pt-12 lg:relative lg:py-24">
        <div className="mx-auto max-w-md px-4 sm:max-w-3xl sm:px-6 lg:rounded-b-xl lg:px-8 lg:max-w-7xl lg:grid lg:grid-cols-2 lg:gap-24">
          <div>
            <div className="lg:mt-20">
              <div className="mt-6 sm:max-w-xl">
                <h1 className="text-4xl font-moontype text-white tracking-tight sm:text-5xl">
                  {t("name")}
                </h1>
                <h3 className="mt-6 font-medium text-teal-200">
                  {greetings.freeSummer}
                </h3>
                <p className="text-base text-justify text-teal-100">
                  {greetings.text}
                </p>
                <a
                  className="text-bold text-teal-300"
                  href="https://t.me/youngvisionev"
                >
                  Telegram Channel
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="sm:mx-auto sm:max-w-3xl sm:px-6">
          <div className="pt-12 sm:py-12 sm:relative sm:mt-12 sm:mb-4 sm:py-16 lg:absolute lg:inset-y-0 lg:right-0 lg:w-1/2">
            <div className="hidden sm:block">
              <div className="absolute inset-y-0 left-1/2 w-screen bg-teal-50 rounded-l-3xl lg:left-80 lg:right-0 lg:w-full" />
            </div>
            <div className="md:h-full md:flex md:flex-col md:justify-center">
              <Trailer />
            </div>
          </div>
        </div>
      </div>
      <div className="md:hidden h-12" />
    </>
  );
};
