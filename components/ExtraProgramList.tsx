import React from "react";
import { AllSpeakersQuery } from "../lib/youngvision-sdk";
import { resortToPlaceholderPortait } from "../lib/resources";
import Image from "next/image";
import { useI18n } from "../i18n";

export type Extras = AllSpeakersQuery["extraPrograms"];

const ExtraProgramList: React.FC<{ extras: Extras }> = ({ extras }) => {
  if (extras.length == 0) return null;
  const { t } = useI18n();

  return (
    <div className="space-y-8 sm:space-y-12 mt-12">
      <div className="space-y-5 sm:max-w-xl sm:space-y-4 lg:max-w-5xl">
        <h2 className="text-3xl font-extrabold tracking-tight sm:text-4xl">
          {t("program").extrasTitle}
        </h2>
      </div>
      <ul className="mx-auto grid grid-cols-2 gap-x-4 gap-y-8 sm:grid-cols-4 md:gap-x-6 lg:max-w-5xl lg:gap-x-8 lg:gap-y-12 xl:grid-cols-6">
        {extras.map((person) => (
          <li key={person.name}>
            <div className="space-y-4 text-center ">
              <div className="h-0 aspect-w-3 aspect-h-3">
                <Image
                  className="object-cover shadow-lg rounded-full m-4"
                  src={resortToPlaceholderPortait(person.image?.url)}
                  layout="fill"
                  alt={`${person.name} portrait`}
                  sizes="256px"
                />
              </div>
              <div className="space-y-2">
                <div className="text-xs font-medium lg:text-sm">
                  <h3>{person.name}</h3>
                  <p className="text-indigo-600">{person.subtitle}</p>
                </div>
              </div>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
};
export default ExtraProgramList;
