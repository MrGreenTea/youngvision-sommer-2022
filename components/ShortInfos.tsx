import React from "react";
import { ShortInfoTextsQuery } from "../lib/youngvision-sdk";
import { ValuesType } from "utility-types";
import Image from "next/image";
import { MDXRemoteSerializeResult } from "next-mdx-remote/dist/types";
import { MDXRemote } from "next-mdx-remote";

type ShortInfosType = ShortInfoTextsQuery["shortInfoTexts"];
export type MDXSerializedShortInfoType = Omit<
  ValuesType<ShortInfosType>,
  "content"
> & {
  content: MDXRemoteSerializeResult;
};

const ShortInfo: React.FC<{
  info: MDXSerializedShortInfoType;
  index: number;
}> = ({ info, index }) => {
  return (
    <div className="relative">
      <div className="lg:absolute lg:inset-0">
        <div
          className={`lg:absolute lg:inset-y-0 lg:w-1/2 ${
            index % 2 === 0 ? "lg:left-0" : "lg:right-0"
          }`}
        >
          <div className="relative h-56 w-full lg:absolute lg:h-full">
            <Image
              className="shadow-xl"
              src={info.image?.url ?? ""}
              layout="fill"
              objectFit="cover"
              alt={info.header}
            />
          </div>
        </div>
      </div>
      <div className="relative pt-12 pb-16 px-4 sm:pt-16 sm:px-6 lg:px-8 lg:max-w-7xl lg:mx-auto lg:grid lg:grid-cols-2">
        <div
          className={`${
            index % 2 === 0
              ? "lg:col-start-2 lg:pl-8"
              : "lg:col-start-1 lg:pr-8"
          }`}
        >
          <div
            className={`text-base max-w-prose mx-auto lg:max-w-lg ${
              index % 2 === 0 ? "lg:ml-auto lg:mr-0" : "lg:mr-auto lg:ml-0"
            }`}
          >
            <h1 className="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
              {info.header}
            </h1>
            <div className="mt-8 text-justify prose">
              <MDXRemote {...info.content} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const ShortInfos: React.FC<{
  shortInfoTexts: MDXSerializedShortInfoType[];
}> = ({ shortInfoTexts }) => {
  if (shortInfoTexts.length == 0) {
    return null;
  }
  return (
    <div className="bg-white">
      {shortInfoTexts.map((info, index) => (
        <ShortInfo key={info.id} info={info} index={index} />
      ))}
    </div>
  );
};

export default ShortInfos;
