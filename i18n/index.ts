import { OrderDetails } from "../pages/api/buyTypes";
import { LINKS } from "../components/navbar";
import { RosettaExtended, useI18n as _origUseI18n } from "next-rosetta";

type TitledDescription = {
  title: string;
  description: string;
};

export interface Locale {
  locale: string;
  cancel: string;
  titles: { [P in keyof typeof LINKS]: string };
  name: string;
  theme: string;
  metaDescription: string;
  greetings: {
    freeSummer: string;
    text: string;
    signed: string;
    here: string;
  };
  getYourTicket: string;
  notifyMe: string;
  willNotifyYou: string;
  somethingWentWrong: string;
  enterEmail: string;
  ourSponsors: string;
  menu: {
    [P in keyof typeof LINKS]: string;
  } & { changeLanguage: string };
  program: {
    program2022: string;
    speakers: string;
    extrasTitle: string;
  };
  jobs: {
    summerNeedsYou: string;
    helpAndCreate: string;
    writeUs: string;
    openQuestions: string;
    cantFindAnswer: string;
    contactUs: string;
    needed: string;
  };
  orderDetails: { [P in keyof Omit<OrderDetails, "disclaimer">]: string } & {
    disclaimer: { minor: string; adult: string };
  };
  signUp: {
    introTexts: {
      ageInfo: {
        general: string;
        footnote: string;
      };
      psychHealth: string;
      camping: string;
    };
    confirmation: {
      privacy: TitledDescription;
      commitment: TitledDescription;
      covid: TitledDescription & { test: string };
    };
    healthInfo: {
      glutenfree: TitledDescription;
    };
    durations: {
      fullTime: TitledDescription;
      firstHalf: TitledDescription;
      secondHalf: TitledDescription;
    };
    motivation: TitledDescription;
    commitment: {
      info: TitledDescription;
      contract: TitledDescription & { hashtags: string };
      signing: TitledDescription;
    };
    travelExpenses: {
      title: string;
      info: string;
      howto: string;
      choices: {
        advance: TitledDescription;
        reimburse: TitledDescription;
      };
    };
    success: {
      title: {
        thankYou: string;
        lookingForward: string;
      };
      confirmationMailInfo: string;
      signUpAnotherPerson: string;
    };
  };
  location: {
    travelTips: string;
  };
  covidFAQ: {
    title: string;
    intro: string[];
    outro: string[];
    accept: string;
  };
  buy: string;
  purchaseInfo: string;
  halfTicket: string;
  commitToPurchase: string;
  ticketQuestions: string;
  ticketsLeft: string;
  noTicketsLeft: string;
  perfectTicket: string;
  personalInformation: string;
  ticketOwner: string;
}

export function useI18n(): RosettaExtended<Locale> {
  return _origUseI18n();
}

type SupportedLocale = "en" | "de";

// Just some nicely typed functions to see early when something goes wrong
export async function loadTable(locale: string | undefined): Promise<Locale> {
  const hardLocale = locale as SupportedLocale;
  const data = await import(`../i18n/${hardLocale}`); // Import locale
  return data.table as Locale;
}
