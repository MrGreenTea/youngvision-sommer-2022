import { Locale } from ".";

export const table: Locale = {
  locale: "Deutsch",
  cancel: "Abbrechen",
  titles: {
    Home: "YoungVision Sommer 2022",
    Location: "YoungVision Sommer 2022 - Ort",
  },
  name: "YoungVision Sommer",
  theme: "Bereit * Befreit * Bereichert",
  metaDescription: "YoungVision Sommer 2022 - vom 12. bis 21. August",
  greetings: {
    freeSummer: "Danke für 2022",
    text:
      "Wir schauen zufrieden auf den vergangenen YoungVision Sommer zurück und freuen uns mit euch auf 2023. Folgt uns gerne unserem Telegram Kanal unter t.me/youngvisionev um informiert zu bleiben.",
    signed: "Dein YoungVision Sommer Team",
    here: "hier",
  },
  getYourTicket: "Melde dich an",
  notifyMe: "Haltet mich auf dem Laufenden",
  willNotifyYou: "Du hörst von uns!",
  somethingWentWrong: "Das hat nicht geklappt! Bitte versuche es noch einmal",
  enterEmail: "Gib deine Email ein",
  ourSponsors: "Unsere Sponsoren",
  menu: {
    Home: "Home",
    Location: "Ort",
    changeLanguage: "EN",
  },
  program: {
    program2022: "Programm 2022",
    speakers: "Referierende",
    extrasTitle: "Musik, Kunst & Co.",
  },
  orderDetails: {
    name: "Voller Name",
    email: "Email",
    phone: "Telefonnummer",
    country: "Land",
    streetAddress: "Straße & Hausnummer",
    city: "Stadt",
    zipCode: "PLZ",
    disclaimer: {
      adult: "Ich werde den Haftungsauschluss unterschreiben",
      minor:
        "Ein*e Erziehungsberechtigte*r wird den Haftungsauschluss unterschreiben",
    },
    englishTranslation: "Ich brauche Übersetzung von Deutsch zu Englischb",
    commitment: "Commitment",
    motivation: "Motivation",
    glutenfree: "Glutenfrei",
    duration: "Dauer",
    birthday: "Geburtsdatum",
    healthinfo: "Gesundheitsinformationen",
  },
  jobs: {
    summerNeedsYou: "Dein Sommer braucht dich!",
    helpAndCreate: "Helfen und Mitgestalten",
    writeUs: "Schreib Uns",
    openQuestions: "Offene Fragen?",
    cantFindAnswer: "Kannst du die Antwort auf deine Frage nicht finden?",
    contactUs: "Schreib uns an",
    needed: "gesucht",
  },
  location: {
    travelTips:
      "Bahnhof: Sternfeld (Achte darauf, dass es Sternfeld ist und nicht Sternenfeld, Hohenmocker)",
  },
  signUp: {
    introTexts: {
      ageInfo: {
        general:
          "Der YoungVision Sommer richtet sich an 15- bis 26-Jährige*, egal welcher Vorbildung und Herkunft, aus dem gesamten Bundesgebiet. Wenn du unter 18 Jahre bist, brauchst du eine Teilnahme-Erlaubnis deiner Erziehungsberechtigten. Siehe unten Haftungsausschluss.",
        footnote:
          "*Die ehrenamtlichen Helfer:innen dürfen älter sein als 26 Jahre.",
      },
      psychHealth:
        "Die Teilnahme erfordert eine normale psychische Belastbarkeit. Es handelt sich um ein pädagogisches und kein psychotherapeutisches Programm. Bitte nimm bei Unsicherheiten Kontakt mit uns auf. Wir versprechen keine Heilung und übernehmen keine Haftung für auftretende Krisen.",
      camping:
        "Die Unterbringung erfolgt in deinem mitgebrachten Zelt oder Camper. Es gibt leider keine Betten! Da es uns um eine bewusste und wache Lebensführung geht, ist der Konsum von Alkohol und anderen Drogen während des YoungVision Sommers verboten.",
    },
    confirmation: {
      privacy: {
        title: "Datenspeicherung",
        description:
          "Ich bin einverstanden, dass meine Anmeldedaten digital gespeichert werden. Eine Löschung ist jederzeit möglich durch Mail an",
      },
      commitment: {
        title: "Verbindlichkeit",
        description:
          "Ich verpflichte mich, im Falle einer Teilnahme-Zusage zu dem von mir angegebenen Zeitraum zu der Veranstaltung YoungVision Sommer 2022 zu kommen. Du bekommst eine Woche vor Beginn eine Mail, mit der du deine Anmeldung noch einmal bestätigen musst. Ohne diese Bestätigung gehen wir davon aus, dass du deinen Platz für jemand anderen frei geben möchtest.",
      },
      covid: {
        title: "COVID-19",
        description:
          "Ich erkläre mich bereit, die jeweils geltenden gesetzlichen Hygieneregeln einzuhalten.",
        test:
          "Bringe zur Anmeldung einen bescheinigten negativen Corona-Schnelltest mit, der nicht älter als 24 Stunden ist.",
      },
    },
    healthInfo: {
      glutenfree: {
        title: "Glutenfrei",
        description: "Ich brauche (oder wünsche) glutenfreie Verpflegung",
      },
    },
    durations: {
      fullTime: {
        title: "Volle Zeit",
        description: "Ich komme die vollen 10 Tage vom 12. bis 21. August",
      },
      firstHalf: {
        title: "1. Hälfte",
        description: "Ich komme zum ersten Modul vom 12. bis 17. August",
      },
      secondHalf: {
        title: "2. Hälfte",
        description: "Ich komme zum zweiten Modul 17. bis 21. August",
      },
    },
    motivation: {
      title: "Motivation",
      description: "Warum möchtest du am YoungVision Sommer teilnehmen?",
    },
    commitment: {
      info: {
        title: "Unser gemeinsames Commitment",
        description:
          "Der YoungVision Sommer ist wie alle Veranstaltungen von YoungVision eine gemeinsame Reise, die wir zusammen mit dir gestalten. Damit wir für diese Reise eine gemeinsame Basis finden können, haben wir eine Vereinbarung für dich formuliert.",
      },
      contract: {
        title: "Meine Vereinbarung mit YoungVision Sommer 2022",
        description:
          "Hiermit melde ich mich für den YoungVision Sommer 2022 verbindlich an. Sollte ich aus irgendwelchen Gründen doch nicht kommen können, gebe ich YoungVision per E-Mail Bescheid, um einer anderen Person auf der Warteliste die Teilnahme zu ermöglichen. Ich bestätige die Teilnahme an dem YoungVision Sommer für den von mir oben angegebenen Zeitraum und verpflichte mich zu diesem Zeitraum auch zu kommen. Sollte sich daran was ändern, gebe ich YoungVision direkt über das Kontaktformular auf dieser Seite Bescheid. Mir ist bewusst, dass ich entweder zu einem Modul oder zu allen 10 Tagen kommen kann, abweichend zum Beginn der zwei Module gibt es keine Anreisemöglichkeit. Auch hier gebe ich YoungVision Bescheid, wenn ich eine Ausnahmeregelung benötige.",
        hashtags: "#verantwortung #verbindlich #meinraum",
      },
      signing: {
        title: "Commitment",
        description:
          "Ich melde mich verbindlich für den YoungVision Sommer 2022 an",
      },
    },
    travelExpenses: {
      title: "Reisekosten",
      info:
        "Dieses Jahr zahlen wir dir, Dank der Bundesförderung, nicht nur dein Ticket sondern auch deine Reisekosten. Bitte bringe dein Bahnticket, deinen Tankbeleg etc. zur Anmeldung mit.",
      howto:
        "So gehst du vor, wenn du einen Vorschuss benötigst: Sobald du deine Verbindung rausgesucht hast und weißt, was sie kostet, schicke uns eine E-Mail mit dem Betrag und deinem vollen Namen.",
      choices: {
        reimburse: {
          title: "Erstattung",
          description:
            "Ich zahle meine Reisekosten selbst und sie werden mir danach erstattet",
        },
        advance: {
          title: "Vorschuss",
          description: "Ich brauche einen Reisekosten-Vorschuss",
        },
      },
    },
    success: {
      title: {
        thankYou: "Danke!",
        lookingForward: "Wir freuen uns schon sehr auf dich.",
      },
      confirmationMailInfo:
        "Du bekommst kurz nach deiner Anmeldung eine Bestätigungsmail. Wenn du diese nicht bekommst, schreibe uns bitte unbedingt eine E-Mail.",
      signUpAnotherPerson: "Noch jemand anmelden",
    },
  },
  buy: "Anmelden",
  purchaseInfo:
    "Mit abschicken dieses Formulars meldest du dich verbindlich für den YoungVision Sommer an. Bist du dir sicher, dass du kommen wirst?",
  halfTicket: "Halbes Ticket",
  commitToPurchase: "Verbindlich anmelden",
  ticketsLeft: "verfügbar",
  noTicketsLeft: "(Ausverkauft)",
  ticketQuestions:
    "Schreib uns bei Fragen [eine Mail](mailto:anmeldung@youngvisionsommer.de)",
  perfectTicket: "Das passende Ticket für dich, egal wer du bist",
  personalInformation: "Persönliche Informationen",
  ticketOwner: "Ticketinhaber*in",
  covidFAQ: {
    title: "Unser COVID 19 Konzept",
    intro: [
      "Alles ist Jetzt - auch Corona. Wir als Sommer-Team haben lange hin und her überlegt und sind letztendlich zu dem Entschluss gekommen, das Sommer trotz der momentanen Pandemie-Situation stattfinden zu lassen. Nachdem es letztes Jahr aufgrund der vielen Einschränkungen kein Life-Sommer gab, wollen wir es dieses Jahr versuchen in dieser nach wie vor schwierigen Situation ein einmaliges YoungVision Sommer in real life für dich zu kreieren.",
      "In enger Zusammenarbeit mit den örtlichen Behörden sind wir weiterhin dabei ein dementsprechend aktuelles Hygiene-Konzept zu entwerfen. Natürlich können wir nicht in die Zukunft schauen, und haben keine Kontrolle darüber wie die Inzidenz Fälle sich in den nächsten Monaten entwickeln werden. Hier sind unsere aktuell wichtigsten Bestimmungen, wann und wie das YoungVision Sommer 2022 stattfinden kann:",
    ],
    outro: [
      "Diese Leitlinie ist eine Momentaufnahme von März 2022, diese kann sich natürlich bis August noch ändern. Wir werden dich dementsprechend immer wieder auf dem Laufenden halten, sollte es Neuigkeiten oder Änderungen geben.",
      "Auch wenn die Situation gerade schwierig erscheint, bleiben wir optimistisch und geben unser Bestes ein wundervoll einmaliges Sommer ganz im YoungVision Spirit stattfinden zu lassen.",
      "Nur wenn du diesen Maßnahmen zustimmst und dich an diese halten wirst darfst du ein Ticket kaufen.  \n**Solltest du vor Ort gegen diese verstoßen, werden wir dich ohne Erstattung nach Hause schicken.**",
      "Vor Ort wirst du hierzu einen Haftungsauschluss unterschreiben",
    ],
    accept: "Accept",
  },
};
