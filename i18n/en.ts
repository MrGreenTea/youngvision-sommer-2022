import { Locale } from ".";

export const table: Locale = {
  locale: "English",
  cancel: "Cancel",
  titles: {
    Home: "YoungVision Summer 2022",
    Location: "YoungVision Summer 2022 - Location",
  },
  name: "YoungVision Summer",
  theme: "Ready * Liberated * Enriched",
  metaDescription: "YoungVision Summer 2022 - from 12. to 21. August",
  greetings: {
    freeSummer: "Thanks for 2022",
    text:
      "We're glad for the YoungVision summer 2022 and are looking forward to 2023. Stay in touch on our telegram channel under t.me/youngvisionev to get updates.",
    signed: "Your YoungVision Summer Team",
    here: "here",
  },
  getYourTicket: "Get your ticket now",
  notifyMe: "Keep me updated",
  willNotifyYou: "We'll notify you soon!",
  somethingWentWrong: "That did not work. Please try again",
  enterEmail: "Enter your email",
  ourSponsors: "Our sponsors",
  menu: {
    Home: "Home",
    Location: "Location",
    changeLanguage: "DE",
  },
  program: {
    program2022: "Programme 2022",
    speakers: "Speakers",
    extrasTitle: "Music, Art & Co.",
  },
  orderDetails: {
    name: "Full name",
    birthday: "Birthdate",
    email: "Email",
    phone: "Phone number",
    country: "Country",
    streetAddress: "Street address",
    city: "City",
    zipCode: "Zip Code",
    disclaimer: {
      minor: "A guardian will sign the disclaimer",
      adult: "I'll sign the disclaimer",
    },
    englishTranslation: "I need translation from German to English",
    commitment: "Commitment",
    motivation: "Motivation",
    glutenfree: "gluten free",
    healthinfo: "Health info",
    duration: "Duration of stay",
  },
  signUp: {
    introTexts: {
      ageInfo: {
        general:
          "YoungVision Summer is open to 15- to 26-year-olds*, regardless of education or background, from all over Germany. If you are under 18, you will need permission from your parents to participate. See below disclaimer.",
        footnote: "*Volunteers may be older than 26 years.",
      },
      psychHealth:
        "Participation requires normal psychological resilience. This is an educational and not a psychotherapeutic program. Please contact us if you are unsure. We do not promise a cure and assume no liability for any crises that may occur.",
      camping:
        "Accommodation will be in your own tent or camper. Since we care about a conscious and awake lifestyle, the consumption of alcohol and other drugs is prohibited during the YoungVision summer.",
    },
    confirmation: {
      privacy: {
        title: "Data storage",
        description:
          "I agree that my registration data will be stored digitally. A deletion is possible at any time by mail to",
      },
      commitment: {
        title: "Commitment",
        description:
          "I commit myself to come to the event 'YoungVision Summer 2022' in case of a confirmation of participation at the time stated by me." +
          "You will receive an email one week before the event to confirm your registration. Without this confirmation we will assume that you want to give your place to someone else.",
      },
      covid: {
        title: "COVID-19",
        description:
          " I agree to comply with the applicable legal hygiene regulations.",
        test:
          "Please bring a certified negative Corona rapid test not older than 24 hours to the registration.",
      },
    },
    healthInfo: {
      glutenfree: {
        title: "Glutenfree",
        description: "I need (or wish) glutenfree catering",
      },
    },
    durations: {
      fullTime: {
        title: "Full time",
        description: "I will come the full 10 days from 12. to 21. of August",
      },
      firstHalf: {
        title: "First half",
        description:
          "I will come to the first module from 12. to 17. of August",
      },
      secondHalf: {
        title: "Second half",
        description:
          "I will come to the second module from 17. to 21. of August",
      },
    },
    motivation: {
      title: "Motivation",
      description: "Why do you want to participate in the YoungVision Summer?",
    },
    commitment: {
      info: {
        title: "Our commitment together",
        description:
          "YoungVision Summer, like all YoungVision events, is a joint journey that we create together with you. In order for us to find common ground for this adventure, we have formulated an agreement for you.",
      },
      contract: {
        title: "My agreement with YoungVision Summer 2022",
        description:
          "I hereby make a binding registration for YoungVision Summer 2022. If for any reason I am not able to come, I will inform YoungVision directly via email in order to allow another person on the waiting list to participate. I confirm my participation in the YoungVision Summer for the period of time I have indicated above and commit myself to come to this period of time. If there is any change, I will let YoungVision know directly via the contact form on this page. I am aware that I can either come to one module or to all 10 days, but there is no possibility to arrive other than at the beginning of the two modules. Again, I will let YoungVision know if I need an exception.",
        hashtags: "#responsibility #commitment #myspace",
      },
      signing: {
        title: "Commitment",
        description:
          "I am making a binding registration for YoungVision Summer 2022.",
      },
    },
    travelExpenses: {
      title: "Travel expenses",
      info:
        "This year, thanks to federal funding, we will pay not only your ticket but also your travel expenses. Please bring your train ticket, gas receipt, etc. to the registration.",
      howto:
        "Here's what to do if you need an advance: Once you have your connection figured out and know what it will cost, send us an email with the amount and your full name.",
      choices: {
        reimburse: {
          title: "Reimbursement",
          description:
            "I pay my travel expenses myself and they will be reimbursed to me afterwards",
        },
        advance: {
          title: "Advance",
          description: "I need a travel advance",
        },
      },
    },
    success: {
      title: {
        thankYou: "Thank you!",
        lookingForward: "We're already looking forward to seeing you.",
      },
      confirmationMailInfo:
        "You will receive a confirmation email shortly. Please don't hesitate to get in contact should you not receive it.",
      signUpAnotherPerson: "Sign up another person",
    },
  },
  jobs: {
    summerNeedsYou: "Your summer needs You!",
    helpAndCreate: "Help and co-create",
    writeUs: "Write us",
    openQuestions: "Open questions?",
    cantFindAnswer: "Can’t find the answer you’re looking for?",
    contactUs: "Contact us at",
    needed: "needed",
  },
  location: {
    travelTips:
      "Train station: Sternfeld (pay attention that you travel to Sternfeld and not Sternenfeld, Hohenmocker)",
  },
  buy: "Sign up",
  purchaseInfo:
    "With the submission of this form you commit to actually coming to the YoungVision Summer event. Are you sure you will come?",
  halfTicket: "Half Ticket",
  commitToPurchase: "Commit to sign up",
  ticketsLeft: "left",
  noTicketsLeft: "(Sold out)",
  ticketQuestions:
    "Write us [a mail](mailto:anmeldung@youngvisionsummer.de) if you have any questions",
  perfectTicket: "The perfect ticket for you",
  personalInformation: "Personal information",
  ticketOwner: "Ticket owner",
  covidFAQ: {
    title: "Our COVID-19 concept",
    intro: [
      "Everything is now – so is Corona. We as a summer team have considered the situation for a long time and finally came to the decision to let the summer take place despite the current pandemic situation. After there was no live summer last year due to the many restrictions, this year we want to try in this still difficult situation to create a unique YoungVision Summer in real life for you.",
      "In close cooperation with the local authorities, we are still in the process of developing an up-to-date hygiene concept. Of course, we cannot look into the future and have no control over how the Corona cases will develop in the coming months. Here are our current most important regulations, when and how the YoungVision Summer 2022 can take place:",
    ],
    outro: [
      "This guideline is a snapshot from March 2022, which can of course still change until August. We will keep you updated accordingly, should there be any news or changes.",
      "Even if the situation seems difficult right now, we remain optimistic and do our best to make a wonderfully unique summer happen in the YoungVision spirit.",
      "Only if you agree to these measures and will abide by them you will be allowed to buy a ticket.  \n**If you violate them on site, we will send you home without refund.**",
      "You will sign a liability waiver on site.",
    ],
    accept: "Accept",
  },
};
