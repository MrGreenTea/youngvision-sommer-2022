This is the webpage for the youngvision summer at https://youngvisionsommer.de/

# Development

## Requirements

- [`node`](https://nodejs.org/en/)
- [`yarn`](https://yarnpkg.com/)

## Get started

- `yarn install`

## Starting local

- `yarn run dev`

## Used frameworks & tooling

- The page is built with [`next.js`](https://nextjs.org/)
- we use `typescript`
- [`tailwindcss`](https://tailwindcss.com/) is used for styling
- [`framer-motion`](https://github.com/framer/motion) is used for animations
- [`graphql`](https://graphql.org/) to communicate with https://graphcms.com/
