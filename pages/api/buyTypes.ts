import * as yup from "yup";
import { SchemaOf } from "yup";
import { parseDateString } from "../../lib/dates";

export type OrderDetails = {
  name: string;
  email: string;
  phone: string;
  country: string;
  streetAddress: string;
  city: string;
  zipCode: string;
  disclaimer: boolean;
  englishTranslation: boolean;
  commitment: boolean;
  motivation: string;
  glutenfree: boolean;
  healthinfo: string;
  duration: string;
  birthday: string;
};

// TODO add localization
export const schema: SchemaOf<OrderDetails> = yup.object().shape({
  name: yup.string().required(),
  birthday: yup
    .string()
    .transform((value) => parseDateString(value)?.toISOString())
    .required(),
  email: yup.string().email().required(),
  phone: yup.string().required(),
  country: yup.string().required(),
  streetAddress: yup.string().required(),
  city: yup.string().required(),
  zipCode: yup.string().required(),
  disclaimer: yup.mixed().oneOf([true]).required(),
  englishTranslation: yup.boolean().required(),
  commitment: yup.mixed().oneOf([true]).required(),
  motivation: yup.string().required().trim().min(50),
  glutenfree: yup.boolean().required(),
  healthinfo: yup.string().trim().ensure(),
  duration: yup.string().trim().required(),
});
