import { NextApiRequest, NextApiResponse } from "next";
import { OrderDetails, schema } from "./buyTypes";
import { Coda } from "coda-js";
import { format } from "date-fns";
import axios from "axios";

const CODA_API_TOKEN = process.env.CODA_API_TOKEN;
if (CODA_API_TOKEN == null || CODA_API_TOKEN.length === 0) {
  console.error("$CODA_API_TOKEN is not set");
}

type ValuesNotSentToCoda =
  | "streetAddress"
  | "zipCode"
  | "city"
  | "country"
  | "disclaimer"
  | "commitment";

type CodaMapping = {
  [P in keyof Omit<OrderDetails, ValuesNotSentToCoda>]: string;
} & { address: string; orderDate: string };

const CODA_COLUMN_MAPPING: CodaMapping = {
  name: "c-UplHkEpHTu",
  email: "c-poJHHr3s6D",
  englishTranslation: "c-9tIk4i5Cui",
  phone: "c--U_o8UuCeH",
  birthday: "c-YQ09zVgotg",
  duration: "c--1nfL8OEg1",
  glutenfree: "c-W0b3m3sU-N",
  motivation: "c-Bsyd-M2YS7",
  healthinfo: "c-E6T7YJ5V-6",
  address: "c-oURONnswuK",
  orderDate: "c-VG63GlMeie",
};

function objectToCodaRow(
  obj: { [P in keyof CodaMapping]: unknown }
): Record<string, unknown> {
  let prop: keyof CodaMapping;
  const newRow: Record<string, unknown> = {};
  for (prop in obj) {
    const codaColumn = CODA_COLUMN_MAPPING[prop];
    // Coda will fail when property names are undefined
    if (codaColumn !== undefined) {
      newRow[codaColumn] = obj[prop];
    }
  }
  return newRow;
}

type Email = {
  subject: string;
  textContent: string;
};

type ValidLocale = "en" | "de";

function createEmailMessage(locale: ValidLocale, name: string): Email {
  switch (locale) {
    case "en":
      return {
        subject: "Your YoungVision Summer 2022 registration",
        textContent: `Hello ${name}!
How great that you want to be part of the YoungVision sommer experience 2022. We get a lot of registrations right now and are looking forward to responding to all of them shortly.

Because we get so many registrations at the moment, it'll likely be up to 2 weeks until you get a response from us.
Should you still have no answer after that time write to us at this email.

One week before the YoungVision Summer you will get a message from us where you will have to confirm your spot again.
Should you not respond we'll assume you want to give your spot to someone on the waiting list.
Write to us until 24:00 on 05.08.2022 if that message has not gotten to you for some reason.  

A packing list and other important info will be provided us one week before the event. 

We're looking forward to seeing you
Your YoungVision Summer Team <3
`,
      };
    case "de":
    default:
      return {
        subject: "Deine YoungVision Sommer 2022 Anmeldung",
        textContent: `Hallo ${name}!
Wie  schön, dass Du ein Teil der gemeinsamen YoungVision Sommer Reise 2022 sein willst. Wir haben Deine Anmeldung bekommen und freuen uns sie in kürze zu bearbeiten. 

Wir haben zur Zeit sehr viele Anmeldungen die reinkommen, daher kann es sein, dass eine Antwort auf Deine eventuellen Fragen bis zu zwei Wochen dauern kann. Solltest Du bis dahin keine Antwort erhalten haben, schreibe uns gerne noch einmal an diese E-Mail Adresse. 
Du bekommst eine Woche vor Beginn des YoungVision Sommers eine Mail, mit der Du deine Anmeldung noch einmal bestätigen musst. Ohne diese Bestätigung gehen wir davon aus, dass du Deinen Platz für jemand anderen frei geben magst. Wenn Du keine bekommst, kontrolliere bitte am 05.08.2022 dein Emailfach und wenn die Mail dort nicht zu finden ist, Deinen Spamordner. Solltest Du dennoch keine Mail von uns finden, schreibe uns bitte bis 24:00 Uhr am 05.08.2022 eine Mail und teile uns mit ob Du weiterhin dabei sein magst, oder sich was für Dich verändert hat.
Die Packliste und weitere wichtige Infos, bekommst Du dann in der Woche vor dem YoungVision Sommer. 

Wir freuen uns auf Dich!
Dein YoungVision Sommer Team <3
`,
      };
  }
}

async function sendConfirmationMail({
  locale,
  email,
  name,
}: {
  locale: ValidLocale;
  email: string;
  name: string;
}) {
  const message = createEmailMessage(locale, name);
  return axios.post(
    "https://api.sendinblue.com/v3/smtp/email",
    {
      sender: {
        name: "YoungVision e.V.",
        email: process.env.ANMELDUNG_EMAIL_ACCOUNT,
      },
      to: [
        {
          email: email,
          name: name,
        },
      ],
      ...message,
    },
    {
      headers: {
        accept: "application/json",
        "api-key": process.env.SENDINBLUE_API_KEY,
        "content-type": "application/json",
      },
    }
  );
}

const resolve = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  const { body, method } = req;
  if (method === "POST") {
    await schema
      .validate(body, { strict: false })
      .then(async (order) => {
        console.log("Received order", order);
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const coda = new Coda(CODA_API_TOKEN!);
        const orderTable = await coda.getTable("2tXvi1-jje", "grid-vgCd4Dg5EG");
        const {
          name,
          email,
          country,
          streetAddress,
          city,
          zipCode,
          birthday,
        } = order;
        const address = `${streetAddress}\n${zipCode} ${city}\n${country}`;
        const newRow = objectToCodaRow({
          ...order,
          address,
          orderDate: new Date().toISOString(),
          // Coda offer only american date formats :/
          birthday: format(new Date(birthday), "dd/MM/yyyy"),
        });
        console.log(newRow);
        await orderTable.insertRows([newRow]);
        // next.js does not provide this information in API routes
        // other work arounds are more effort than this
        // https://github.com/vercel/next.js/discussions/21798

        const locale: ValidLocale = order.englishTranslation ? "en" : "de";
        return { email, name, locale };
      })
      .then(sendConfirmationMail)
      .then((mail) => {
        console.log("Message sent: %s", mail.data);
      })

      .then(() => res.status(200).json({}))
      .catch((reason) => {
        const { errors } = reason;
        console.log("Failed with error", reason);
        res.status(400).json({ errors });
      });
  } else {
    res.setHeader("Allow", ["POST"]);
    res.status(405).end(`Method ${method} Not Allowed`);
  }
};
export default resolve;
