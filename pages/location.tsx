import React from "react";
import { useI18n } from "../i18n";
import Head from "next/head";
import Footer from "../components/footer";
import { Toaster } from "react-hot-toast";
import { LocalizedGetStaticProps } from "../lib/types";

const Location: React.FC = () => {
  const { t } = useI18n();

  return (
    <>
      <Head>
        <title>{t("titles").Location}</title>
        <link rel="icon" href="/favicon.svg" />
        <meta name="description" content={t("metaDescription")} />
      </Head>
      <div className="bg-white">
        <main className="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:px-8">
          <p>
            Atelier17111 <br />
            Hohenbrünzow 25, 17111 <br />
            Hohenmocker
          </p>
          <p className="mt-2">{t("location").travelTips}</p>
        </main>
        <Footer />
      </div>
      <Toaster />
    </>
  );
};

export default Location;

export const getStaticProps: LocalizedGetStaticProps = async (context) => {
  const locale = context.locale || context.defaultLocale;
  const { table } = await import(`../i18n/${locale}`); // Import locale

  return { props: { table } };
};
