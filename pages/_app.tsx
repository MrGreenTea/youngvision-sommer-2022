import type { AppProps } from "next/app";
import { I18nProvider } from "next-rosetta";
import "../styles/globals.css";
import React from "react";
import NavBar from "../components/navbar";

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <I18nProvider table={pageProps.table}>
      <NavBar />
      <Component {...pageProps} />
    </I18nProvider>
  );
}

export default MyApp;
