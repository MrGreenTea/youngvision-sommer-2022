import React from "react";
import Head from "next/head";
import { useI18n } from "../i18n";
import getSDK from "../lib/sdk";
import Footer from "../components/footer";
import { LocalizedProps } from "../lib/types";
import { HeroSection } from "../components/hero";
import { Toaster } from "react-hot-toast";
import dynamic from "next/dynamic";
import Sponsors from "../components/sponsors";
import { serialize } from "next-mdx-remote/serialize";
import { MDXSerializedShortInfoType } from "../components/ShortInfos";

// TODO add program / referierende

type Props = {
  shortInfoTexts: MDXSerializedShortInfoType[];
};

const ShortInfos = dynamic(() => import("../components/ShortInfos"), {
  loading: function Loading() {
    return <div>Loading</div>;
  },
});

const Home: React.FC<Props> = ({ shortInfoTexts }) => {
  const { t } = useI18n();

  return (
    <>
      <Head>
        <title>{t("titles").Home}</title>
        <link rel="icon" href="/favicon.svg" />
        <meta name="description" content={t("metaDescription")} />
      </Head>
      <div className="bg-white">
        <main>
          <HeroSection />
          <ShortInfos shortInfoTexts={shortInfoTexts} />
        </main>
        <Sponsors />
        <Footer />
      </div>
      <Toaster />
    </>
  );
};

export const getStaticProps: LocalizedProps<Props> = async (context) => {
  const locale = context.locale || context.defaultLocale;
  const { table } = await import(`../i18n/${locale}`); // Import locale
  const sdk = getSDK({ locale: locale ?? "de" });
  const shortInfos = await sdk.shortInfoTexts();
  const shortInfoTexts: MDXSerializedShortInfoType[] = await Promise.all(
    shortInfos.shortInfoTexts.map(async ({ content, ...s }) => ({
      ...s,
      content: await serialize(content),
    }))
  );

  return { props: { table, shortInfoTexts } };
};

export default Home;
